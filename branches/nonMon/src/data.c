/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file data.c
 * \brief Functions of data.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr 
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : data.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent, Jerome Vienne
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Secondary programm use to R/W access to the datas
*
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h> 

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "data.h"
#include "lect_model.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/


/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/***************************************************************/
/******* DECLARATION DES VARIABLES EXTERNES AU MODULE **********/
/***************************************************************/
extern double *lg_traj_max;
extern int *nbr_echantillon;
extern int *nbr_VA; // number of anthitetic variables
extern int *trajectoire;

extern int *nbr_file;

/** pointeurs sur tableaux de taille nombre de files ***/

extern int *capfile;
extern int *etat_init_supU;
extern int *etat_init_infU;
extern int *etat_inf;
extern int *etat_sup;

extern int *nb_jeton;
extern int **couplage;

extern int *nb_evt;
/** pointeurs sur tableaux de tailles nombres evenements modeles statistiques ***/
/** probabilites des evenements */
extern double *P;
/** seuils */  
extern double *R; 
/** evenements alternatifs si el seuil est depasse */
extern int *A;   


extern struct st_evt *evt;
extern struct st_multi *multis;

/***********************************************************************/
/**** IMPLANTATION DES FONCTIONS EXPORTABLES   **************************/
/***********************************************************************/



/************************************************************************/
/****    FONCTIONS INITIALISATION              ************************/
/************************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : init_etat
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : get_nb_file,get_etat_init_supU,get_etat_init_infU,
*
* Description  :
*    
*     initialised intial states of etat sup/inf tables
*     0 is the value of the coupling table
*
*----------------------------------------------------------------------
*/


inline void init_etat()
{
 int i_boucle;
 int DIMENSION = get_nb_file();
 for (i_boucle = 0; i_boucle< DIMENSION; i_boucle++){
       etat_sup[i_boucle] = get_etat_init_supU(i_boucle);
       etat_inf[i_boucle] = get_etat_init_infU(i_boucle);
 }
}
/**
*----------------------------------------------------------------
*
* Function     : init_couplage
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : get_nb_file,get_etat_init_supU,get_etat_init_infU,
*
* Description  :
*    
*     initialised coupling table
*     0 is the value of the coupling table
*
*----------------------------------------------------------------------
*/
inline void init_couplage(int nb_ant)
{
 int i, i_boucle;
 int DIMENSION = get_nb_file();
 for (i= 0; i<nb_ant; i++){
	for (i_boucle = 0; i_boucle< DIMENSION; i_boucle++){
       couplage[i][i_boucle]= 0;
   }
 }
}

/**
*----------------------------------------------------------------
*
* Function     : init_probabilite
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : nb_evenements
*
* Description  :
*
*	Compute the probality rate, the sum need to be equal to 1    
*
*----------------------------------------------------------------------
*/

inline void init_probabilite(){
  double somme=0;
  int i_boucle;
  int NB_EVTS=nb_evenements();
 for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++)
       somme+=(evt+i_boucle)->lambda;
 for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++)
      *(P+i_boucle)=((evt+i_boucle)->lambda)/somme;
#ifdef DEBUG    
   printf("%f ", somme);
   for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++)
           printf("%f ",*(P+i_boucle);
   printf("\n");
#endif
}

/**
*----------------------------------------------------------------
*
* Function     : set_etat_init_supG
*
* Result       : void
*
* Parameters   : nothing
*
* 
* Description  :
*
*	Assign the capacity give at the initial etat_sup	
*
*----------------------------------------------------------------------
*/

inline void set_etat_init_supG(){
  int i;
  for(i=0;i<*nbr_file;i++)
  *(etat_init_supU+i)=*(capfile+i);
}

/**************************************************************************/
/********************* FONCTIONS DE FIXATION ********************************/
/*****************************************************************************/
/**
*----------------------------------------------------------------
*
* Function     : set_seuil
*
* Result       : void
*
* Parameters   : 
*
* Name      Type      Role
*
* i         int       event number
*
* seuil     double    limit for the event
*
* Description  :
*
*     Fix the threshold at an event
*
*----------------------------------------------------------------------
*/

inline void set_seuil(int i,double seuil){
  *(R+i)=seuil;
}

/**
*----------------------------------------------------------------
*
* Function     : set_evt_alt
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* i         int       event number
*
* k         int       alternative event number
*
* Description  :
*
*     fix the alternative event for an event
*
*----------------------------------------------------------------------
*/

inline void set_evt_alt(int i, int k){
 *(A+i)=k;
}

/**
*----------------------------------------------------------------
*
* Function     : set_trajectoire
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* i         int       trajectory number
*
* evt       int       event number 
*
* Description  :
*
*    fix the trajectory starting from an event
*
*----------------------------------------------------------------------
*/

inline void set_trajectoire(int i, int evt){
  trajectoire[i]=evt;
}

/**
*----------------------------------------------------------------
*
* Function     : set_couplage
*
* Result       : void
*
* Parameters   : 
*
* Name                  Type      Role
*
* i                    int       coupling number
*
* log2tempsarret       int       iteration number 
*
* Description  :
*
*   Assign to the queue number i, the iteration number (log2)   
*   corresponding to the coupling of this queue
*
*----------------------------------------------------------------------
*/

inline void set_couplage(int num, int i,int log2temps_arret){
  couplage[num][i]=log2temps_arret;
}



/*************************************************************************/
/*************  FONCTION DE MODIFICATION ********************************/
/*****************************************************************************/   

/**
*----------------------------------------------------------------
*
* Function     : ajout_file
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* n         int       Queue number 
*
* Description  :
*
*	   add an entry at the queue of rank k 
*
*----------------------------------------------------------------------
*/
inline void ajout_file(n){
   *(nb_jeton+n)+=1;    
  }

/**
*----------------------------------------------------------------
*
* Function     : supp_file
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* n         int       Queue number 
*
* Description  :
*
*     Delete an entry at the queue of rank k 
*
*----------------------------------------------------------------------
*/

inline void supp_file(n){
   *(nb_jeton+n)-=1;  /*  suppression d'un jeton dans la file de rang n */
 }


  
/************************************************************************/
/************* ACCESSEURS **********************************************/
/***********************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : etat_file
*
* Result      : int
*
* Parameters   : 
*
* Nom          Type      Role
*
* n            int       Queue number
*
* Description  :
*
*		return the token number of the queue n
*
*----------------------------------------------------------------------
*/

inline int etat_file(n){
   return nb_jeton[n];
 }

/**
*----------------------------------------------------------------
*
* Function     : etat_file
*
* Result       : int
*
* Parameters   : nothing
*                   
* Description  :
*
*           return the number of queue in the system
*
*----------------------------------------------------------------------
*/

inline int get_nb_file(){
  return *nbr_file;
}



/**
*----------------------------------------------------------------
*
* Function     : nb_evnements
*
* Result       : int
*
* Parameters   : nothing 
*                    
* Description  :
*
*				return the event number
*
*----------------------------------------------------------------------
*/

inline int nb_evenements(){
  return *nb_evt;;
}



/**
*----------------------------------------------------------------
*
* Function     : get_capacite
*
* Result       : int *
*
* Parameters   : nothing
*                    
* Description  :
*
*				return the adress of the beginning of the capacity table 
*
*----------------------------------------------------------------------
*/

inline int *  get_capacite(){
  return capfile;
}

/**
*----------------------------------------------------------------
*
* Function     : get_capacite
*
* Result       : int 
*
* Parameters   : 
*
* Name         Type      Role
*
* n            int       Queue number
*
* Description  :
*
*			return the capacity of the queue n
*
*----------------------------------------------------------------------
*/

inline int capacite_file(n){
  return *(capfile+n);
  }

/**
*----------------------------------------------------------------
*
* Function     : get_taux
*
* Result       : double 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the probability rate between 0 & 1 of the event i
*
*----------------------------------------------------------------------
*/

inline double get_taux(i){
  return *(P+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_seuil
*
* Result      : double 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       event number
*
* Description  :
*
*       return the threshold of the event
*
*----------------------------------------------------------------------
*/

inline double get_seuil(i){
  return *(R+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_evt_alt
*
* Result       : int 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the alternative event
*
*----------------------------------------------------------------------
*/

inline int get_evt_alt(i){
  return *(A+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_taille_ech
*
* Result       : int 
*
* Parameters   : nothing
*
* Description  :
*
*			return the number of sample
*
*----------------------------------------------------------------------
*/

inline int get_taille_ech(){
  return *nbr_echantillon;
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_VA
*
* Result       : int 
*
* Parameters   : nothing
*
* Description  :
*
*			return the number of antithetic variable
*
*----------------------------------------------------------------------
*/

inline int get_nb_VA(){
  return *nbr_VA;
}


/**
*----------------------------------------------------------------
*
* Function     : get_etat_sup
*
* Result       : int * 
*
* Parameters   : nothing 
*
* Description  :
*
*			return adress of the first element of the table of etats_sup
*
*----------------------------------------------------------------------
*/

inline int * get_etat_sup(){
  return etat_sup;
}

/**
*----------------------------------------------------------------
*
* Function     : get_etat_inf
*
* Result       : int * 
*
* Parameters   : nothing
*
* Description  :
*
*       return the address of the first element of the table of etats_inf
*
*----------------------------------------------------------------------
*/

inline int * get_etat_inf(){
  return etat_inf;
}


/**
*----------------------------------------------------------------
*
* Function     : get_trajectoire
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       stage in the past
*
* Description  :
*
*       return the event corresponding at stage i in the past
*
*----------------------------------------------------------------------
*/

inline int get_trajectoire( int indice){
  return trajectoire[indice];
}


/**
*----------------------------------------------------------------
*
* Function     : get_couplage
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Queue number
*
* Description  :
*
*			function which indicates if a file coupled or not
*			return 0 if the queue number i hasn't been coupled or the log2 of iteration number associated to the coupling
*
*----------------------------------------------------------------------
*/

inline int get_couplage(int num, int i){
  return couplage[num][i];
}

/**
*----------------------------------------------------------------
*
* Function     : get_lg_traj_max
*
* Result       : double  
*
* Parameters   : nothing
*
* Description  :
*
*       return the maximal length of the trajectory 
*----------------------------------------------------------------------
*/

inline double get_lg_traj_max(){
  return *lg_traj_max;
} 

/**
*----------------------------------------------------------------
*
* Function     : get_type_evt
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the type of an event
*
*----------------------------------------------------------------------
*/

inline int  get_type_evt(i){
 return  (evt+i)->typ_evt;
}

/**
*----------------------------------------------------------------
*
* Function     : get_param1
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* j            int       index of the dynamic table of the number of queue
*
* Description  :
*
*			return the number of the file of index j associates the event number i
*       
*----------------------------------------------------------------------
*/

inline int  get_param(i,j){
  return *((evt+i)->param_evt+j); 
}


/**
*----------------------------------------------------------------
*
* Function     : get_numf
*
* Result       : int  
*
* Parameters   : 
*
* Name         Type      Role
*
* i            int       Event number
*
* j            int       index of the dynamic table in the number of queue
*
* Description  :
*
*       return the function number of index assiates at the queue.
*       
*----------------------------------------------------------------------
*/

inline int  get_numf(i,j){
  return *((evt+i)->num_fct+j); 
}

/**
*----------------------------------------------------------------
*
* Function     : get_nbr_file_evt
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the number of origin and destination queue.
*
*----------------------------------------------------------------------
*/

inline int get_nbr_file_evt(i){
  return (evt+i)->nbr_file_evt;
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_ori
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the number of origin queue.
*
*----------------------------------------------------------------------
*/

inline int get_nb_ori(i){
  return (evt+i)->num_ori;
}


/**
*----------------------------------------------------------------
*
* Function     : get_batch_size
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			returns the batch size (default 1)
*
*----------------------------------------------------------------------
*/

inline int get_batch_size(i){
  return (evt+i)->batch_size;
}


/**
*----------------------------------------------------------------
*
* Function     : get_etat_init_infU
*
* Result       : int  
*
* Parameters   : 
*
* Namz          Type      Role
*
* num_file     int       Queue number
*
* Description  :
*
*       return the number of token in the initial inferior state of the queue
*
*----------------------------------------------------------------------
*/

inline int get_etat_init_infU(num_file){
  return *(etat_init_infU+num_file);
}

/**
*----------------------------------------------------------------
*
* Function     : get_etat_init_supU
*
* Result       : int  
*
* Parameters   : 
*
* Nom          Type      Role
*
* num_file     int       Queue number
*
* Description  :
*
*       return the number of token in the initial superior state of the queue
*
*----------------------------------------------------------------------
*/

inline int get_etat_init_supU(num_file){
  return *(etat_init_supU+num_file);
}

/**
*----------------------------------------------------------------
*
* Function     : get_multi_server
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the multiserver queue table
*
*----------------------------------------------------------------------
*/

inline int ** get_multi_server(i){
  return (multis[i].queue_list);
}

/**
*----------------------------------------------------------------
*
* Function     : imprime_etat
*
* Result     : void  
*
* Parameters   : 
*
* Name          Type      Role
*
* etat1        int *     adress of the beginning of the state table
*
* Called functions : get_nb_file
*
* Description  :
*
*			print on the screen the state of each queues
*
*----------------------------------------------------------------------
*/

inline void imprime_etat(int *etat1)
{
  int * etat=etat1;
 int i_boucle;
 int NB_FILE=get_nb_file();
 for (i_boucle = 0; i_boucle < NB_FILE ; i_boucle++)
   printf("%3d ",etat[i_boucle]);
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_trajectories
*
* Result     : int
*
* Parameters   : none
*
*
* Called functions : get_nb_file, get_etat_inf, get_etat_sup
*
* Description  :
*
*		returns the number of trajectories
*
*----------------------------------------------------------------------
*/

inline int get_nb_trajectories()
{
  int * inf = get_etat_inf();
  int * sup = get_etat_sup();
  int NB_FILE=get_nb_file();

  int i;

  int nb_traj=1;

  for (i=0; i<NB_FILE; i++) nb_traj*=(sup[i]-inf[i]+1);

  return nb_traj;
}

/**
*----------------------------------------------------------------
*
* Function     : equal
*
* Result     : int
*
* Parameters   : 
*
* Name          Type      Role
*
* E1       int *     adress of the beginning of the state 1 table
* E2        int *     adress of the beginning of the state 2 table
*
* Called functions : get_nb_file
*
* Description  :
*
*		returns 1 if the states are equal
*
*----------------------------------------------------------------------
*/

inline int equal(int *E1, int *E2)
{
  int NB_FILE=get_nb_file();

  int i;

  for (i=0; i<NB_FILE; i++) if (E1[i]!=E2[i]) return 0;

  return 1;
}


