/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "data.c" /* force le module data �etre inline [MQ] */


/*! \file transition.c
 * \brief Functions of transition.c . 
 * \author  Bernard.Tanzi@imag.fr
 * \author  Jean-Marc.Vincent@imag.fr 
 * \author  Jerome.Vienne@imag.fr
 * \date 2004-2006
 */

/*
*------------------------------------------------------------------
* Fichier      : transition.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme secondaire contenant les fonctions de transitions elementaires 
*   ainsi que la fonction de transition interface avec les fonction elementaires
* fichier pouvant ulterieurement etre eclate  en 2 fichiers 
* une bibliotheque de fonctions de transition et la fonction globale interface elle-meme
*-------------------------------------------------------------------------------
*/

//#define DEBUG 1

/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "transition.h"

#include "data.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/



/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/**********************************************************/
/********* DEFINITIONS DES VARIABLES EXPORTEES **************/
/**********************************************************/
extern double **table_index;
extern struct st_evt *evt;
extern struct st_multi *multis;

/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : arrivee_ext_deb_rejet
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* nb_dest   int       destination Queue number associate with event
*
* par       int *     pointer on the destination queue number table
*
*
* Called function : get_capacite
*
* Description  :
*
*      function exterior arrived on a queue
*	   starting from the destination queue number vector state 
*      and with destination queue number table
*      return vector state updated
*      
*----------------------------------------------------------------------
*/

int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k=1;

//  imprime_etat(capacite); printf("\n");
//  imprime_etat(etat); printf("\n");

  //printf("Nombre de destinataire %d", nb_dest);
  while ((etat[*(par+k)] == capacite[*(par+k)]) && k<(nb_dest-2))
    /** parcours des files destinations
	tant que la file destination est pleine on passe a la suivante */
    k++;
  /* on verifie si la derniere file est pleine  */
  if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
    /** toutes les files destinations sont pleines on ne fait rien */
    ;
  else
    /** sinon on ajoute dans la file destination */
    etat[*(par+k)]=etat[*(par+k)]+1;     
  /** on renvoie l'adresse du vecteur d'etat */

//  imprime_etat(etat); printf("\n");

  return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Function     : sortie_ext
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* numFile   int       queue number
*
* Description  :
*
*      function exit to a queue from exterior
*	   starting from the destination queue number vector state 
*	   return an integer giving the queue client remaining in the queue 
*      
*----------------------------------------------------------------------
*/

int sortie_ext (int etat[],int numFile){ 
   if (etat[numFile] > 0 ) 
     etat[numFile]=etat[numFile]-1;
   return etat[numFile];
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_rejet
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* nb_dest   int      destination+origin queue number associate with event
*
* par       int *     pointer on the origin, destination number table
*
* Called function : get_capacite,get_nb_file
*
* Description  :
*  
*      routage avec debordement sur ne files  avec rejet
*      fonction de routage d'une file origine vers n files destination 
*  au cas ou toutes les files destinations sont pleines, le paquet emis est perdu 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  
  if (etat[*par] > 0){
      k=1;
      etat[*par]=(etat[*par]-1);
      while (k<(nb_dest-1) && etat[*(par+k)] == capacite[*(par+k)]) 
	k++;
      /* on verifie si la derniere file est pleine  */
      if( k == (nb_dest-1) && etat[*(par+k)] == capacite[*(par+k)])
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
      else
		etat[*(par+k)]=etat[*(par+k)]+1;     
    }
   return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_bloc
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adresse de debut du vecteur d'etat
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*      routage avec debordemetn avec rejet
*      fonction de routage d'une file origine vers une premiere file destination et si elle
* est pleine vers une deuxieme file destination 
*  au cas ou la deuxieme file destination  est pleine le paquet emis est perdu
*      a partir du vecteur d'etat d'un numero de file origine et d'un
*     numero de 1ere file destiantaire d'un numero de deuxieme file destinataire 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_bloc (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  

  if (etat[*par] > 0)
    {
      k=1; 
      while (etat[*(par+k)] == capacite[*(par+k)] && k<(nb_dest-2))
	k++;
      if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
	else
	  {
	    etat[*par]=(etat[*par]-1);
	    etat[*(par+k)]=etat[*(par+k)]+1; 
	  }    
    }
  return (&etat[0]);
  }


/**
*----------------------------------------------------------------
*
* Fonction     : JSQ_rejet
*
* Resultat     : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  JSQ_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int num_file_min;
  int k=1;
  
  /* file origine != -1*/
  if(*par != -1){
  	
        if( etat[*par] == 0){
		return (&etat[0]);
	} else {	
		etat[*par]-=1;
		// On cherche la premiere file non pleine et on verifie que l'on ne depasse pas nbdest
    		while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    		}

            	// num_file_min prend la valeur de la 1ere file non pleine trouv
            	num_file_min=*(par+k);
		k++;
		while(k<(nb_dest-1)){		
                    if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
			num_file_min=*(par+k);
		    k++;
		} ;
		if(etat[num_file_min] < capacite[num_file_min]){
			etat[num_file_min]+=1;
		}
	}	return (&etat[0]);
  } else {
  	/* File d'origine == -1 */
	while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    	}

        // num_file_min prend la valeur de la 1ere file non pleine trouv
        num_file_min=*(par+k);
	k++;
	while(k<(nb_dest-1)){		
        if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
		num_file_min=*(par+k);
		k++;
	} ;
	if(etat[num_file_min] < capacite[num_file_min]){
		etat[num_file_min]+=1;
	}
	return (&etat[0]);
  }
}



/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_index(int etat[],int nb_dest,int * par, int * numf){ 
  
  int *  capacite = get_capacite();
  int num_file_min, j;
  int k=1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  
   // We look at the first not empty queue in the list of destination 
   while (k<nb_dest-1 && *(par+k)!=-1 &&!(etat[*(par+k)] < capacite[*(par+k)]) ){
		k++;
   }
   
   // num_file_min has the value of the first not empty queue
   num_file_min=*(par+k);
   j=k;	
   while(k<nb_dest){
				
	if (  *(par+k)!=-1 && (table_index[*(numf+k)][etat[*(par+k)]] < table_index[*(numf+j)][etat[num_file_min]]) && (etat[*(par+k)] < capacite[*(par+k)]) ){
		num_file_min=*(par+k);
		j=k;
	}
	if( *(par+k)== -1 && (table_index[*(numf+k)][0] < ( (table_index[*(numf+j)][etat[num_file_min]]) +1)  ) ){
		num_file_min = -1;
	}
	k++;
   };
   
   //Check if the queue is really not empty
   if( (num_file_min != -1) && etat[num_file_min] < capacite[num_file_min] ){
	etat[num_file_min]=etat[num_file_min]+1;
   }
   
   return (&etat[0]); 
}

/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_multi_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf, int ** tab){ 
  
  int *  capacite = get_capacite();
  int i, j;
  int Setat=0, Scapa=0;
  int nb_file;
  double val_min=100000000;
  int file_min=-1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  //printf("\n\n\n");
  
  for(i=1; i<nb_dest;i++){
	Setat=0;
	Scapa=0;
	nb_file = tab[i][0];
	//printf("Il y a %d file sur le serveur %d\n", nb_file, i);
	
	//si on n'est pas avec la file -1
	if(nb_file != -1){
		for (j=1; j<(nb_file+1); j++){
		    //printf("File: %d en %d, %d\n", tab[i][j], i, j);
			// on accumule le nb de client
			//printf("On ajoute %d a Setat (file %d)\n", etat[tab[i][j]], tab[i][j]);
			Setat += etat[tab[i][j]]; 
			// on accumule le nb de capa
			//printf("On ajoute %d a Scapa (file %d)\n", capacite[tab[i][j]], tab[i][j]);
			Scapa += capacite[tab[i][j]];
		}
		
		if(Setat<Scapa){
			// compare valeur min
			// Si plus petit, on change et on garde
			//printf("On a file = %d Se<Sc, valeur d'index %lf\n", *(numf+i), table_index[*(numf+i)][Setat]); 
			if(table_index[*(numf+i)][Setat]<val_min){
				val_min = table_index[*(numf+i)][Setat];
				//printf("On a file_min = %d\n", *(numf+i));
				file_min = i;
			}
		}
	} else {
		//printf("On est en -1 et on a numf+i = %d\n", *(numf+i));
		if(table_index[*(numf+i)][0]<val_min)
			file_min = -1;
	}
	
  }
  
  //printf("On a file_min: %d \n", file_min);
  
  // on a trouv le multi-serveur  index min
  // Maintenant, on va ajout le client
  if( file_min != -1){
	nb_file = tab[file_min][0];
	
	for (j=1; j<(nb_file+1); j++){
		if (etat[tab[file_min][j]] < capacite[tab[file_min][j]]){
			//printf("On va incrmenter la file %d\n", tab[file_min][j]);
			etat[tab[file_min][j]] +=1;
			return (&etat[0]); 
		}
	}
  }else {
	return (&etat[0]); 
  }
  
  printf("Pb ds multi_serveur index");
  return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_multi_serveur
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_multi_serveur (int etat[],int nb_dest,int * par){ 

int *  capacite = get_capacite();
int i=2;
// If there are 1 client or more in the "cache" queue, we extrat one of them 
if (etat[*par]!=0 && etat[*par+1]!=0 ){
    etat[*par]-=1;
// else we check if there is a client in the queue
}else{
    // If It's the case, we extract the client from the queue
    if (etat[*par]==0 && etat[*(par+1)] !=0){
        etat[*(par+1)]-=1;
    } else { 
		if (etat[*par]!=0 && etat[*(par+1)] ==0){
			 etat[*par]-=1;
			   etat[*(par+1)]+=1;
			return (&etat[0]);
		}else{
			return (&etat[0]);
		}
	}
}
// On parcours la liste des distantion, on fait +1 à la 1ere destination non pleine


// if the destination queue is not -1 & i < nb_dest
while(i< nb_dest && *(par+i)!=-1){
	if(etat[*(par+i)] < capacite[*(par+i)]){
		etat[*(par+i)]+=1;
		i=nb_dest;
	}
	i++;
}
// if all queues are full, there is rejection of client
return (&etat[0]);

}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_call_center
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* nb_ori	int		number of origin queue
* 
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_call_center (int etat[],int nb_dest,int * par, int nb_ori){ 

	int i=0;
	int *  capacite = get_capacite();

	
	
	//On cherche la premiere file non vide
	while (i < nb_ori && etat[*(par+i)]==0){
		i++;
	} 
	
	
	//Test au cas si on est sur une file origine et non -1 (i =nb_ori)
	if( *(par+i)!=-1 ){
			//test pour eviter erreur dut a x: (alors qu'il faut x : sinon nb_ori =0) pour eviter de vider une file vide
	    if (capacite[*(par+i)]<=0) {
			printf("Problem in description file !\n");
		} else {
			//printf("On vide %d de capacite %d \n", *(par+i), etat[*(par+i)]);
			etat[*(par+i)]-=1;
		}
	}
		
	return (&etat[0]);
	

}


/**
*----------------------------------------------------------------
*
* Function     : negative_customer
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etatInf   int []    address of the begining of state Inf vector
*
* etatSup   int []    address of the begining of state Sup vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (>= 0)
*
*
* Called function :
*
* Description  :
*  
*      Negative customer with routing 
*             
*      
*      
*----------------------------------------------------------------------
*/

int  negative_customer(int etatInf[], int etatSup[], int *par){ 

  int source = *par;
  int dest = *(par+1);

  if (dest == -1) {
	printf("Error: negative customer destination cannot be exterior.\n");
  } 

  if (etatSup==NULL) {
	/* negative customer external arrival (source == -1) or impatience (source == dest) */
  
  	if ((source == -1) || (source==dest)){ //simple departure
  		if (etatInf[dest] > 0) etatInf[dest] = etatInf[dest]-1;
  	}

  	else { /* negative customer routing (source != dest) */
		if (etatInf[source] && etatInf[dest] > 0 ) etatInf[dest]--;
		if (etatInf[source] > 0) etatInf[source]--;
	}

  }
  else{

  	/* negative customer external arrival (source == -1) or impatience (source == dest) */
  
  	if ((source == -1) || (source==dest)){ //simple departure
  		if (etatInf[dest] > 0) etatInf[dest] = etatInf[dest]-1;
		if (etatSup[dest] > 0) etatSup[dest] = etatSup[dest]-1;
  	}

  	else { /* negative customer routing (source != dest) */

  		if (etatInf[source]>0) { /* negative customer in both Inf and Sup state */
			etatInf[source]=etatInf[source]-1;
			if (etatInf[dest]>0) etatInf[dest]=etatInf[dest]-1;

			etatSup[source]=etatSup[source]-1;
			if (etatSup[dest]>0) etatSup[dest]=etatSup[dest]-1;
  		}
  		else if (etatSup[source]==0); /* negative customer in neither Inf nor Sup state */
  		else { /* negative customer only in Sup state */
		       /* the only case that breaks the monotonicity property */
			etatSup[source] = etatSup[source]-1;
			if (etatInf[dest] < etatSup[dest]) etatSup[dest] = etatSup[dest]-1;
			else if (etatInf[dest] > 0) { 
				etatInf[dest] = etatInf[dest]-1;
			}
  		}
  	}
  }

  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : batch
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etatInf   int []    address of the begining of state Inf vector
*
* etatSup   int []    address of the begining of state Sup vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (-1 if exterior)
*
* size	    int      batch size
*
* Called function :
*
* Description  :
*  
*      Batch arrival/routing/service (non decomposable)
*             
*      
*      
*----------------------------------------------------------------------
*/

int batch(int etatInf[], int etatSup[], int *par, int size){ 

  int source = *par;
  int dest = *(++par);
  int tmp;
  int *  capacite = get_capacite();
  int flagInf=0;

  if (etatSup==NULL) {

	if (source != -1) {
		//Check if the queue has at least size clients
		if (etatInf[source]>=size){
			//If yes, size clients leave the original queue
			etatInf[source]-=size;
		} 
		else {
	    		// If the queue has less than size clients, we return the original state
			return (0);
		}
  	} 

  	if ((dest != -1) && (etatInf[dest] + size <= capacite[dest])) etatInf[dest]+=size;
  }
  else {
	//If the first queue is different of -1
  	if(source != -1){
		//Check if the Sup queue has at least size clients 
		if (etatSup[source]>=size){ //If it's not empty, a client leave the original queue
                        if (etatInf[source]>= size) {
				etatInf[source]-=size;
				etatSup[source]-=size;
			}
			else {
				etatInf[source]=0; 
				etatSup[source] = size-1;
				flagInf = 1;  //There is at least one state for which there is no batch
			} 
		}
		else {
	    	// If the Sup queue doesn't have at least size clients, we return the original states
			return (0);
		}
  	}

        if (dest != -1){

        	if (etatSup[dest] + size <= capacite[dest]) {
			if (flagInf==0) etatInf[dest]+=size;
			etatSup[dest]+=size;
  		}
  		else if (etatInf[dest] + size > capacite[dest]) {}
  		else {
			if (flagInf==0) {
				etatInf[dest]+=size;
				tmp=capacite[dest]-size+1;
				if (tmp<etatInf[dest]) etatInf[dest]=tmp;
			}
			etatSup[dest]=capacite[dest];
  		}
	}
  }
  
  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : inf_batch
*
* Result       : int
*
* Parameters   : 
*
* Name      Type      Role
*
* x         int       inf state
* y         int       sup state 
* C	    int       buffer capacity
* k         int       batch size
*
* Description  :
*
*     Returns the new inf state
*
*----------------------------------------------------------------------
*/

int inf_batch(int x, int y, int C, int k){

	#ifdef DEBUG 
		printf("(inf) batch size=%d\n", k);
	#endif
	if (y+k <= C) return x+k;
	else if (x+k > C) return x;
	else if (x+k <= C-k+1) return x+k;
	else return C-k+1; 
}

/**
*----------------------------------------------------------------
*
* Function     : sup_batch
*
* Result       : int
*
* Parameters   : 
*
* Name      Type      Role
*
* x         int       inf state
* y         int       sup state 
* C	    int       buffer capacity
* size      int       batch size
*
* Description  :
*
*     Returns the new sup state
*
*----------------------------------------------------------------------
*/

int sup_batch(int x, int y, int C, int k){
	
	if (y+k <= C) return y+k;
	else if (x+k > C) return y;
	else return C; 
}


/**
*----------------------------------------------------------------
*
* Fonction     :  batch_index_routing
*
* Resultat     : int 
*
* Parameters   : 
*
* Name		Type	Role
*
* etatInf	int []    address of the begining of state Inf vector
*
* etatSup	int []    address of the begining of state Sup vector
* 
* nb_dest	int	number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contains the number of the origin & destination queue
*
* numf		int*	pointer of the table which contains the information on index functions to be used with each queue	
*
* size		int     batch size
* 
* Function called  : get_capacite, get_nb_file
*
* Description  :
*  
*   batch index routing with rejection if the batch cannot be accepted due to the finite capacity (no partial acceptance) 
*   
*      
*----------------------------------------------------------------------
*/

int batch_index_routing(int etatInf[], int etatSup[], int nb_dest, int * par, int * numf, int size){ 
  
  int *  capacite = get_capacite();
  int minInf, minSup, jMinInf, jMinSup;
  int k;
  int x, y, C;
  int b, flag, val, i, j, tmp;
  int *etatInfOld, *etatSupOld;
  int nbfile = get_nb_file();
  int flagInf = 0;

  if (etatSup==NULL) { 

	#ifdef DEBUG 
		printf("BIR: one state\n");
	#endif
	
	//Create copy of Inf 
	etatInfOld = (int *) calloc(nbfile, sizeof(int));
	
	for (i=0; i<nbfile; i++) {
		etatInfOld[i] = etatInf[i];
	}

	//If the first queue is different of -1
  	if(*par != -1){
		//Check if the queue has at least size clients
		if (etatInfOld[*par]>=size){
	    	//If yes, size clients leave the original queue
			etatInf[*par]-=size;
		} else {
	    	// If the queue has less than size clients, we return the original state
			return (0);
		}
  	}
     
   	// minInf has the value of the first queue with minimal value of index function
   	k=1;
	minInf=*(par+k);
   	jMinInf=k;	
   	while(k<nb_dest){
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] < table_index[*(numf+jMinInf)][etatInfOld[minInf]]) ){
			minInf=*(par+k);
			jMinInf=k;
		}

		if (  *(par+k)==-1 ) printf("Warning: batch index routing ignores -1 as destination\n");
		k++;
   	};
   
   	//Check if the queue is really not empty
   	if( (minInf != -1) && etatInfOld[minInf] + size <= capacite[minInf] ){
		etatInf[minInf]=etatInfOld[minInf]+size;
   	}

	free(etatInfOld);
   }
   else{
	#ifdef DEBUG 
		printf("BIR: (Inf, Sup)\n");
	#endif

	//Create copies of Inf and Sup
	etatInfOld = (int *) calloc(nbfile, sizeof(int));
	etatSupOld = (int *) calloc(nbfile, sizeof(int));

	for (i=0; i<nbfile; i++) {
		etatInfOld[i] = etatInf[i];
		etatSupOld[i] = etatSup[i];
	}

	
	//If the first queue is different of -1
  	if(*par != -1){
		//Check if the Sup queue has at least size clients 
		if (etatSupOld[*par]>=size){ //If it's not empty, a client leave the original queue
                        if (etatInfOld[*par]>= size) {
				etatInf[*par]-=size;
				etatSup[*par]-=size;
			}
			else {
				etatInf[*par]=0; 
				etatSup[*par] = size-1;
				flagInf = 1;  //There is at least one state for which there is no batch
			} 
		}
		else {
	    	// If the Sup queue doesn't have at least size clients, we return the original states
			return (0);
		}
  	}

        // minInf (minSup) has the value of the first queue with minimal value of index function for state Inf (Sup)
   	k=1;
	minInf=*(par+k);
	minSup=*(par+k); 
   	jMinInf=k; 
	jMinSup=k;
	while(k<nb_dest){
		//Inf		
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] < table_index[*(numf+jMinInf)][etatInfOld[minInf]]) ){
			minInf=*(par+k);
			jMinInf=k;
		}
		
		//Sup
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatSupOld[*(par+k)]] < table_index[*(numf+jMinSup)][etatSupOld[minSup]]) ){
			minSup=*(par+k);
			jMinSup=k;
		}
		
		if (  *(par+k)==-1 ) printf("Warning: batch index routing ignores -1 as destination\n");
		k++;
   	};

	

	#ifdef DEBUG 
		printf("Inf: %d Sup: %d\n", minInf, minSup);
	#endif

/*	//First case: indexInf[x] >= indexSup[minSup], for all x
	if (table_index[*(numf+jMinInf)][etatInfOld[minInf]] >= table_index[*(numf+jMinSup)][etatSupOld[minSup]]){
		#ifdef DEBUG 
			printf("case 1\n");
		#endif
		x = etatInfOld[minSup]; y = etatSupOld[minSup];
		C = capacite[minSup];
		etatInf[minSup] = inf_batch(x, y, C, size);
		etatSup[minSup] = sup_batch(x, y, C, size);
	}
	else{
		#ifdef DEBUG 
			printf("case 2\n");
		#endif*/
		k=1;
		while(k<nb_dest){
			if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] <= table_index[*(numf+jMinSup)][etatSupOld[minSup]]) ) {
/*				#ifdef DEBUG 
					printf("file: %d\n", *(par+k));
					printf("Sup\n");
				#endif*/

				//Sup
				if (k==jMinSup) {
/*					#ifdef DEBUG 
						printf("case a\n");
					#endif*/
					etatSup[minSup] = sup_batch(etatInfOld[minSup], etatSupOld[minSup], capacite[minSup], size);
				}
				else {
/*					#ifdef DEBUG 
						printf("case b\n");
					#endif*/
					b = 0; flag=1;
					while (flag){
						b++;
/*					 	#ifdef DEBUG 
							printf("b=%d, x=%d, y(min)=%d\n",b, etatInf[*(par+k)]+b, etatSup[minSup]);
						#endif*/
						if ((etatInfOld[*(par+k)]+b > etatSupOld[*(par+k)]) || (table_index[*(numf+k)][etatInfOld[*(par+k)]+b] > table_index[*(numf+jMinSup)][etatSupOld[minSup]])){
							flag=0;
							b--;
						}	
					}
					etatSup[*(par+k)] = sup_batch(etatInfOld[*(par+k)], etatInfOld[*(par+k)]+b, capacite[*(par+k)], size);
					if (etatSup[*(par+k)] < etatSupOld[*(par+k)]) etatSup[*(par+k)] = etatSupOld[*(par+k)];
				}
/*				#ifdef DEBUG 
					printf("Inf\n");
				#endif*/

				//Inf
				if (k==jMinInf && flagInf==0) {
					j = 1; 
					//second smallest
					if (jMinInf!=1) val = table_index[*(numf+1)][etatInfOld[*(par+1)]];
					else val = table_index[*(numf+2)][etatInfOld[*(par+2)]];
					while(j<nb_dest){
						if (j!=jMinInf && table_index[*(numf+j)][etatInfOld[*(par+j)]] < val){
							val = table_index[*(numf+j)][etatInfOld[*(par+j)]];
						}
						j++;
					}					
					if (table_index[*(numf+jMinInf)][etatSupOld[*(par+jMinInf)]] <= val) {
						etatInf[minInf] = inf_batch(etatInfOld[minInf], etatSupOld[minInf], capacite[minInf], size);
						#ifdef DEBUG
							printf("inf(inf=%d, sup=%d, cap=%d, size=%d)=%d\n", etatInfOld[minInf], etatSupOld[minInf], capacite[minInf], size, etatInf[minInf]);
						#endif 
					}
					else {
						b = 0; flag=1;
						while (flag){
							b++;
					 		if (table_index[*(numf+jMinInf)][etatInfOld[*(par+jMinInf)]+b] > val){
								flag=0;
								b--;
							}	
						}
						tmp = inf_batch(etatInfOld[*(par+jMinInf)], etatInfOld[*(par+jMinInf)]+b, capacite[*(par+jMinInf)], size);
						if (tmp <= etatInfOld[*(par+jMinInf)] + b + 1) etatInf[*(par+jMinInf)] = tmp;
						else etatInf[*(par+jMinInf)] = etatInfOld[*(par+jMinInf)] + b + 1;
						#ifdef DEBUG
							printf("inf=%d\n", etatInf[minInf]);
						#endif
					}
				}
			}
			k++;
		}
		free(etatInfOld);
		free(etatSupOld);
//	}
   }
	
   return (0); 
}

/**
*----------------------------------------------------------------
*
* Function     : dec_batch (monotonic)
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat	    int []    address of the begining of state vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (-1 if exterior)
*
* size	    int      batch size
*
* Called function :
*
* Description  :
*  
*      Decomposable batch arrival/routing/service
*             
*      
*      
*----------------------------------------------------------------------
*/

int dec_batch(int etat[], int *par, int size){ 

  int source = *par;
  int dest = *(++par);
  int tmp;
  int *  capacite = get_capacite();

  if (source != -1) {
	if (etat[source] - size >= 0) etat[source]-=size;
        else {
		size = etat[source]; 
		etat[source] = 0;
	}
  } 

  if (dest != -1) {
	if (etat[dest] + size <= capacite[dest]) etat[dest]+=size;
  	else etat[dest] = capacite[dest];
  }
  
  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : transition_interface
*
* Result     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of vector etat
*
* event		int		  event
*
* fonctions appelees : get_type_evt,arrivee_ext,sortie_ext,routage_rejet,routage_blocage,routage_debord_bloc,routage_debord_rejet
*
* Description  :
* 
*      en fonction des parametres route vers la fonction de transition adhoc
*      et met a jour le vecteur d'etat pour les files correspondantes
*     
*      
*----------------------------------------------------------------------
*/

void transition_interface(etat,evenement)
     int etat[];
     int evenement;
{
   int **tab;
   int nbdest; 
   static int *par = NULL;
   static int *numf = NULL;
   int type_evt;
   int nb_ori; 
   type_evt=get_type_evt(evenement);
   nbdest=get_nbr_file_evt(evenement);
   int m;
   int batch_size;
   
   par=malloc(nbdest*sizeof(int));
   
   for(m=0;m<nbdest;m++){
      *(par+m)= get_param(evenement,m);
   }
   
   
   //printf(" Evt: %d \n", evenement);
   switch(type_evt){   
	   case 1: /* sortie externe */
		   etat[*par] = sortie_ext(etat,*par); 
  		   break;

	   case 2: /* arrivee externe */
   		   /* caracteristique 1er parametre numero de file = -1 dernier parametre numero de file = -1 */
      		   arrivee_ext_deb_rejet(etat,nbdest,par); 
   		   break;

	   case 3: /* When a client leave a Multi-server network */
      		   Depart_multi_serveur(etat, nbdest, par);
	   	   break;

	   case 4: /* Join the shortest queue arrivee avec rejet */
		   JSQ_rejet(etat,nbdest,par);
 	   	   break;
  
	   case 5: /* Index avec rejet */
		   numf=malloc((nbdest+1)*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   Arrivee_rejet_index(etat, nbdest, par, numf);
		   free(numf);
 	   	   break;
  
	   case 6: /* routage vers n files avec debordement avec rejet */
      		   /* caracteristique 1er parametre numero de file != avant dernier param numero de file  et dernier parametre numero de file == -1 */
      		   //printf("ROUTAGE nbdest : %d *par %d \n",nbdest,*par);
      		   routage_nfile_rejet(etat,nbdest,par);
 	   	   break;
  
	   case 7: /* routage vers n files avec debordement avec blocage */
      		   /* caracteristique 1er parametre numero de file == avant dernier param numero de file et dernier parametre numero de file == -1 */
      		   routage_nfile_bloc(etat,nbdest,par);
 	   	   break;
  
	   case 8: /* Arrive dans un multi serveur avec index */
		   numf=malloc(nbdest*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   
		   tab = get_multi_server(evenement);	
		   Arrivee_rejet_multi_index(etat, nbdest, par, numf, tab);
		   free(numf);
 	   	   break;
  
	   case 9: /* depart call center */
		   nb_ori= get_nb_ori(evenement);
	  	   Depart_call_center(etat, nbdest, par, nb_ori);
 	   	   break;

           case 12:  /*decomposable batch*/
		   batch_size = get_batch_size(evenement);
		   dec_batch(etat, par, batch_size);
		   break;
  
	   default : /*On a reconnu aucun type */
		   printf("erreur de donnee dans le fichier d'import veuillez controler ce fichier \n"); 
		   exit(0);
   }
   free(par);
  
   //printf("Nouvel etat =: %d \n",etat[0]); 
}

/**
*----------------------------------------------------------------
*
* Function     : transition_interface_envelope
*
* Result     : int *
*
* Parameters   : 
*
* Name         	Type      	Role
*
* etatInf	int []    	current infimum state
*
* etatSup	int[]		current supremum state
*
* event		int		event
*
* fonctions appelees : get_type_evt,arrivee_ext,sortie_ext,routage_rejet,routage_blocage,routage_debord_bloc,routage_debord_rejet
*
* Description  :
* 
*      en fonction des parametres route vers la fonction de transition adhoc
*      et met a jour les vecteurs d'etat inf et sup pour les files correspondantes
*
* Difference compared to 'transition_interface' function: 'transition_interface_envelope' can be used for non-monotone events
*
*	- for a monotone event: calls corresponding transition function twice (once for 'etatInf' and once for 'etatSup')
*	- for a non-monotone event: returns the envelope for the image of the transition function - new 'etatInf' and 'etatSup' 
*				    (not necessarily reachable by the event!)
*     
*      
*----------------------------------------------------------------------
*/

void transition_interface_envelope(etatInf,etatSup,evenement)
     int etatInf[], etatSup[];
     int evenement;
{
   int **tab;
   int nbdest; 
   static int *par = NULL;
   static int *numf = NULL;
   int type_evt;
   int nb_ori; 
   type_evt=get_type_evt(evenement);
   nbdest=get_nbr_file_evt(evenement);
   int m;
   int batch_size;
   
   par=malloc(nbdest*sizeof(int));
   
   for(m=0;m<nbdest;m++){
      *(par+m)= get_param(evenement,m);
   }
   
   
   //printf(" Evt: %d \n", evenement);
   switch(type_evt){   
	   case 1: /* sortie externe */
		   #ifdef DEBUG
			if (etatSup!=NULL) printf("Service (%ld): (%ld, %ld) -> ", *par, etatInf[*par], etatSup[*par]);
			else printf("Service (%ld): %ld -> ", *par, etatInf[*par]);
		   #endif

		   etatInf[*par] = sortie_ext(etatInf,*par);
		   if (etatSup!=NULL) etatSup[*par] = sortie_ext(etatSup,*par);

                   #ifdef DEBUG
			if (etatSup!=NULL) printf("(%ld, %ld)\n", etatInf[*par], etatSup[*par]); 
			else printf("%ld\n", etatInf[*par]); 
  		   #endif

		   break;

	   case 2: /* arrivee externe */
   		   /* caracteristique 1er parametre numero de file = -1 dernier parametre numero de file = -1 */
		   arrivee_ext_deb_rejet(etatInf,nbdest,par); 
		   if (etatSup!=NULL) arrivee_ext_deb_rejet(etatSup,nbdest,par);

   		   break;

	   case 3: /* When a client leaves a Multi-server network */
      		   Depart_multi_serveur(etatInf, nbdest, par);
		   if (etatSup!=NULL) Depart_multi_serveur(etatSup, nbdest, par);
	   	   break;

	   case 4: /* Join the shortest queue arrivee avec rejet */
		   JSQ_rejet(etatInf,nbdest,par);
		   if (etatSup!=NULL) JSQ_rejet(etatSup,nbdest,par);
 	   	   break;
  
	   case 5: /* Index avec rejet */
		   numf=malloc(nbdest*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   Arrivee_rejet_index(etatInf, nbdest, par, numf);
		   if (etatSup!=NULL) Arrivee_rejet_index(etatSup, nbdest, par, numf);
		   free(numf);
 	   	   break;
  
	   case 6: /* routage vers n files avec debordement avec rejet */
      		   /* caracteristique 1er parametre numero de file != avant dernier param numero de file  et dernier parametre numero de file == -1 */
      		   //printf("ROUTAGE nbdest : %d *par %d \n",nbdest,*par);
      		   routage_nfile_rejet(etatInf,nbdest,par);
		   if (etatSup!=NULL) routage_nfile_rejet(etatSup,nbdest,par);

 	   	   break;
  
	   case 7: /* routage vers n files avec debordement avec blocage */
      		   /* caracteristique 1er parametre numero de file == avant dernier param numero de file et dernier parametre numero de file == -1 */
      		   routage_nfile_bloc(etatInf,nbdest,par);
		   if (etatSup!=NULL) routage_nfile_bloc(etatSup,nbdest,par);
 	   	   break;
  
	   case 8: /* Arrive dans un multi serveur avec index */
		   numf=malloc(nbdest*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   
		   tab = get_multi_server(evenement);	
		   Arrivee_rejet_multi_index(etatInf, nbdest, par, numf, tab);
		   if (etatSup!=NULL) Arrivee_rejet_multi_index(etatSup, nbdest, par, numf, tab);
		   free(numf);
 	   	   break;
  
	   case 9: /* depart call center */
		   nb_ori= get_nb_ori(evenement);
	  	   Depart_call_center(etatInf, nbdest, par, nb_ori);
		   if (etatSup!=NULL) Depart_call_center(etatSup, nbdest, par, nb_ori);
 	   	   break;

	   case 10: /*negative customer*/
                   if (nbdest!=2) printf("Error: negative customer event associated with %d queues.\n", nbdest);
		   negative_customer(etatInf, etatSup, par);
  		   break;

	   case 11: /*batch*/
		   batch_size = get_batch_size(evenement);  

		   #ifdef DEBUG
			if (etatSup!=NULL) printf("Batch arrival (%ld, +%ld): (%ld, %ld) -> ", *(par+1), batch_size, etatInf[*(par+1)], etatSup[*(par+1)]);
			else printf("Batch arrival (%ld, -%ld): %ld -> ", *(par+1), batch_size, etatInf[*par]);
		   #endif

		   batch(etatInf, etatSup, par, batch_size);
		   
		   #ifdef DEBUG
		   	if (etatSup!=NULL) printf("(%ld, %ld)\n", etatInf[*(par+1)], etatSup[*(par+1)]);
			else printf("%ld\n", etatInf[*(par+1)]);
		   #endif

		   break;

 	   case 12: /*decomposable batch*/
		   batch_size = get_batch_size(evenement);
		   dec_batch(etatInf, par, batch_size);
		   if (etatSup!=NULL) dec_batch(etatSup, par, batch_size);
		   break;

	   case 13: /*batch index routing*/
		   batch_size = get_batch_size(evenement);
 		   numf=malloc(nbdest*sizeof(int));
		   for(m=1;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		 
		   #ifdef DEBUG
			printf("Before - Batch index routing (size = %ld): ", batch_size);
			if (etatSup==NULL) imprime_etat(etatInf);
			else {
				printf("Inf:"); 
				imprime_etat(etatInf); 
				printf(", Sup:"); 
				imprime_etat(etatSup);
			}
			printf("\n"); 
		   #endif

		   batch_index_routing(etatInf, etatSup, nbdest, par, numf, batch_size);

		   #ifdef DEBUG
			printf("After - Batch index routing (size = %ld): ", batch_size);
			if (etatSup==NULL) imprime_etat(etatInf);
			else {
				printf("Inf:"); 
				imprime_etat(etatInf); 
				printf(", Sup:"); 
				imprime_etat(etatSup);
			}
			printf("\n\n"); 
		   #endif
		   break;

	   default : /*On a reconnu aucun type */
		   printf("erreur de donnee dans le fichier d'import veuillez controler ce fichier \n"); 
		   exit(0);
   }
   free(par);
  
   //printf("Nouvel etat =: %d \n",etat[0]); 
}




 
