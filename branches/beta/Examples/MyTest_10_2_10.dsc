#Number of queues
10
#Number of call types
2
#Queue capacity
1 1 1 1 1 1 1 1 1 1 
#Priority file
File: ../Examples/Priority_10_2_10.txt
#queue_minimal_initial_state
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
#Max file -N for no max file
File: ../Examples/Etats_10_2_10.txt
#Number_of_events
11
#Index file - N for No index file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0	11  2.0 2.0 12 -1 : 0 1 2 3 4 5 6 7 8 9  -1
1	10  6.0 6.0 2 0 : -1
2	10  6.0 6.0 2 1 : -1
3	10  6.0 6.0 2 2 : -1
4	10  6.0 6.0 2 3 : -1
5	10  6.0 6.0 2 4 : -1
6	10  6.0 6.0 2 5 : -1
7	10  6.0 6.0 2 6 : -1
8	10  6.0 6.0 2 7 : -1
9	10  6.0 6.0 2 8 : -1
10	10  6.0 6.0 2 9 : -1
