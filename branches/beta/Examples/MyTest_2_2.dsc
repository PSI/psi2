#Number of queues
2
#Number of call types
2
#Queue capacity
1 1
#Priority file
File: ../Examples/Priorite_2_2.txt
#queue_minimal_initial_state
0 0 0 0
#Max file -N for no max file
File: ../Examples/Etats_2_2.txt
#Number_of_events
3
#Index file - N for No index file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0	11  2.0 2.0 4 -1 : 0 1 -1
1	10  2.0 2.0 2 0 : -1
2	10  2.0 2.0 2 1 : -1
