#Number of queues
10
#Number of call types
2
#Queue capacity
1 1 1 1 1 1 1 1 1 1 
#Priority file
File: ../Examples/Priority_10_2_10.txt
#queue_minimal_initial_state
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
#Max file -N for no max file
File: ../Examples/Etats_10_2_10.txt
#Number_of_events
11
#Index file - N for No index file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0	11  2.0 2.0 3 -1 : 0  -1
1	12  6.0 6.0 3 0 : 1  -1
2	12  6.0 6.0 3 1 : 2 -1
3	12  6.0 6.0 3 2 : 3 -1
4	12  6.0 6.0 3 3 : 4 -1
5	12  6.0 6.0 3 4 : 5 -1
6	12  6.0 6.0 3 5 : 6 -1
7	12  6.0 6.0 3 6 : 7 -1
8	12  6.0 6.0 3 7 : 8 -1
9	12  6.0 6.0 3 8 : 9 -1
10	10  6.0 6.0 2 9 : -1
