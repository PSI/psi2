#Number of queues
14
#Number of call types
3
#Queue capacity
1 1 1 1 1 1 1 1 1 1 1 1 1 1 
#Priority file
File: ../Examples/Priority_14_3_14.txt
#queue_minimal_initial_state
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 
#Max file - N for No max file
File: ../Examples/Etats_14_3_14.txt
#Number_of_events
17
#Index file - N for No index file
File: N

#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0  11 6.0 0.0 0.0 10 -1  :  0 1 6 7 8 9 12 13  -1
1  11 0.0 6.0 0.0 10 -1  :  2 3 6 7 10 11 12 13 -1
2  11 0.0 0.0 6.0 10 -1  :  4 5 8 9 10 11 12 13 -1
3  10 1.0 0.0 0.0  2  0  :  -1
4  10 1.0 0.0 0.0  2  1  :  -1
5  10 0.0 1.0 0.0  2  2  :  -1
6  10 0.0 1.0 0.0  2  3  :  -1
7  10 0.0 0.0 1.0  2  4  :  -1
8  10 0.0 0.0 1.0  2  5  :  -1
9  10 0.5 0.5 0.0  2  6  :  -1
10 10 0.5 0.5 0.0  2  7  :  -1
11 10 0.5 0.0 0.5  2  8  :  -1
12 10 0.5 0.0 0.5  2  9  :  -1
13 10 0.0 0.5 0.5  2  10 :  -1
14 10 0.0 0.5 0.5  2  11 :  -1
15 10 0.3 0.3 0.3  2  12 :  -1
16 10 0.3 0.3 0.3  2  13 :  -1
