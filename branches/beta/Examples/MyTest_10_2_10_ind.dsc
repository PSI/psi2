#Number of queues
10
#Number of call types
2
#Queue capacity
1 1 1 1 1 1 1 1 1 1  
#Priority file
File: ../Examples/Priority_10_2_10.txt
#queue_minimal_initial_state
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
#Max file -N for no max file
File: ../Examples/Etats_10_2_10.txt
#Number_of_events
20
#Index file - N for No index file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0	11  2.0 2.0 3 -1 : 0 -1
1	11  2.0 2.0 3 -1 : 1 -1
2	11  2.0 2.0 3 -1 : 2 -1
3	11  2.0 2.0 3 -1 : 3 -1
4	11  2.0 2.0 3 -1 : 4 -1
5	10  6.0 6.0 2 0 : -1
6	10  6.0 6.0 2 1 : -1
7	10  6.0 6.0 2 2 : -1
8	10  6.0 6.0 2 3 : -1
9	10  6.0 6.0 2 4 : -1
10	11  2.0 2.0 3 -1 : 5 -1
11	11  2.0 2.0 3 -1 : 6 -1
12	11  2.0 2.0 3 -1 : 7 -1
13	11  2.0 2.0 3 -1 : 8 -1
14	11  2.0 2.0 3 -1 : 9 -1
15	10  6.0 6.0 2 5 : -1
16	10  6.0 6.0 2 6 : -1
17	10  6.0 6.0 2 7 : -1
18	10  6.0 6.0 2 8 : -1
19	10  6.0 6.0 2 9 : -1
