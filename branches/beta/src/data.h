/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file data.h
 * \brief Header of data.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr 
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */

#ifndef _H_Data
#define _H_Data

/***********************************************************/
/********* SIGNATURES DES FONCTIONS LOCALES ****************/
/***********************************************************/


/************************************************************/
/**** SIGNATURE DES FONCTIONS EXPORTABLES  ******************/
/************************************************************/


/**** Table de hachages **************************/

/* type "generique" des entrees de la table.
 * (renommage de "void *") pour rendre l'intention derriere le code plus claire. 
 */
typedef void *EntreeTable;

typedef struct _ListeEntree *ListeEntree ;

/* D�finitions export�es de "tablehash.c"
 */

/* type abstrait des tables
 */
typedef struct _Table *Table ;
/* creation d'une nouvelle table (vide) non NULL.
 * taille: param�tre pour jouer sur le compromis espace/temps.
 * HYPOTHESES: 
 *  1. (*equals)(e1,e2)==0  equivaut �  (*equals)(e2,e1)==0.
 *  2. (*hashCode)(e1) != (*hachCode)(e2) implique  (*equals)(e1,e2)==0 
 */
inline Table creeTable(int taille, 
		       int (*equals)(EntreeTable,EntreeTable), 
		       int (*hashCode)(EntreeTable)) ;

/* chercher une entr�e associ�e "equals" � "e" dans la table "t".
 * t est une table non NULL. 
 * EN SORTIE: "e" et "t" ne sont pas modifi�e ("e" est modifable).
 *   res = si pour tout "x" de la table, "! equals(x,e)", alors  "NULL", 
 *         sinon un "x" de la table tq "equals(x,e)". 
 */
inline EntreeTable chercheTable(Table t, EntreeTable e) ;

/* ajoute l'entr�e dans la table.
 * HYPOTHESES:
 *   "t" est non NULL.
 * EN SORTIE:
 *   "t" est modifi�e.
 *   "e" est une entr�e de la "t"  si elel n'etait pas deja presente
 */
inline void ajouteTable(Table t, EntreeTable e) ;

/* parcours de la table t, en appliquant traiter sur chaque entr�e 
 */
inline void parcoursTable(Table t, void (*traiter)(EntreeTable)) ;

/* parcours de la table t, en appliquant traiter (avec parametre differents) sur chaque entr�e 
 */
inline void parcoursTable_transition(int traj, int num_ant,void (*traiter)(int*,int)) ;
inline int parcoursTable_couplage(int indice);

EntreeTable Renvoie_Premier_non_null(Table t);

/* vide completement la table
 */
inline void viderTable(Table t) ;
inline void Libere_table(Table t); 


/**** initialisation **************************/

/* fixe les valeurs initiales etat_inf, etat_sup, couplage */
inline void init_etat();

/* fixe les valeurs initiales couplage */
inline void init_couplage(int i);
inline void init_couplage_inter();

/* effectue le calcul de probabilite des evenements a partir des valeurs de lambda saisis */
inline void init_probabilite();

/*  affecte les capacites saisies al'etat_sup intial *****/
/*inline void set_etat_init_supG();*/

/*************** fixateurs **************************/


/* fixe l'evenement alternatif pour un evenement ****/
inline void set_evt_alt(int j,int k);

/* fixe le seuil pour un evenement   */
inline void set_seuil(int i,double seuil);

/* affecte le couplage a l'iteration log2tempsarret *****/
inline void set_couplage(int num, int i,int log2tempsarret);

/* affecte une valeur dans le tableau des trajectoires   *****/
inline void set_trajectoire( int i, int k);

/*********** modificateurs ***********************/

/* ajoute un element dans la  file de rang n*/
inline void ajout_file(int n); 

/* supprime un element dans la file derang n */
inline void supp_file(int n);

/**** accesseurs ********************************************/

/*   renvoie l'etat de la file de numero (nombre d'element) */
inline int get_opt_s();

/*   renvoie l'etat de la file de numero (nombre d'element) */
inline int etat_file(int n);

/* renvoie le nombre de file du systeme */
inline int get_nb_file();

/* renvoie le nombre d'etats max du systeme */
inline int get_nb_max();

/* renvoie le nombre d'evenements du systeme */
inline int nb_evenements();

/* renvoie le nombre de types du systeme */
inline int nb_types();

/* renvoie le nombre de file d'une eveneement */
inline int get_nbr_file_evt(int i);

/* renvoie le nombre de file origine d'un evenement */
inline int get_nb_ori(int i);

/* renvoie l'adresse du premier element du tableau des capacites ****/
inline int * get_capacite();

/* renvoie la valeur de la capacite de la file de numero n */
inline int capacite_file(int n);

/* renvoie la valeur du taux de probabilite de l'evenement i ****/
inline double get_taux(int i);

/* renvoie le seuil d'un evenement */
inline double  get_seuil(int i);

/* renvoie l'evnement alternatif d'un evenement */
inline int get_evt_alt(int i);

/* renvoie le nombre d'echantillon de la simulation  ***/
inline int get_taille_ech();

/* renvoie le nombre de variable antithetic  ***/
inline int get_nb_VA();

/*  recupere la trajectoire correspondant a l'etape i ***/
inline int get_trajectoire(int i);

/* renvoie l'adrsse du premier element d'un des tableau des etats sup ****/
inline Table get_etat_sup();

/* renvoie l'adrsse du premier element du tableau des etats sup ****/
/*inline int ** get_etat_sup_tot();*/

/* renvoie l'adresse du premier element du tableau des etats inf ****/
/*inline int * get_etat_inf();*/

/* renvoie le statut de couplage a  l'etape i ******/
inline int get_couplage(int num, int i);

/*  renvoie la longueur maxi du trajet *****/
inline double get_lg_traj_max();

/* renvoie le type d'un evenement *****/
inline int get_type_evt(int evenement);

/* renvoie le numer o de file associe a une evt et un indice ****/
inline int get_param(int evenement ,int indice_file);

/* renvoie le numero de la fonction d'index associe a une evt et un indice ****/
inline int get_numf(int evenement ,int indice_file);

/*  renvoie pour l'evenement i la liste des files src */
inline int get_nbr_file_evt(int evenement);


/* renvoie le nombre de jeton dans l'etat initial inferieur d'une file ****/
inline int get_etat_init_infU(int num_file);

/* renvoie le nombre de jeton dans l'etat initial superieur d'une file *****/
inline int* get_etat_init_supU(int num_file);

/*  renvoie pour l'evenement i le tableau des files multiserveurs */
inline int ** get_multi_server(int evenement);

/*Renvoie le nombre d'etats courants dans la table de hachage*/
inline int get_nb_max_cour();

/*Renvoie un element non null de la table de hachage*/
inline int* get_premier_non_null();

/*Renvoie le parametre de la fonction de hachage*/
inline int get_hash_param(int puis);

/*Renvoie le tableau de priorites*/
inline int* get_prior_files();

/*Renvoie la taille de la table de hachage*/
inline int get_taille_table();

/* affichage ecran  des etats *******/
inline void imprime_etat(int *etat1);

#endif     /* _H_Data  */
