/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file psi2.c
 * \brief Functions of psi2.c . 
 * \author Bernard.Tanzi@imag.fr
 * \author Jean-Marc.Vincent@imag.fr 
 * \author Jerome.vienne@imag.fr
 *
 * \version 4.4.3
 * \date 2004-2006
 */

/*
*------------------------------------------------------------------
* Fichier      : psi2.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme principal contenant le main permettant de lancer l'execution 
*   des differents traitements de l'application
*-------------------------------------------------------------------------------
*/

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>


/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "data.h"
#include "modele.h"
#include "lect_model.h"
#include "save_resultat.h"

#define DEBUG 1
/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/



/***********************************************************/
/********* SIGNATURES DES FONCTIONS LOCALES ****************/
/***********************************************************/


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/*************************************************************/
/******* DECLARATION DES VARIABLES EXTERNES AU MODULE ********/
/************************************************************/
extern int *lg_traj_max;
extern int *trajectoire;
extern int *nbr_echantillon;
extern int *nbr_VA; // number of anthitetic variables

extern int *capfile;
extern int *prior_files;
extern int **etat_init_supU;
extern int *etat_init_infU;
extern int *germe_gener;
extern int **etat_inf;
//extern int **etat_sup;
extern Table Table_etat_sup;
extern int* taille_Table;
extern int *nb_jeton;
extern int **couplage;

extern int *nb_evt;
extern int *nbr_file;
extern int *nbr_type;
extern int *nbr_etats; 
extern int *nbr_final_queue;
extern int *nb_max_cour;
extern int* puiss_hash_param;

extern void *lib_handle;
extern int dym_on;

/** pointeurs sur tableaux de tailles nombres evenements modeles statistiques ***/
extern double *P;
extern double *R;
extern int *A;

extern struct st_evt *evt;
extern FILE *descfp;
extern struct st_multi *multis;
extern double **table_index; 
extern int compt_s;  

/******************************************************************/
/***** DECLARATION DES VARIABLES EXPORTEES *************************/
/*******************************************************************/

char* version="P.S.I.2 version 4.4.3";

int nbparamlc;


/*! Executable name. */
char *prog;   


/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/*!
* \fn help_sample
* \brief Print on screen the help message. 
*/

void help_sample()
{
  printf(" USAGE : %s [-ipo] argument [-hdtv]\n",prog);
  printf("\n  -i : input file in ext directory \n");
  printf("\n  -p : parameter file in ext directory\n");
  printf("\n  -o : output file in ext directory \n");
  printf("\n       By default, output file has outputtest.txt name in ext directory\n");
  printf("\n  -h : help.\n");
  printf("\n  -d : With details on output file\n");
  printf("\n  -t : Without doubling period, show coupling time of each queue\n");
  printf("\n  -v : version\n");
  printf("\n  -s: queue by queue display\n");
  printf(" \n");
}

/*!
* \fn version_sample
* \brief Print on screen the soft version
*/

void version_sample()
{
  printf(" %s ", version);
  printf(" - Perfect Simulator 2 -  \n");
  printf(" Last update: \n");
  printf("	+Linux/Mac dynamic library loading in the same file \n");
  printf(" Authors : Bernard.Tanzi@imag.fr\n");
  printf("           Jean-Marc.Vincent@imag.fr\n");
  printf("           Jerome.Vienne@imag.fr\n");
  printf(" Web : http://www-id.imag.fr \n");
  printf(" \n");
}



/******************************************************************/
/******** IMPLANTATION DES FONCTIONS EXPORTEES ********************/
/******************************************************************/




/*!
* \fn main
* \brief main function of psi2
* \param int argc: number of parameter of the function
* \param char*[] argv table of pointer which contain all the different parameters
* \return int 0 if all is ok
*/

int main(int argc,char **argv) {
  nbparamlc=argc; 
  int cpt;
  char c;
  char *opt_i=NULL,*opt_o=NULL;
  char *opt_p=NULL;
  prog=argv[0];
  int compt_o = 0;
  int compt_h = 0;
  int compt_i = 0;
  int compt_t = 0;
  int compt_d = 0; //Use to active detail option
  
  
  printf(" \n");
  while((c = getopt(argc,argv,"hdtvsi:o:p:")) != -1){
	switch(c){
	case 'h': help_sample(); 
	   compt_h ++;
	   compt_i ++; // to avoid repetition help message
	   break;

	case 'v': version_sample();
	   compt_h ++;
	   compt_i ++; // to avoid repetition help message
	   break;
	 
	 case 'i' : opt_i = optarg;
	   compt_i ++;
	   break;
	
	 case 'o' : opt_o = optarg;
	   compt_o ++; // utilization of -o option
	   break;
	
	 case 'p' : opt_p = optarg;
	   break;
	   
	 case 't' : 
	   compt_t ++; // utilization of -t option
	   break;
	   
	 case 'd' : 
	   compt_d ++; // utilization of -d option
	   break;
	   
	 case 's':
	   compt_s ++;// utilization of -s option
	   break;
	   
	 }
    }
  
  if((c == -1) && (compt_i == 0)) // no option specified
    {
      help_sample();
      compt_h++;
    }
  else{
      if(compt_h == 0)  // backward simulation called
	{
	
	  
	      char *chemin1;
	      char *chemin2;
	      char *chemin4;
    
          	chemin4=(char *)malloc(150*sizeof(char));
	      strcpy(chemin4,"");
	      strcat(chemin4,opt_p);
	      lire_param_FU(chemin4); 
 
	      chemin1=(char *)malloc(150*sizeof(char)); 
	      strcpy(chemin1,"");
	      strcat(chemin1,opt_i);    
	      lire_data_FU(chemin1);  

	      chemin2=(char *)malloc(150*sizeof(char));
	      strcpy(chemin2,"");
	      if (compt_o ==1) 
		strcat(chemin2,opt_o);
	      else
		strcat(chemin2,"./outputtest.txt");	
	      sauve_present_RS(chemin1, chemin4, chemin2);

	      init_probabilite();

	      if (compt_t == 0){
			lancement(compt_d);
	      } else {
			lancement_single(compt_d);
	      }
	    
		free(chemin1);      
		free(chemin2);
		free(chemin4);
	 
	free(lg_traj_max);
	free(trajectoire);
	free(nbr_echantillon);
	free(capfile);
	free(prior_files);
	free(nbr_file);
	free(nbr_final_queue);
	for(cpt=0; cpt<*nbr_etats; cpt++){
		free(etat_init_supU[cpt]);
	}
	free(etat_init_supU);
	free(etat_init_infU);
	free(multis);
	free(table_index);
	//free(etat_inf);
	free(nb_max_cour);
	
	/*for(cpt=0; cpt<*nbr_etats; cpt++){
		free(etat_sup[cpt]);
	}*/
	viderTable(Table_etat_sup);
	Libere_table(Table_etat_sup); 
	//free(Table_etat_sup); 
	free(nbr_etats);
	free(nb_jeton);
	free(puiss_hash_param);
	for(cpt=0; cpt<*nbr_VA; cpt++){
		free(couplage[cpt]);
	}
	free(couplage);
	
	if(dym_on==1){
		dlclose(lib_handle);
	}
      
        for(cpt=0; cpt<(*nb_evt)*(*nbr_type); cpt++){
		free((evt+cpt)->param_evt);
		free((evt+cpt)->num_fct);
	}
	free(nbr_type);
	free(nb_evt);
	free(nbr_VA);
	free(P);
	free(R);
	free(A);
	free(germe_gener);
	free(evt);
	fclose(descfp);      
	  
    }
  }
  return 0;     
}
  



