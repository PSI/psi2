/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file data.c
 * \brief Functions of data.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr 
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : data.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent, Jerome Vienne
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Secondary programm use to R/W access to the datas
*
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h> 
#include <stdlib.h>

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "data.h"
#include "lect_model.h"


/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/


/**Garde en memoire un element non null du tableau: a la fin du couplage cela designera l'etat max restant*/
	ListeEntree premier_non_null;


/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/***************************************************************/
/******* DECLARATION DES VARIABLES EXTERNES AU MODULE **********/
/***************************************************************/
extern double *lg_traj_max;
extern int *nbr_echantillon;
extern int *nbr_VA; // number of anthitetic variables
extern int *trajectoire;

extern int *nbr_file;
extern int *nbr_type;
extern int *nbr_etats;
/** pointeurs sur tableaux de taille nombre de files ***/

extern int *capfile;
extern int *prior_files;
extern int **etat_init_supU;
extern int *etat_init_infU;
//extern int *etat_inf;
//extern int **etat_sup;
extern Table Table_etat_sup;
extern int taille_Table;
extern int hash_param;
extern int* puiss_hash_param;


extern int *nb_jeton;
extern int **couplage;
extern int* couplage_inter;

extern int *nb_evt;
extern int *nb_max_cour;
/** pointeurs sur tableaux de tailles nombres evenements modeles statistiques ***/
/** probabilites des evenements */
extern double *P;
/** seuils */  
extern double *R; 
/** evenements alternatifs si le seuil est depasse */
extern int *A;   


extern struct st_evt *evt;
extern struct st_multi *multis;
int compt_s;
/***********************************************************************/
/**** IMPLANTATION DES FONCTIONS EXPORTABLES   **************************/
/***********************************************************************/

/************************************************************************/
/****    FONCTIONS TABLES DE HACHAGE             ************************/
/************************************************************************/

/** Structure des tables de hachage*/ 
struct _ListeEntree {
	EntreeTable entree ;
	struct _ListeEntree *suiv ;
};

struct _Table {
	int taille ;
	ListeEntree *table ; /* table est un tableau de longueur "taille" */
	int (*equals)(EntreeTable,EntreeTable) ;
	int (*hashCode)(EntreeTable) ;
} ;



/*static void lecture_table(EntreeTable e){
	int i;
	for(i=0;i<get_nb_file()+2;i++){
		printf("%d\n",((int*)e)[i]);
	}
	printf("\n#\n");
}*/


/*static int hashCode(Table t, EntreeTable e) 
{
	if((*t->hashCode)(e)==somme_max){
	return 0;
	}
	return (*t->hashCode)(e);
}*/
/**
*----------------------------------------------------------------
*
* Function     : creeTable
*
* Result     : Table
*
* Parameters   : 
*
*taille int size of the table
*
* equals pointer on equality function
*
* hashcode pointer on hashcode function
*
* Called functions :
*
* Description  :
*    
*    create and return a hashtable  
*     
*
*----------------------------------------------------------------------
*/

Table creeTable(int taille, 
		int (*equals)(EntreeTable,EntreeTable), 
		int (*hashCode)(EntreeTable)) 
{
	Table T =malloc(sizeof(struct _Table));
	T->taille=taille;
	T->table=calloc(sizeof(ListeEntree),taille);
	T->equals=equals;
	T->hashCode=hashCode;
	return T;
}

/**
*----------------------------------------------------------------
*
* Function     : chercheTable
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions :
*
* Description  :
*    
*     
*     
*
*----------------------------------------------------------------------
*/

/*EntreeTable chercheTable(Table t, EntreeTable e)
{
	int h=hashCode(t,e);
	ListeEntree l = t->table[h];
	for(;l && (!t->equals(e,l->entree));l=l->suiv);
	if(l){
		return l->entree;
	}
	return NULL;
}*/

/**
*----------------------------------------------------------------
*
* Function     : ajouteTable
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions :
*
* Description  :
*    
*     
*     
*
*----------------------------------------------------------------------
*/
/*void ajouteTable(Table t, EntreeTable e) 
{
	int h=hashCode(t,e);
	if(!chercheTable(t,e)){
		ListeEntree new = malloc(sizeof(struct _ListeEntree));
		new->entree=e;
		new->suiv=t->table[h];
		t->table[h]=new;
	}
	else{
		(*nb_max_cour)--;
		
	}
}*/
/**
*----------------------------------------------------------------
*
* Function     : parcoursTable
*
* Result     : void
*
* Parameters   : 
*
*t Table concerned table
*
*traiter function pointer on a function
*
* Called functions :
*
* Description  :
*    
*   Apply traiter to every element of t  
*     
*
*----------------------------------------------------------------------
*/
void parcoursTable(Table t, void (*traiter)(EntreeTable)) {
	int i;
        /*Calcul du nombre d'elements par colonne*/
	int nb_el=0;
	for(i=0;i<t->taille;i++){
		nb_el=0;
		//printf("Parcours %d\n",i);
		ListeEntree list;
		for(list=t->table[i];list;list=list->suiv){
                //on applique traiter et on compte le nombre d'element
			traiter(list->entree);
			nb_el++;
		}
		if (nb_el!=0)
		printf("nombre element colonne %d:%d\n",i,nb_el);
	}
}

/**
*----------------------------------------------------------------
*
* Function     : chercheTable_sans_hash
*
* Result     : void
*
* Parameters   : 
*
*t Table concerned table
*
*e EntreeTable entry of the table
*
*h int hashcode of e
*
* Called functions :
*
* Description  :
*    
*     
* 	seek for an element == e in t[h]    
*
*----------------------------------------------------------------------
*/

static EntreeTable chercheTable_sans_hash(Table t, EntreeTable e,int h)
{	
	//recherche dans la colonne h de t l'element e
        ListeEntree l = t->table[h];
	for(;l && (!t->equals(e,l->entree));l=l->suiv);
	if(l){
		return l->entree;
	}
	return NULL;
}



/**
*----------------------------------------------------------------
*
* Function     : parcoursTable_transition
*
* Result     : void
*
* Parameters   :
*
*traj int second parameter of traiter
*
*num_ant int number of antithétic variables
*
*traiter function pointer on a function
*
* Called functions :
*
* Description  :
*    
*     
*    Apply traiter(.,traj) to every element in Table_etat_sup  
*
*----------------------------------------------------------------------
*/
inline void parcoursTable_transition(int traj,int num_ant, void (*traiter)(int*,int)) {
	int i;
	int h;	
	int NB_FILE=get_nb_file();
	//init_couplage_inter();


	
	
        //On cree une nouvelle table vide
	Table tmp= creeTable(taille_Table, &equals_etats,&hashCode_etats) ;
        //la table est vide donc on reinitialise premier_non_null
	premier_non_null=NULL;
	
        //Parcours de l'ancienne table
	for(i=0;i<taille_Table;i++){
		ListeEntree list=Table_etat_sup->table[i];
		while(list){
			/*On applique la fonction transition */
			traiter((int*)list->entree,traj);

                        /*Recuperation du nouvel hashcode de cet entree*/
			h=((int*)list->entree)[NB_FILE+1];

			if(!chercheTable_sans_hash(tmp,list->entree,h)){
			/*L'element n'etait pas deja present, on l'installe dans la nouvelle table*/
				if(!premier_non_null){
                                /*Mise a jour de premier_non_null*/
					premier_non_null=list;	
				}
				/*Idee d'implementation de -d*/
				/*else{
					for (j=0; j<NB_FILE; j++){
						//printf("premier_non_null[%d]=%d, list=%d\n",j,((int*)premier_non_null->entree)[j],((int*)list->entree)[j]);
						if(((int*)list->entree)[j]!=((int*)premier_non_null->entree)[j] && couplage_inter[j]==1){
							couplage_inter[j]=0;
						}
							//printf("couplage_inter[%d]=%d\n",j,couplage_inter[j]);
					}
				}*/

                                /*On installe l'entree au bon endroit dans la
                                 * table temporaire*/
				Table_etat_sup->table[i]=list->suiv;
				list->suiv=tmp->table[h];
				tmp->table[h]=list;
				list=Table_etat_sup->table[i];
			}
			else{
			/*l'evenement etait deja present, on libere l'espace et
                         * on passe a l'evenement suivant*/
				(*nb_max_cour)--;
				Table_etat_sup->table[i]=list->suiv;
				free(list->entree);
				free(list);
				list=Table_etat_sup->table[i];
		
			};
		}
	}
        /*Liberation de l'espace*/
	free(Table_etat_sup->table);
	free(Table_etat_sup);

        /*Table_etat_sup prend sa nouvelle valeur*/
	Table_etat_sup=tmp;
	
	/*printf("FIN\n");
	parcoursTable(Table_etat_sup, &lecture_table);
	printf("nb_max_cour=%d\n",*nb_max_cour);*/
}
/**
*----------------------------------------------------------------
*
* Function     : parcoursTable_couplage
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions :
*
* Description  :
*    
*     
*     
*
*----------------------------------------------------------------------
*/
/*inline int parcoursTable_couplage(int indice) {
	return couplage_inter[indice];
}*/
/**
*----------------------------------------------------------------
*
* Function     : Renvoie_Premier_non_null
*
* Result     : void
*
* Parameters   : 
*
*t Table concerned table
*
* Called functions :
*
* Description  :
*    
*     
*    return a pointer on an element of the hastable. if the table is empty return null 
*
*----------------------------------------------------------------------
*/
/*EntreeTable Renvoie_Premier_non_null(Table t){
	int i;
	for(i=0;i<taille_Table;i++){
		ListeEntree list;
		for(list=t->table[i];list;list=list->suiv){
			return list->entree;
		}
	}
	return NULL;
}*/



/**
*----------------------------------------------------------------
*
* Function     : viderTable
*
* Result     : void
*
* Parameters   : 
*
*t Table concerned table
*
* Called functions :
*
* Description  :
*    
*     
* 	Empty the hashtable    
*
*----------------------------------------------------------------------
*/
void viderTable(Table t) 
{
	int i;
	struct _ListeEntree sent, *prec, *tmp;
	for(i=0;i<t->taille;i++){
		sent.suiv = t->table[i];
		prec = &sent; 
		while(prec->suiv){
     			t->table[i] = t->table[i]->suiv;
    			tmp = prec->suiv;
    			prec->suiv = tmp->suiv;
			free(tmp->entree);
    			free(tmp);
		}
	}
	
}

/**
*----------------------------------------------------------------
*
* Function     : Libere_table
*
* Result     : void
*
* Parameters   : 
*
*t Table concerned table
*
* Called functions :
*
* Description  :
*    
*     
* 	Free the board of t    
*
*----------------------------------------------------------------------
*/
inline void Libere_table(Table t) 
{
	free(t->table);	
	
}

 


/************************************************************************/
/****    FONCTIONS INITIALISATION              ************************/
/************************************************************************/



/**
*----------------------------------------------------------------
*
* Function     : init_etat
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : get_nb_file,get_etat_init_supU,get_etat_init_infU,
*
* Description  :
*    
*     initialised intial states of etat sup/inf tables
*     0 is the value of the coupling table
*
*----------------------------------------------------------------------
*/


inline void init_etat()
{
 int i_boucle,j_boucle;
 int** tmp;
 int NB_FILE=get_nb_file()+2;
 int NB_MAX=get_nb_max()+1;
 int hash_tmp;

 /*Creation d'un nouveaux tableau rempli avec les etats initiaux*/
 tmp=(int **)malloc(((*nbr_etats)+1)*sizeof(int));  
     for(i_boucle=0;i_boucle<((*nbr_etats)+1);i_boucle++){
			tmp[i_boucle]=(int *)calloc(NB_FILE,sizeof(int));
      }
        
	
	for(j_boucle=0;j_boucle<NB_FILE;j_boucle++){
			tmp[0][j_boucle]=etat_init_infU[j_boucle];
	}
        /*On vide l'ancienne table*/
	viderTable(Table_etat_sup); 
	
        /*Instalation de l'etat inf*/
	hash_tmp=tmp[0][NB_FILE-1];
	//printf("DEBUT\nhash=%d\n",hash_tmp);
	ListeEntree new = malloc(sizeof(struct _ListeEntree));
	new->entree=tmp[0];
	new->suiv=Table_etat_sup->table[hash_tmp];
	Table_etat_sup->table[hash_tmp]=new;
	
        /*Installation des autres etats*/
      for(i_boucle=1;i_boucle<NB_MAX;i_boucle++){
      //printf("i=%d\n",i_boucle);
      	for(j_boucle=0;j_boucle<NB_FILE;j_boucle++){
	 //printf("j=%d,etat=%d\n",j_boucle,etat_init_supU[i_boucle-1][j_boucle]);
			tmp[i_boucle][j_boucle]=etat_init_supU[i_boucle-1][j_boucle];
	}
      	/*ajouteTable(Table_etat_sup,tmp[i_boucle]);*/
	
	hash_tmp=tmp[i_boucle][NB_FILE-1];
	//printf("hash=%d\n",hash_tmp);
	new = malloc(sizeof(struct _ListeEntree));
	new->entree=tmp[i_boucle];
	new->suiv=Table_etat_sup->table[hash_tmp];
	Table_etat_sup->table[hash_tmp]=new;
      }
      
      
 /*int DIMENSION = get_nb_file();*/
 
 /*for (i_boucle = 0; i_boucle< DIMENSION; i_boucle++){
       etat_inf[i_boucle] = get_etat_init_infU(i_boucle);
 }*/
 
      
 *nb_max_cour=NB_MAX;
 free(tmp);
      
/*printf("REMPLISSAGE\n");
 parcoursTable(Table_etat_sup, &lecture_table);*/

}
/**
*----------------------------------------------------------------
*
* Function     : init_couplage
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : get_nb_file,get_etat_init_supU,get_etat_init_infU,
*
* Description  :
*    
*     initialised coupling table
*     0 is the value of the coupling table
*
*----------------------------------------------------------------------
*/
inline void init_couplage(int nb_ant)
{
 int i, i_boucle;
 int DIMENSION = get_nb_file();
 for (i= 0; i<nb_ant; i++){
	for (i_boucle = 0; i_boucle< DIMENSION; i_boucle++){
       couplage[i][i_boucle]= 0;
   }
 }
 
 
}

/**
*----------------------------------------------------------------
*
* Function     : init_couplage
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : get_nb_file,get_etat_init_supU,get_etat_init_infU,
*
* Description  :
*    
*     initialised coupling table
*     0 is the value of the coupling table
*
*----------------------------------------------------------------------
*/
inline void init_couplage_inter()
{
 int i_boucle;
 int DIMENSION = get_nb_file();
 for (i_boucle = 0; i_boucle< DIMENSION; i_boucle++){
       couplage_inter[i_boucle]= 1;
   }
}

/**
*----------------------------------------------------------------
*
* Function     : init_probabilite
*
* Result     : void
*
* Parameters   : nothing
*
* Called functions : nb_evenements
*
* Description  :
*
*	Compute the probality rate, the sum need to be equal to 1    
*
*----------------------------------------------------------------------
*/

inline void init_probabilite(){
  double somme=0;
  int i_boucle,j_boucle;
  int NB_EVTS=nb_evenements()/nb_types();
  int NB_TYPES=nb_types();
 	for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++){
       		//somme+=(evt+i_boucle)->lambda;
		for (j_boucle = 0; j_boucle< NB_TYPES; j_boucle++){
			somme+=(evt+i_boucle*NB_TYPES+j_boucle)->lambda;
		}
       }
 	for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++){
     		// *(P+i_boucle)=((evt+i_boucle)->lambda)/somme;
     		for (j_boucle = 0; j_boucle< NB_TYPES; j_boucle++){
			*(P+i_boucle*NB_TYPES+j_boucle)=((evt+i_boucle*NB_TYPES+j_boucle)->lambda)/somme;
		}
	}
#ifdef DEBUG    
   printf("%f ", somme);
   for (i_boucle = 0; i_boucle< NB_EVTS; i_boucle++)
           printf("%f ",*(P+i_boucle);
   printf("\n");
#endif
}

/**
*----------------------------------------------------------------
*
* Function     : set_etat_init_supG
*
* Result       : void
*
* Parameters   : nothing
*
* 
* Description  :
*
*	Assign the capacity give at the initial etat_sup	
*
*----------------------------------------------------------------------
*/
/*
inline void set_etat_init_supG(){
  int i;
  for(i=0;i<*nbr_file;i++)
  *(etat_init_supU+i)=*(capfile+i);
}*/

/**************************************************************************/
/********************* FONCTIONS DE FIXATION ********************************/
/*****************************************************************************/



/**
*----------------------------------------------------------------
*
* Function     : set_seuil
*
* Result       : void
*
* Parameters   : 
*
* Name      Type      Role
*
* i         int       event number
*
* seuil     double    limit for the event
*
* Description  :
*
*     Fix the threshold at an event
*
*----------------------------------------------------------------------
*/

inline void set_seuil(int i,double seuil){
  *(R+i)=seuil;
}

/**
*----------------------------------------------------------------
*
* Function     : set_evt_alt
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* i         int       event number
*
* k         int       alternative event number
*
* Description  :
*
*     fix the alternative event for an event
*
*----------------------------------------------------------------------
*/

inline void set_evt_alt(int i, int k){
 *(A+i)=k;
}

/**
*----------------------------------------------------------------
*
* Function     : set_trajectoire
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* i         int       trajectory number
*
* evt       int       event number 
*
* Description  :
*
*    fix the trajectory starting from an event
*
*----------------------------------------------------------------------
*/

inline void set_trajectoire(int i, int evt){
  trajectoire[i]=evt;
}

/**
*----------------------------------------------------------------
*
* Function     : set_couplage
*
* Result       : void
*
* Parameters   : 
*
* Name                  Type      Role
*
* i                    int       coupling number
*
* log2tempsarret       int       iteration number 
*
* Description  :
*
*   Assign to the queue number i, the iteration number (log2)   
*   corresponding to the coupling of this queue
*
*----------------------------------------------------------------------
*/

inline void set_couplage(int num, int i,int log2temps_arret){
  couplage[num][i]=log2temps_arret;
}



/*************************************************************************/
/*************  FONCTION DE MODIFICATION ********************************/
/*****************************************************************************/   

/**
*----------------------------------------------------------------
*
* Function     : ajout_file
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* n         int       Queue number 
*
* Description  :
*
*	   add an entry at the queue of rank k 
*
*----------------------------------------------------------------------
*/
inline void ajout_file(n){
   *(nb_jeton+n)+=1;    
  }

/**
*----------------------------------------------------------------
*
* Function     : supp_file
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* n         int       Queue number 
*
* Description  :
*
*     Delete an entry at the queue of rank k 
*
*----------------------------------------------------------------------
*/

inline void supp_file(n){
   *(nb_jeton+n)-=1;  /*  suppression d'un jeton dans la file de rang n */
 }


  
/************************************************************************/
/************* ACCESSEURS **********************************************/
/***********************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : get_opt_s
*
* Result       : int
*
* Parameters   : 
*
* Name      Type      Role
*
* Description  :
*
*    return if the user has asked for -s option
*
*----------------------------------------------------------------------
*/

inline int get_opt_s(){
  return compt_s;
}

/**
*----------------------------------------------------------------
*
* Function     : etat_file
*
* Result      : int
*
* Parameters   : 
*
* Nom          Type      Role
*
* n            int       Queue number
*
* Description  :
*
*		return the token number of the queue n
*
*----------------------------------------------------------------------
*/

inline int etat_file(n){
   return nb_jeton[n];
 }

/**
*----------------------------------------------------------------
*
* Function     : etat_file
*
* Result       : int
*
* Parameters   : nothing
*                   
* Description  :
*
*           return the number of queue in the system
*
*----------------------------------------------------------------------
*/

inline int get_nb_file(){
  return (*nbr_file)*(*nbr_type);
}


/**
*----------------------------------------------------------------
*
* Function     : get_nb_max
*
* Result       : int
*
* Parameters   : nothing
*                   
* Description  :
*
*           return the number of max states in the system
*
*----------------------------------------------------------------------
*/
inline int get_nb_max(){
	return *nbr_etats;
}

/**
*----------------------------------------------------------------
*
* Function     : nb_evnements
*
* Result       : int
*
* Parameters   : nothing 
*                    
* Description  :
*
*				return the event number
*
*----------------------------------------------------------------------
*/

inline int nb_evenements(){
  return *nb_evt*(*nbr_type);
}

/**
*----------------------------------------------------------------
*
* Function     : nb_types
*
* Result       : int
*
* Parameters   : nothing 
*                    
* Description  :
*
*				return the types number
*
*----------------------------------------------------------------------
*/

inline int nb_types(){
  return *nbr_type;
}



/**
*----------------------------------------------------------------
*
* Function     : get_capacite
*
* Result       : int *
*
* Parameters   : nothing
*                    
* Description  :
*
*				return the adress of the beginning of the capacity table 
*
*----------------------------------------------------------------------
*/

inline int *  get_capacite(){
  return capfile;
}

/**
*----------------------------------------------------------------
*
* Function     : get_capacite
*
* Result       : int 
*
* Parameters   : 
*
* Name         Type      Role
*
* n            int       Queue number
*
* Description  :
*
*			return the capacity of the queue n
*
*----------------------------------------------------------------------
*/

inline int capacite_file(n){
  return *(capfile+n);
  }

/**
*----------------------------------------------------------------
*
* Function     : get_taux
*
* Result       : double 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the probability rate between 0 & 1 of the event i
*
*----------------------------------------------------------------------
*/

inline double get_taux(i){
  return *(P+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_seuil
*
* Result      : double 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       event number
*
* Description  :
*
*       return the threshold of the event
*
*----------------------------------------------------------------------
*/

inline double get_seuil(i){
  return *(R+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_evt_alt
*
* Result       : int 
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the alternative event
*
*----------------------------------------------------------------------
*/

inline int get_evt_alt(i){
  return *(A+i);
}

/**
*----------------------------------------------------------------
*
* Function     : get_taille_ech
*
* Result       : int 
*
* Parameters   : nothing
*
* Description  :
*
*			return the number of sample
*
*----------------------------------------------------------------------
*/

inline int get_taille_ech(){
  return *nbr_echantillon;
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_VA
*
* Result       : int 
*
* Parameters   : nothing
*
* Description  :
*
*			return the number of antithetic variable
*
*----------------------------------------------------------------------
*/

inline int get_nb_VA(){
  return *nbr_VA;
}


/**
*----------------------------------------------------------------
*
* Function     : get_etat_sup
*
* Result       : int * 
*
* Parameters   : 
*
*i int concerned maximal state
*
* Description  :
*
*			return adress of the first element of the table of etats_sup[i]
*
*----------------------------------------------------------------------
*/

inline Table get_etat_sup(){
  return Table_etat_sup;
}

/**
*----------------------------------------------------------------
*
* Function     : get_etat_sup_tot
*
* Result       : int * *
*
* Parameters   : Nothing
*
* Description  :
*
*			return adress of the first element of the table of etats_sup
*
*----------------------------------------------------------------------
*/

/*inline int ** get_etat_sup_tot(){
  return etat_sup;
}*/


/**
*----------------------------------------------------------------
*
* Function     : get_etat_inf
*
* Result       : int * 
*
* Parameters   : nothing
*
* Description  :
*
*       return the address of the first element of the table of etats_inf
*
*----------------------------------------------------------------------
*/

/*inline int * get_etat_inf(){
  return etat_inf;
}*/


/**
*----------------------------------------------------------------
*
* Function     : get_trajectoire
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       stage in the past
*
* Description  :
*
*       return the event corresponding at stage i in the past
*
*----------------------------------------------------------------------
*/

inline int get_trajectoire( int indice){
  return trajectoire[indice];
}


/**
*----------------------------------------------------------------
*
* Function     : get_couplage
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Queue number
*
* Description  :
*
*			function which indicates if a file coupled or not
*			return 0 if the queue number i hasn't been coupled or the log2 of iteration number associated to the coupling
*
*----------------------------------------------------------------------
*/

inline int get_couplage(int num, int i){
  return couplage[num][i];
}


/**
*----------------------------------------------------------------
*
* Function     : get_lg_traj_max
*
* Result       : double  
*
* Parameters   : nothing
*
* Description  :
*
*       return the maximal length of the trajectory 
*----------------------------------------------------------------------
*/

inline double get_lg_traj_max(){
  return *lg_traj_max;
} 

/**
*----------------------------------------------------------------
*
* Function     : get_type_evt
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the type of an event
*
*----------------------------------------------------------------------
*/

inline int  get_type_evt(i){
 return  (evt+i)->typ_evt;
}

/**
*----------------------------------------------------------------
*
* Function     : get_type_evt
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return the call type of an event
*
*----------------------------------------------------------------------
*/

inline int  get_type_app_evt(i){
 return  (evt+i)->type_app;
}

/**
*----------------------------------------------------------------
*
* Function     : get_param1
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* j            int       index of the dynamic table of the number of queue
*
* Description  :
*
*			return the number of the file of index j associates the event number i
*       
*----------------------------------------------------------------------
*/

inline int  get_param(i,j){
  return *((evt+i)->param_evt+j); 
}


/**
*----------------------------------------------------------------
*
* Function     : get_numf
*
* Result       : int  
*
* Parameters   : 
*
* Name         Type      Role
*
* i            int       Event number
*
* j            int       index of the dynamic table in the number of queue
*
* Description  :
*
*       return the function number of index assiates at the queue.
*       
*----------------------------------------------------------------------
*/

inline int  get_numf(i,j){
  return *((evt+i)->num_fct+j); 
}

/**
*----------------------------------------------------------------
*
* Function     : get_nbr_file_evt
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the number of origin and destination queue.
*
*----------------------------------------------------------------------
*/

inline int get_nbr_file_evt(i){
  return (evt+i)->nbr_file_evt;
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_ori
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the number of origin queue.
*
*----------------------------------------------------------------------
*/

inline int get_nb_ori(i){
  return (evt+i)->num_ori;
}


/**
*----------------------------------------------------------------
*
* Function     : get_etat_init_infU
*
* Result       : int  
*
* Parameters   : 
*
* Namz          Type      Role
*
* num_file     int       Queue number
*
* Description  :
*
*       return the number of token in the initial inferior state of the queue
*
*----------------------------------------------------------------------
*/

inline int get_etat_init_infU(num_file){
  return *(etat_init_infU+num_file);
}

/**
*----------------------------------------------------------------
*
* Function     : get_etat_init_supU
*
* Result       : int  
*
* Parameters   : 
*
* Nom          Type      Role
*
* num_file     int       Queue number
*
* Description  :
*
*       return the number of token in the initial superior state of the queue
*
*----------------------------------------------------------------------
*/

inline int* get_etat_init_supU(num_file){
  return etat_init_supU[num_file];
}

/**
*----------------------------------------------------------------
*
* Function     : get_multi_server
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
*
* i            int       Event number
*
* Description  :
*
*			return for the event i, the multiserver queue table
*
*----------------------------------------------------------------------
*/

inline int ** get_multi_server(i){
  return (multis[i].queue_list);
}

/**
*----------------------------------------------------------------
*
* Function     : get_nb_max_cour
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
* Description  :
*
*	return the current number of states in the hashtable
*
*----------------------------------------------------------------------
*/

inline int get_nb_max_cour(){
  return *nb_max_cour;
}

/**
*----------------------------------------------------------------
*
* Function     : get_premier_non_null
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
* Description  :
*
*	Return a pointer on a state board in the hashtable
*
*----------------------------------------------------------------------
*/

inline int* get_premier_non_null(){
  return (int*)(premier_non_null->entree);
}

/**
*----------------------------------------------------------------
*
* Function     : get_hash_param
*
* Result       : int  
*
* Parameters   : 
*
*puis int power
*
* Name          Type      Role
* Description  :
*
*	Return the parameter of the hashcode elevated to the power puiss
*
*----------------------------------------------------------------------
*/

inline int get_hash_param(int puis){
  return puiss_hash_param[puis];
}

/**
*----------------------------------------------------------------
*
* Function     : get_prior_files
*
* Result       : *int  
*
* Parameters   : 
*
* None
*
* Name          Type      Role
* Description  :
*
*	Return the pointer on the priority board
*
*----------------------------------------------------------------------
*/

inline int* get_prior_files(){
  return prior_files;
}


/**
*----------------------------------------------------------------
*
* Function     : get_taille_table
*
* Result       : int  
*
* Parameters   : 
*
* Name          Type      Role
* Description  :
*	Return the hash tab size
*
*----------------------------------------------------------------------
*/

inline int get_taille_table(){
  return taille_Table;
}

/**
*----------------------------------------------------------------
*
* Function     : imprime_etat
*
* Result     : void  
*
* Parameters   : 
*
* Name          Type      Role
*
* etat1        int *     adress of the beginning of the state table
*
* Called functions : get_nb_file
*
* Description  :
*
*			print on the screen the state of each queues
*
*----------------------------------------------------------------------
*/

inline void imprime_etat(int *etat1)
{
  int * etat=etat1;
 int i_boucle;
 int NB_FILE=get_nb_file();
 for (i_boucle = 0; i_boucle < NB_FILE ; i_boucle++)
   printf("%3d ",etat[i_boucle]);
}
