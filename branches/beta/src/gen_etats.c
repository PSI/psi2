
/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>

/********************************************************/
/** DECLARATION VARIABLES GLOBALES A L'APPLICATION  *****/
/********************************************************/

/* donn�s lues du fichier utilisateur */
  

/** pointer on queue number */
int *nbr_file;   

 /** pointer on type number */
int *nbr_type;  
 
/** pointer on final number of queue */
int *nbr_final_queue;

/**** tableaux dynamiques de longueur le nombre de files ****/

/** pointer on queue capacity */
int *capfile;  

/** pointer on etat */
int ** etats;
;


/** pointer on number of max states */
int *nbr_etats;   

FILE *descfp;
/*----------------------------------------------------------------
*
* Function     : nb_max_file
*
* Result       : int
*
* Parameters   : 
*
*nb_type int number of possible requests
*
*cap int queue capacity
*
* Description  : return the number of maximal states of a queue   
*	
*
*  
*----------------------------------------------------------------------
*/

static int nb_max_file(int nb_type, int cap){
  if (nb_type==1||cap==0){
    return 1;
  }else if(cap==1){ 
    return nb_type;
  }else{
    int i;
    int s=0;
    for(i=0;i<=cap;i++){
      s+=nb_max_file(nb_type-1,cap-i);
    }
    return s;
  }
}
  /**
*----------------------------------------------------------------
*
* Function     : nb_max
*
* Result       : int
*
* Parameters   : 
*
*nb_type int* number of possible requests
*
*cap int* pointer on the queues capacity board
*
*nb_file int* number of queues
*
* Description  : return the number of maximal states of a set of  queues
*	
*
*  
*----------------------------------------------------------------------
*/

static int nb_max(int* nb_type ,int* cap, int* nb_file){
  int i;
  int s=1;
  for(i=0;i<(*nb_file);i++){
    s*=nb_max_file(*nb_type,cap[i*(*nb_type)]);
  }
  return s;
}

  /**
*----------------------------------------------------------------
*
* Function     : est_dernier_max
*
* Result       : void
*
* Parameters   : 
*
*cour int* pointer on an initial max state board
*
*cap int* pointer on the queues capacity board
*
*nb_type int number of possible requests
*
*nb_file int number of queues
*
* Description  : return 1 if all queues execpt the first are in their last maximal state (all people in the queue are of the last type of requests) 
*	
*
*  
*----------------------------------------------------------------------
*/

static int est_dernier_max(int* cour,int* cap,int nb_type,int nb_file){
   int res=1;
   int i;
   for (i=2;i<=nb_file;i++){
     res=res &&(cour[i*nb_type-1]==cap[i*nb_type-1]);
   }
 
   return res;
 }
 
 
  /**
*----------------------------------------------------------------
*
* Function     : max_suiv_file
*
* Result       : void
*
* Parameters   : 
*
*cour int* pointer on an initial max state board
*
*cap int queue capacity
*
*nb_type int number of possible requests
*
* Description  : modifies the current state to give the next maximal state of a queue 
*	
*
*  
*----------------------------------------------------------------------
*/

static void max_suiv_file(int* cour, int cap, int nb_type){
   /*retourne dans cour la configuration maximale suivante
     d'une file de capacite cap recevant nb_type types d'appel*/
   int i_boucle;
   if (cour[nb_type-1]+cour[0]==cap){
     cour[0]--;
     cour[1]=cap-cour[0];
     for(i_boucle=2;i_boucle<nb_type;i_boucle++){
       cour[i_boucle]=0;
     }
  }else{
     max_suiv_file(cour+1,cap-cour[0],nb_type-1);
   }
 }
 
 
 /**
*----------------------------------------------------------------
*
* Function     : max_suiv
*
* Result       : void
*
* Parameters   : 
*
*cour int* pointer on an initial max state board
*
*cap int* pointer on the queues capacity board
*
*nb_type int number of possible requests
*
*nb_file int number of queues
*
* Description  : modifies the current state to give the next maximal state of the set of queues
*	
*
*  
*----------------------------------------------------------------------
*/


static  void max_suiv(int* cour, int* cap, int nb_type, int nb_file){
   int i_boucle;
   int j_boucle;
   if(est_dernier_max(cour,cap,nb_type,nb_file)){
     max_suiv_file(cour,cap[0],nb_type);
     for(i_boucle=1;i_boucle<nb_file;i_boucle++){
       cour[i_boucle*nb_type]=cap[i_boucle*nb_type];
       for(j_boucle=1;j_boucle<nb_type;j_boucle++){
         cour[i_boucle*nb_type+j_boucle]=0;
       }
     }
   }else{
     max_suiv(cour+nb_type,cap+nb_type,nb_type,nb_file-1);
   }
 }
 
/**
*----------------------------------------------------------------
*
* Function     : calcul_etats_max
*
* Result       : void
*
* Parameters   : 
*
*cour int** pointer on an initial max state board
*
* Description  :
*	Complete the initial max states board
*
*  
*----------------------------------------------------------------------
*/


static void calcul_etats_max(int ** cour){
 int i=1;
 int j;
 for (i=0;i<*nbr_file;i++){
 /*Premier etat: toutes les files sont pleines avec des clients de type 0*/
     	cour[0][i*(*nbr_type)]=capfile[i*(*nbr_type)];
     }
       
  for(i=1;i< *nbr_etats;i++){
   for(j=0;j<*nbr_final_queue;j++){
	cour[i][j]=cour[i-1][j];
   }
   /*Calcul de l'etat suivant a partir du precedent*/
    max_suiv(cour[i],capfile,*nbr_type,*nbr_file);
 }		
}

/**
 *----------------------------------------------------------------*/

int lire_data_FU(char *nomf){ 
  int i, j;
  FILE *fd;     /* pointeur sur descripteur du fichier */
  char commande[BUFSIZ];

 //On etire toute les lignes qui commence par un #
  sprintf(commande, "grep -v \"#\" %s",nomf );
  
  if ((fd = popen(commande,"r")) == NULL ){
    printf("Pb d'ouverture de fichier '%s' \n",nomf);
    perror("Le Pb est : ");
    return(1);
  }
  else{ 
    printf("Reading of the data file : %s\n", nomf);
    
    /* lecture variable nombre de files ****/  
    nbr_file=(int *)malloc(sizeof(int));  
    fscanf(fd,"%d",nbr_file);    
    /* pour affichage nombre de files *****/
    // printf("nombre de file : %d\n",*nbr_file);
    
    /* lecture variable nombre de types ****/  
    nbr_type=(int *)malloc(sizeof(int));
    fscanf(fd,"%d",nbr_type); 
    /* pour affichage nombre de types *****/
    //      printf("nombre de type : %d\n",*nbr_type);
    
    /* calcul du nombre de files modelisees au final ****/  
    nbr_final_queue=(int *)malloc(sizeof(int));
    *nbr_final_queue =(*nbr_file)*(*nbr_type);

    /* pour affichage nombre de files modelisees*****/
    //  printf("nombre final de files modelisees : %d\n",*nbr_final_queue);
    
    /*lecture des capacit�s des files*/	
    capfile=(int *)malloc((*nbr_final_queue) * sizeof(int)); 
    /* alloue chaque pointeur sur entier une adresse memoire *****/
    for(j=0;j<*nbr_file;j++){ 
      fscanf(fd,"%d",capfile+(j*(*nbr_type)));  /* stocke a cette @ la valeur lue ****/
      /*taille_Table+=*(capfile+(j*(*nbr_type)));*/
      for(i=0;i<*nbr_type;i++){ 
	*(capfile+i+(j*(*nbr_type)))=*(capfile+(j*(*nbr_type)));  /* stocke a cette @ la valeur lue ****/
	
	/* pour affichage capacite file ****/
	//	printf("capacite  file %d : %d\n",i+(j*(*nbr_type)),*(capfile+i+(j*(*nbr_type))));
	
      }
    }
  }
  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : sauve_etat
*
* Result       :
*
* Parameters   :
*
* Name       Type      Role
* 
* etat	int **	      pointer on the current states  
*					  
*
* nomf  char *       pointer on the character strings including the name of the  save file 
*					  with his relative access path
* 
* 
* Called function : get_nb_file
* 
* Description  :
*	 Save the current states on the file passed on parameter with "-o" option    
*
*----------------------------------------------------------------------
*/
int sauve_etats(int **etat, char *nomf){
   int i,j;
   
   if ((descfp = fopen(nomf,"w")) == NULL ){
      printf("Pb d'ouverture de fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
    }    

    fprintf(descfp,"# Nb etats\n");
    fprintf(descfp,"%d\n",*nbr_etats);
    fprintf(descfp,"# Etats consid�r�s\n");
    for(i=0;i<(*nbr_etats);i++){    
      for(j=0;j<(*nbr_final_queue);j++){
	fprintf(descfp," %d ",etat[i][j]);
      }
      fprintf(descfp,"\n");
    }
    
   fprintf(descfp,"\n#===================================================================================================\n");
   
   
   return(0);
}
   

int main(int argc, char ** argv){
  char c;
  int i;
  int compt_o = 0;
  int compt_i = 0;
  char *nomf_data=NULL;
  char *nomf_res=NULL;
  //int compt;
   
  printf(" \n");
  while((c = getopt(argc,argv,"i:o:")) != -1){
	switch(c){
	case 'i' : nomf_data = optarg;
	  compt_i ++;
	  break;
	
	 case 'o' : nomf_res = optarg;
	   compt_o ++; // utilization of -o option
	   break;
	}
  }

  /*lecture du fichier description*/
  lire_data_FU(nomf_data);
   
  /*Calcul des �tats*/
  nbr_etats=(int*)malloc(sizeof(int));
  *nbr_etats=nb_max( nbr_type , capfile, nbr_file);
  etats=(int**)calloc((*nbr_etats)*(*nbr_final_queue),sizeof(int));
  for(i=0;i<(*nbr_etats);i++){
    etats[i]=(int*)calloc((*nbr_final_queue),sizeof(int));
  }
  calcul_etats_max(etats);

  /*Ecriture des r�sultats dans un fichier*/
  sauve_etats(etats,nomf_res);
  
  /*Lib�ration de la m�moire*/
 for(i=0;i<(*nbr_etats);i++){
   free(etats[i]);
 }
 free(etats);
 free(capfile);
 free(nbr_file);
 free(nbr_final_queue);
 free(nbr_etats);
 free(nbr_type);
 return 0;
}		
