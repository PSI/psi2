/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file transition.h
* \brief Header of transition.c
* \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/

#ifndef _H_transition
#define _H_transition

/****************************************************************/
/*********  SIGNATURE DES FONCTIONS LOCALES  *********************/
/****************************************************************/

/* fonction arrivee exterieur sur une file destination  **************/
int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par);

/***  fonction sortie exterieure depuios une file origine ***********/
int sortie_ext(int etat[],int param1);
/***  fonction sortie exterieure depuios une file origine avec type ***********/
int sortie_ext_types(int etat[],int queue,int type_app);

/***  fonction routage n files avec rejet ***********/
int *  routage_nfile_rejet (int etat[],int nb_dest,int * par);

/***  fonction routage n files avec rejet prenant en compte les requetes***********/
int *  routage_nfile_rejet_types(int etat[],int nb_dest,int * par,int type_app);

/***  fonction routage n files avec blocage ***********/
int *  routage_nfile_bloc (int etat[],int nb_dest,int * par);

/***  fonction routage n files avec blocage ***********/
int *  routage_nfile_bloc_types (int etat[],int nb_dest,int * par,int type_app);

/******* fonction JSQ  arrivee  avec rejet *********/
int *  Arrivee_rejet (int etat[],int nb_dest,int * par, int * numf);

/******* fonction JSQ  routage avec rejet *********/
int *  JSQ_rejet (int etat[],int nb_dest,int * par);


/******* fonction multi serveur index avec rejet *********/
int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf,int ** tab);

int * arrivee_ext_deb_rejet_types(int etat[],int nb_dest,int * par,int type_app);

/**************************************************************/
/*****  SIGNATURE DES FONCTIONS EXPORTABLES  ******************/
/**************************************************************/

/***  fonction de transition  interface            *****/
extern void transition_interface(int etat[], int evenement);

#endif             /* _H_transition ***/
