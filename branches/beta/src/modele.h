/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file modele.h
* \brief Header of modele.c
* \author - Bernard.Tanzi@imag.fr  
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/

#ifndef _H_Modele
#define _H_Modele


/******************************************************/
/******** INTERFACE DES FONCTIONS LOCALES  ************/
/******************************************************/

/* fonction de construction ******/
void construction();

/*** fonction initialisation du generateur ******/
void init_generateur();

/**** loi uniforme sur 0,1   ******/
double u01();

/* fonction de generation d'evenement ****/
int genere_evenement();

/* fonction de test des etats impossible ****/
/*int test_etat_imp (int ** etat_saved,int num);*/

/****************************************************/
/***  INTERFACE DES FONCTIONS EXPORTABLES ***********/
/****************************************************/

/** fonction de test du couplage   */
extern int test_couplage(int i);

/* fonction de cout ***************/
extern int cout_etat(int * etat1,int num_file);

/*** fonction principale en +1 ******/
extern void lancement_single(int detail);

/*** fonction principale doublement de la p�riode (ancien main ) ******/
extern void lancement(int detail);


#endif /* _H_modele */


