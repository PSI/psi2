 /* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file lect_model.c
 * \brief Functions of lect_model.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 *
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : lect_model.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme permettant la lecture des donn�s d utilsateurs 
* ainsi que la creation dynamique a partir des donnees lues des 
* structures de donnees qui seront utiliser par l'application 
*  
*-------------------------------------------------------------------------------
*/

/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>

/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "lect_model.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/
/* #define DEBUG 1 */
#ifdef __linux__
	#define _LINUX_
#endif

#ifdef __APPLE__
	#define _APPLE_
#endif
/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/


/********************************************************/
/** DECLARATION VARIABLES GLOBALES A L'APPLICATION  *****/
/********************************************************/

/* donn�s lues du fichier utilisateur */
 
 

/**  pointer on the maximal length on a trajectory */
double *lg_traj_max; 

/** pointer on the table of trajectories */ 
int *trajectoire;   
 
/** pointer on the number of sample */   
int *nbr_echantillon; 

/** pointer on the number of antithetic variable */ 
int *nbr_VA;

/** pointer on seed  */   
int *germe_gener; 
 

/** pointer on queue number */
int *nbr_file;   

 /** pointer on type number */
int *nbr_type;  
 
 
/** pointer on final number of queue */
int *nbr_final_queue;

/** pointer on number of max states */
int *nbr_etats;   


/**** tableaux dynamiques de longueur le nombre de files ****/

/** pointer on queue capacity */
int *capfile;  
/** pointer on queues priority */
int *prior_files;  
/** pointer on etat_init_sup */
int **etat_init_supU;

/**Hash table size*/
int taille_Table;

/** pointeur on the number of max states which haven't coupled yet*/ 
int *nb_max_cour;

/** pointeur on etat_init_inf */ 
int *etat_init_infU;

/** hash function parameter */ 
int hash_param;

/** board of hash function parameter */ 
int* puiss_hash_param;
 
/** pointer etat sup */     
Table Table_etat_sup;

/** pointer on token number of a queue */    
int *nb_jeton; 

/** pointer coupling of a queue  */     
int **couplage;
int *couplage_inter;
      
/** pointer on event number */
int *nb_evt;   

int dym_on;


/*** tableaux dynamiques de longueur le nombre d'evnements ***/

/** pointer on events rate*/
double *P;
/** pointer on thresholds */     
double *R;
/** pointer on alternative events */      
int *A;       

/** pointer on structure event */
struct st_evt *evt;  

/** Pointer on the list of servers for multi servers*/
struct st_multi *multis;

/** Pointer on the table which contain index function values .*/
double **table_index;   

/**option -s option boolean*/
int compt_s=0;

void *lib_handle ;
int (*ptrfunc)(int * etat_inf, int * etat_sup);
int (*ptraff)(int * etat); 

 
void purger(void){
    int c;

    while ((c = getchar()) != '\n' && c != EOF)
    {}
}

void clean (char *chaine){
    char *p = strchr(chaine, '\n');


    if (p)
    {
        *p = 0;
    }

    else
   {
        purger();
    }
} 
 
/*static void lecture_table(EntreeTable e){
	int i;
	for(i=0;i<*nbr_final_queue;i++){
		printf("%d\n",((int*)e)[i]);
	}
	printf("\n#\n");
}*/
  /**
*----------------------------------------------------------------
*
* Function     : hashCode_etats
*
* Result       : int
*
* Parameters   : 
*
*e EntreeTable pointer on a current state
*
* Description  : return the hashcode associated  
*	
*
*  
*----------------------------------------------------------------------
*/

int hashCode_etats(EntreeTable e){
/*le hashcode est contenu dans une des cases du tableau*/
	//printf("h=%d\n",((int*)e)[(*nbr_final_queue)]);
	return ((int*)e)[(*nbr_final_queue)+1];
}


  /**
*----------------------------------------------------------------
*
* Function     : equals_etats
*
* Result       : int
*
* Parameters   : 
*
*e1 EntreeTable pointer on a current state
*
*e2 EntreeTable pointer on a current state
*
* Description  : return 0 if e1==e2 
*	
*
*  
*----------------------------------------------------------------------
*/
/* fonction d'egalite des 2 etats*/
int equals_etats(EntreeTable e1,EntreeTable e2){
	int NB_FILE=(*nbr_final_queue);
	
	/*egalite terme a terme*/
	int i;
	for(i=0;i<NB_FILE;i++){
		if(((int*)e1)[i]!=((int*)e2)[i]){
			return 0;
		};
	}
	return 1;
}





 /**
*----------------------------------------------------------------
*
* Function     : calcul_hash_initial
*
* Result       : int
*
* Parameters   : 
*
*cour int* pointer on a state
*
* Description  :
*	Calcul of the sum associated to the state
*
*  
*----------------------------------------------------------------------
*/
static int calcul_hash_initial(int * cour){
	int i_boucle;
	int somme=0;
	//int par=1;
	for(i_boucle=*nbr_final_queue-1;i_boucle>=0;i_boucle--)
	{
		/*somme+=par*cour[i_boucle];
		par=par*hash_param;*/
		somme+=cour[i_boucle]*puiss_hash_param[i_boucle];
	}
	return somme;
	
}

 /**
*----------------------------------------------------------------
*
* Function     : rempli_table_hash_param
*
* Result       : int
*
* Parameters   : None
*
*
*
* Description  :
*	complete puiss_hash_param with the differents power of the hash param
*
*  
*----------------------------------------------------------------------
*/
static void rempli_table_hash_param(){
	int i_boucle;
	int par=1;
	for(i_boucle=(*nbr_final_queue)-1;i_boucle>=0;i_boucle--)
	{
		if(par*hash_param>0){
			puiss_hash_param[i_boucle]=par;
			par=par*hash_param;
		}
		else{
			
			puiss_hash_param[i_boucle]=par;
			par=1;
		}
	}
	
}
 
 
/**
*----------------------------------------------------------------
*
* Function     : nombre_premier_suivant
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* nb  int    number to test
*
* Description  :
*
*		return the first following prime number after nb
*
*  
*----------------------------------------------------------------------
*/ 
static int nombre_premier_suivant(int nb){
	int i=2;
	int chiffre=nb+1;
	while(1){
		while(i<chiffre && chiffre%i!=0)
		{
			i++;
		}
		if(i==chiffre){
			return chiffre;
		}
		chiffre++;
		i=2;
	}
}
 
/**
*----------------------------------------------------------------
*
* Function     : lire_data_FU
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* nomf  char *    pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -i option, create data structures (with malloc) 
*		and read index file if there is one.
*		return 1 if there is a problem or 0 if all is ok
*
*  
*----------------------------------------------------------------------
*/


int lire_data_FU(char *nomf){

  
  int i, j, k,l, error;
  int cpt;
  int  nb_file_multi;
  int temp1,temp2;
  char var[10];   // utilise qd on lit la liste des files
  char var_max[10];
  char var_priorite[10];
  FILE *fd,*fd2,*fd3,*fd4;      /* pointeur sur descripteur du fichier */
  

  int nb_function;
  int *value_number=NULL;
  int value; 
 
  char file_index[100];
  char file_index_max[100];
  char file_index_priorite[100];
  char commande[BUFSIZ];
  char commande_max[BUFSIZ];
  char commande_priorite[BUFSIZ];
  

  //On etire toute les lignes qui commence par un #
  sprintf(commande, "grep -v \"#\" %s",nomf );
  	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture de fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
    }
  else{ 
      printf("Reading of the data file : %s\n", nomf);
      /* lecture variable nombre de files ****/  
      nbr_file=(int *)malloc(sizeof(int));  
      fscanf(fd,"%d",nbr_file);    
#ifdef DEBUG  /* pour affichage nombre de files *****/
      printf("nombre de file : %d\n",*nbr_file);
#endif

	/* lecture variable nombre de types ****/  
      nbr_type=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_type); 

#ifdef DEBUG  /* pour affichage nombre de types *****/
      printf("nombre de type : %d\n",*nbr_type);
#endif


	/* calcul du nombre de files modelisees au final ****/  
      	nbr_final_queue=(int *)malloc(sizeof(int));
      	*nbr_final_queue =(*nbr_file)*(*nbr_type);

#ifdef DEBUG  /* pour affichage nombre de files modelisees*****/
      printf("nombre final de files modelisees : %d\n",*nbr_final_queue);
#endif
	
	
	
      capfile=(int *)calloc((*nbr_final_queue),sizeof(int)); 
	  /* alloue �chaque pointeur sur entier une adresse memoire ******************/
      for(j=0;j<*nbr_file;j++){ 
      		fscanf(fd,"%d",capfile+(j*(*nbr_type)));  /* stocke a cette @ la valeur lue ****/
		/*taille_Table+=*(capfile+(j*(*nbr_type)));*/
      		for(i=0;i<*nbr_type;i++){ 
			*(capfile+i+(j*(*nbr_type)))=*(capfile+(j*(*nbr_type)));  /* stocke a cette @ la valeur lue ****/
		
#ifdef DEBUG /* pour affichage capacite file ****/
	printf("capacite  file %d : %d\n",i+(j*(*nbr_type)),*(capfile+i+(j*(*nbr_type))));
#endif
      		}
      }
      
      
      
      /*lecture priorite des files et stockage dans un tableau alloue dynamiquement*/
 fscanf(fd,"%s",&var_priorite);
	  if(strcmp("File:", var_priorite)){
		printf("Error: \"File:\" waited, we have %s\n", var_priorite);
	  } else {
	  
		fscanf(fd,"%s", file_index_priorite);
			printf("Reading of priority file %s\n",file_index_priorite);
			sprintf(commande_priorite, "grep -v \"#\" %s",file_index_priorite );
		  	if ((fd4 = popen(commande_priorite,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",nomf);
					perror("Le Pb est : ");
					return(1);
				} else {
				
					prior_files=(int*)calloc((*nbr_final_queue),sizeof(int));
					for(j=0;j<(*nbr_final_queue);j++){
						fscanf(fd4,"%d",prior_files+j);		
					}	
				}
		
	  }/*Fin lecture fichier priorites*/

	/*for(i=0;i<(*nbr_final_queue);i++){
		printf("prior[%d]=%d\n",i,prior_files[i]);
	}*/
	
	
	
	
      
      /* lecture etat_inf initial et stockage dans tableau alloue dynamiquement, on garde 2 places pour le hashcode ***/      
      etat_init_infU=(int *)calloc(((*nbr_final_queue)+2),sizeof(int));
	  
	for(j=0;j<*nbr_file;j++){ 
		for(i=0;i<*nbr_type;i++){ 
		fscanf(fd,"%d",(etat_init_infU+i+(j*(*nbr_type))));

#ifdef DEBUG
      //printf("mini file %d  : %d\n",i+(j*(*nbr_type)),*(etat_init_supU+i+(j*(*nbr_type))));
#endif
      		}
      	}
	
	
			
			

	/*Parametre de la table de hachage, on le laisse a 2*/
			hash_param=2;

			/*On rempli le tableau des puissances du parametres*/
			puiss_hash_param=(int*)calloc((*nbr_final_queue), sizeof(int));
			rempli_table_hash_param();
			
			/*for(i=0;i<(*nbr_final_queue);i++){
				printf("hash_param[%d]=%d\n",i,puiss_hash_param[i]);
			}*/
	

   /*lecture etat sup initiaux et stockage dans un tableau allou�*/
 fscanf(fd,"%s",&var_max);
	  if(strcmp("File:", var_max)){
		printf("Error: \"File:\" waited, we have %s\n", var_max);
	  } else {
	  
			fscanf(fd,"%s", file_index_max);
			
			printf("Reading of max file %s\n",file_index_max);
			sprintf(commande_max, "grep -v \"#\" %s",file_index_max );
			
		  	if ((fd3 = popen(commande_max,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",nomf);
					perror("Le Pb est : ");
					return(1);
				
				} else {
				
					/*recuperation du nombre d'etats*/
				  nbr_etats=(int*)malloc(sizeof(int));
				  fscanf(fd3,"%d",nbr_etats);
				  printf("nbr_etats=%d\n",*nbr_etats);
				  
				  /*Calcul de la taille de la table*/
				  taille_Table=nombre_premier_suivant(*nbr_etats);
				  
				  nb_max_cour=(int*)malloc(sizeof(int));
				  *nb_max_cour=*nbr_etats+1;
				  
				  etat_init_supU=(int **)malloc((*nbr_etats)*sizeof(int));  
				  for(j=0;j<(*nbr_etats);j++){
				    etat_init_supU[j]=(int *)malloc(((*nbr_final_queue)+2)*sizeof(int));
				  }
				  for(j=0;j<(*nbr_etats);j++){
				    for(i=0;i<(*nbr_final_queue);i++){
				      fscanf(fd3,"%d",&etat_init_supU[j][i]);
				    }
				     etat_init_supU[j][(*nbr_final_queue)]=calcul_hash_initial( etat_init_supU[j]); 
				    /*hashcode associ�e*/
				     etat_init_supU[j][(*nbr_final_queue)+1]=calcul_hash_initial( etat_init_supU[j])%taille_Table;
				  }
				}
	  }


				//printf("taille_Table=%d\n",taille_Table);
				
				/*Fin du remplissage de la table d'etat inf*/
		 			etat_init_infU[(*nbr_final_queue)]=calcul_hash_initial(etat_init_infU); 
				/*hashcode associ�e*/
					etat_init_infU[(*nbr_final_queue)+1]=etat_init_infU[(*nbr_final_queue)]%taille_Table;
					
					
				/*for(j=0;j<(*nbr_etats);j++){
				    for(i=0;i<(*nbr_final_queue)+2;i++){
				      printf("Etat[%d][%d]=%d\n",j,i,etat_init_supU[j][i]);
				    }
				  }*/


		
		
		
		
		/*Creation de la table de hachage qui contient tous les etats extremaux*/      
		Table_etat_sup=creeTable(taille_Table, &equals_etats,&hashCode_etats);
		
		//parcoursTable(Table_etat_sup, &lecture_table);
  
		/* creation dynamique d'un tableau pour stocker l'etat des files **/
		nb_jeton=(int *)malloc((*nbr_final_queue)*sizeof(int));
     
      /* creation dynamique d'un tableau pour stocker l'etat du couplage des files **/
      couplage=(int**)calloc(*nbr_VA, sizeof(int));
	  for (i=0; i< *nbr_VA; i++){
		couplage[i]=(int *)calloc((*nbr_final_queue),sizeof(int));
      }
      
      couplage_inter=(int*)calloc((*nbr_final_queue), sizeof(int));
	  
      /* lecture du nombre d'evenement **/
      nb_evt=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nb_evt);  

	  /////////////////////////////////////////////////////////////////////////////
	  // Lecture du nom du fichier 
	  fscanf(fd,"%s",&var);
	  if(strcmp("File:", var)){
		printf("Error: \"File:\" waited, we have %s\n", var);
	  } else {
	  
		fscanf(fd,"%s", file_index);
	  
		if(!strcmp("N", file_index)){
			printf("There is no index file\n");
		} else {
			printf("Reading of the index file : %s\n", file_index);
	  
			
			sprintf(commande, "grep -v \"#\" %s",file_index );
	
				if ((fd2 = popen(commande,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",nomf);
					perror("Le Pb est : ");
					return(1);
				} else {
					// On recupere le nombre de fonction
					fscanf(fd2,"%d", &nb_function);
					//printf("On a %d fonctions d'index \n", nb_function);		

					value_number=(int *)malloc((nb_function)*sizeof(int));
					// On recupère le nombre de valeur pour chaque file
					for(i=0;i<nb_function; i++){
						fscanf(fd2,"%d\n", (value_number+i));
						//printf("Nombre de valeur: %d\n", *(value_number+i));
					} 
	
					// Creation of the table
					printf("Creation of the index function table......\n");
					table_index = (double **) calloc(nb_function, sizeof(double));
					if (table_index != NULL){
						error=0;
						cpt =0;
				// On rempli la table d'index par l'index des files dest
				for(i=0; i<nb_function; i++){
					table_index[i] = (double *) calloc(*(value_number+i), sizeof(double));
					if (table_index[i] == NULL){
						error = 1;
					}
					cpt++;
				}
				
				if (error){
					while (cpt){
						cpt--;
						free(table_index[cpt]);
					}
					perror("Error not enough memory ");
					return(1);
				}
			} else {
				printf("Not enough memory\n");
			}
			
			// Remplissage du tableau
			
			for(i=0; i<nb_function; i++){
				
				for (j=0; j<*(value_number+i); j++){
					
					fscanf(fd2, "%d", &value);
					
					if(value != j){
						printf("Error: there is a problem on the line where there is the number of the index function, it should be %d\n", j);
					}
					fscanf(fd2, "%lf", &table_index[i][j]);
					//printf("On a mis %lf en (%d, %d)\n",table_index[i][j], i, j);
				}
			}
				
		}
		pclose(fd2);
	  	printf("Reading of the index file %s over\n", file_index);
	}
	}
	  /////////////////////////////////////////////////////////////////////////////
      
      /* creation dynamique d'un tableau pour les probabilites des evenements ***/
      P=(double *)malloc((*nb_evt)*(*nbr_type)*sizeof(double));

      /* creation dynamique d'un tableau pour les seuils ***/
      R=(double *)malloc((*nb_evt)*(*nbr_type)*sizeof(double));

      /* creation dynamique d'un tableau pour les evenements alternatifs ***/
      A=(int *)malloc((*nb_evt)*(*nbr_type)*sizeof(int));

      /* pour importer le tableau  des evenements dans une structure ******/
      evt=(struct st_evt *)malloc((*nb_evt)*(*nbr_type)*sizeof(struct st_evt));
     
	  multis=(struct st_multi *)calloc((*nb_evt)*(*nbr_type), sizeof(struct st_multi));
	   
      for(i=0;i<(*nb_evt);i++){
			fscanf(fd,"%d %d",&temp1,&temp2);
		for(l=0;l<(*nbr_type);l++){
			
			(evt+i*(*nbr_type)+l)->id_evt=temp1;
			(evt+i*(*nbr_type)+l)->typ_evt=temp2;
			//fscanf(fd,"%d %d %lf %d ",&(evt+i)->id_evt,&(evt+i)->typ_evt,&(evt+i)->lambda,&(evt+i)->nbr_file_evt);
			//printf("\n i=%d, l=%d:id: %2d       type: %2d\n",i,l,(evt+i*(*nbr_type)+l)->id_evt,(evt+i*(*nbr_type)+l)->typ_evt);
			//printf("\n id: %2d       type: %2d    lambda: %f   nbr_file evt:%2d \n",(evt+i)->id_evt,(evt+i)->typ_evt,(evt+i)->lambda,(evt+i)->nbr_file_evt);
		}
		
		for(l=0;l<(*nbr_type);l++){
			
			fscanf(fd,"%lf",&(evt+i*(*nbr_type)+l)->lambda);
			
			(evt+i*(*nbr_type)+l)->type_app=l;
			//printf("\n i=%d, l=%d :lamdba: %f type +%d\n",i,l,(evt+i*(*nbr_type)+l)->lambda,(evt+i*(*nbr_type)+l)->type);
		}  
		
		//recuperation du nombre de files dans l'evenement
		fscanf(fd,"%d",&temp1);
		for(l=0;l<(*nbr_type);l++){
			
			(evt+i*(*nbr_type)+l)->nbr_file_evt=temp1;
			//printf("\n i=%d, l=%d :nbrfile: %d\n",i,l,(evt+i*(*nbr_type)+l)->nbr_file_evt);
			
			((evt+i*(*nbr_type)+l)->param_evt)=malloc(((evt+i*(*nbr_type)+l)->nbr_file_evt)*sizeof(int));
			((evt+i*(*nbr_type)+l)->num_fct)=malloc(((evt+i*(*nbr_type)+l)->nbr_file_evt)*sizeof(int));
		
	    	}
		
		if ((evt+i*(*nbr_type))->typ_evt == 5){
			// On est ds le type 5
			
			fscanf(fd,"%s",&var);
			for(l=0;l<(*nbr_type);l++){
				*((evt+i*(*nbr_type)+l)->param_evt)=atoi(var);
				if((evt+i*(*nbr_type)+l)->lambda==0){
					//Test si la requete est acceptee par cet evenement
					/*A REVOIR*/
					//requete_refusee(*((evt+i*(*nbr_type)+l)->param_evt+j),l);
				}
			}
			

			fscanf(fd,"%s",&var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			for (j=1;j<((evt+i*(*nbr_type))->nbr_file_evt);j++){
				//On parcourt les files en parametre
				
				
				fscanf(fd," (%d,%d)", &temp1,&temp2);
				for(l=0;l<(*nbr_type);l++){
					*((evt+i*(*nbr_type)+l)->param_evt+j)=temp1;
					/*if((evt+i*(*nbr_type)+l)->lambda==0){
						//Test si la requete est acceptee par cet evenement
						requete_refusee(*((evt+i*(*nbr_type)+l)->param_evt+j),l);
					}*/
					
					*((evt+i*(*nbr_type)+l)->num_fct+j)=temp2;
				}
				
				//fscanf(fd," (%d,%d)", ((evt+i*(*nbr_type))->param_evt+j), ((evt+i*(*nbr_type))->num_fct+j));
				//printf("J'ai lu: %d et %d\n", *((evt+i)->param_evt+j), *((evt+i)->num_fct+j));
				// on vérifie d'on a assez de valeur de fonction d'index pour chaque fonction
				// Capacité de la file doit être < ou = à Nombre de valeur de la fonction d'index associé
				//printf("Capacité de la file: %d\n", *(capfile+*((evt+i)->param_evt+j)));
				//printf("Nombre de valeur de la fonction d'index: %d\n", *(value_number+*((evt+i)->num_fct+j)));

				if( (*((evt+i*(*nbr_type))->param_evt+j)) != -1 ){
					//printf("On est diff de -1\n");
					
					if ( *(capfile+*((evt+i*(*nbr_type))->param_evt+j)) > (*(value_number+*((evt+i*(*nbr_type))->num_fct+j)) - 1) ){
					
						printf(" Function index %d is not defined for all the value of the queue %d \n", *((evt+i*(*nbr_type)+l)->num_fct+j), *((evt+i*(*nbr_type)+l)->param_evt+j));
						printf("There is %d index values for a queue with a capacity of %d\n", *(value_number+*((evt+i*(*nbr_type)+l)->num_fct+j)) , *(capfile+*((evt+i*(*nbr_type)+l)->param_evt+j)) );
					}
					
				} else {
					if( (*(value_number+*((evt+i*(*nbr_type))->num_fct+j))) < 1 )
						printf("You need at least 1 value for the exit (-1) on index function");
				}	

			}
			
		}
		
		else if ((evt+i*(*nbr_type))->typ_evt == 8){
			// On est ds le type 8
			
			fscanf(fd,"%s",&var);
			for(l=0;l<(*nbr_type);l++){
				*((evt+i*(*nbr_type)+l)->param_evt)=atoi(var);
				/*if((evt+i*(*nbr_type)+l)->lambda==0){
					//Test si la requete est acceptee par cet evenement
					requete_refusee(*((evt+i*(*nbr_type)+l)->param_evt+j),l);
				}*/
			}
			
			fscanf(fd,"%s",&var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			for(l=0;l<(*nbr_type);l++){
				multis[i*(*nbr_type)+l].queue_list = (int **) calloc((evt+i*(*nbr_type))->nbr_file_evt, sizeof(int*));
			}
			
			for (j=1;j<((evt+i*(*nbr_type))->nbr_file_evt);j++){
				
				fscanf(fd," (%d,", &nb_file_multi);
				//printf("Il y a  %d file \n", nb_file_multi);
				
				if( nb_file_multi != -1){
				
					for(l=0;l<(*nbr_type);l++){
						multis[i*(*nbr_type)+l].queue_list[j] = (int *) calloc((nb_file_multi+1), sizeof(int));
						multis[i*(*nbr_type)+l].queue_list[j][0] = nb_file_multi;
					}
				
					for(k=0; k<nb_file_multi; k++){
						
						fscanf(fd,"%d,", &temp1);
						
						for(l=0;l<(*nbr_type);l++){
							multis[i*(*nbr_type)+l].queue_list[j][k+1]=temp1;
						}
						//fscanf(fd,"%d,", &(multis[i].queue_list[j][k+1]));
						//printf("On a la file %d en %d, %d\n", multis[i].queue_list[j][k+1], j, (k+1) );
				
					}
				
					fscanf(fd,"%d)", &temp1 );
					for(l=0;l<(*nbr_type);l++){
							*((evt+i*(*nbr_type)+l)->num_fct+j)=temp1;
					}
					//fscanf(fd,"%d)", ((evt+i)->num_fct+j) );
					//printf("La file d'index est: %d \n",*((evt+i)->num_fct+j) );
				}
				
				else{
					for(l=0;l<(*nbr_type);l++){
						multis[i*(*nbr_type)+l].queue_list[j] = (int *) calloc(1, sizeof(int));
						multis[i*(*nbr_type)+l].queue_list[j][0] = -1;
					}
					
					//printf("On a la file %d en %d, O\n", multis[i].queue_list[j][0], j );
					
					fscanf(fd,"%d)", &temp1 );
					for(l=0;l<(*nbr_type);l++){
					
						*((evt+i*(*nbr_type)+l)->num_fct+j)=temp1;
						//fscanf(fd,"%d)", ((evt+i*(*nbr_type)+l)->num_fct+j) );
						//printf("La file d'index est %d \n",*((evt+i)->num_fct+j) );
					}
				}
			
			}
		} 
		else if ((evt+i*(*nbr_type))->typ_evt == 9){	
			// On est ds le type 9 -> call center ^^
			
			
			for (j=0;j<((evt+i*(*nbr_type))->nbr_file_evt);j++){
			
				fscanf(fd,"%s",&var);
				
				if(strcmp(":", var) ){
				
					for(l=0;l<(*nbr_type);l++){
						*((evt+i*(*nbr_type)+l)->param_evt+j)=atoi(var);
						/*if((evt+i*(*nbr_type)+l)->lambda==0){
								//Test si la requete est acceptee par cet evenement
								requete_refusee(*((evt+i*(*nbr_type)+l)->param_evt+j),l);
						}*/
					}
					
				}else{
					
					for(l=0;l<(*nbr_type);l++){
						(evt+i*(*nbr_type)+l)->num_ori=j;
					}

					//printf("Evt: %d Nombre de file origine: %d \n",i, (evt+i)->num_ori );
					// Ici on a rencontre le caractere :
					j--;
				}
			}
		}
		else{
			// On n' est pas dans le type 5 ou 8 ou 9
			for (j=0;j<((evt+i*(*nbr_type))->nbr_file_evt);j++){
				fscanf(fd,"%s",&var);
				
				
					if(strcmp(":", var) ){
						for(l=0;l<(*nbr_type);l++) {
							*((evt+i*(*nbr_type)+l)->param_evt+j)=atoi(var);
							//printf("\n i=%d, l=%d :param: %d\n",i,l,*((evt+i*(*nbr_type)+l)->param_evt+j));

						}
					}
					
					else{
						// Ici on a rencontre le caractere :
						j--;
					}
			}
		}
    
	}
  }
  pclose(fd);
  printf("Reading of the data file : %s over\n", nomf);
  return(0);
}

/**
*----------------------------------------------------------------
*
* Function     : lire_couplage
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* 
*
* Description  :
*
*	Use to open dynamic library
*
*  
*----------------------------------------------------------------------
*/

int lire_couplage(FILE *fd){

char var[10];   // utilis pour lire le nom du fichier  
char file_coupl[100];
char *chemin3;
char *nfin;  
char *nfout;
char *chficso;
	  
fscanf(fd,"%s",&var);
//fgets(var, 100 , fd);
	  
// On supprime le \n
//	clean(var);
	  
if(strcmp("File:", var)){
	printf("Error: \"File:\" waited, we have %s\n", var);
} else {
	  
	fscanf(fd,"%s", &file_coupl);
	// on recupre le nom du fichier
	//fgets(file_coupl, 100 , fd);
		
	// On supprime le \n
	//clean(file_coupl);	
	 
	if(!strcmp("No", file_coupl)){
		printf("There is no coupling file\n");
		dym_on=0;
	} else {
		dym_on=1;
		printf("Reading of the coupling file : %s\n", file_coupl);
		 /* pour stocker le nom de fichier sans son extension */

		nfin = (char *) malloc(40*sizeof(char));
		nfout = (char *) malloc(40*sizeof(char)); 
		strcpy(nfin,"");
		strcat(nfin,file_coupl);
		strcat(nfin,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfin);
		#endif

		int lgfile = strlen(nfin);

		#ifdef DEBUG
		printf("longueur du fichier %d\n",lgfile);
		#endif

		int i;
		for(i=0;i<(lgfile-1);i++){
			*(nfout+i)=nfin[i];
		}
		strcat(nfout,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfout);
		#endif
	     
		
		chemin3=(char *)malloc(100*sizeof(char));
		strcpy(chemin3,"");
		#ifdef _LINUX_
			/* pour creer la commande systeme ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
			strcat(chemin3,"gcc -c ");
			strcat(chemin3,file_coupl);
			strcat(chemin3,";ld -shared -o ");
			strcat(chemin3,nfout);
			strcat(chemin3,"so ");
			strcat (chemin3,nfout);
			strcat(chemin3,"o ");;
		#endif
		
		#ifdef _APPLE_
			/* pour creer la commande system ("gcc -dynamiclib chemin/test_couplage.c libtest_couplage.dylib ") ; */ 
			strcat(chemin3,"gcc -dynamiclib -flat_namespace -undefined suppress ");
			strcat(chemin3,file_coupl);
			strcat(chemin3," -o lib");
			strcat(chemin3,nfout);
			strcat(chemin3,"dylib ");
		#endif

		#ifdef DEBUG
		printf("commande system %s\n",chemin3);

		printf ("C'est le programme main qui s'execute\n") ;

		printf ("Compilation/creation de la bibliotheque \n") ;
		#endif 
	      
		/* pour executer la commande */
		system(chemin3);
  
		#ifdef DEBUG
		printf ("Ouverture de la bibliotheque partagee\n") ;
		#endif

		chficso = (char *)malloc(30*sizeof(char));

		#ifdef _LINUX_
			strcat(chficso,"");
			strcat(chficso,nfout);
			strcat(chficso,"so");
		#endif
		
		#ifdef _APPLE_
			strcat(chficso,"lib");
			strcat(chficso,nfout);
			strcat(chficso,"dylib");
		#endif
		
		#ifdef DEBUG
		printf("chemin fichier so %s\n",chficso);
		#endif
	      
		lib_handle = dlopen (chficso, RTLD_LAZY) ;
		if (lib_handle){
			printf("Dynamic library opening: Successful\n");	
		} else {
			printf ("[%s] Unable to open library: %s \n", chficso, dlerror ()) ;
			exit (-1) ;
		}
		  
		#ifdef DEBUG
		printf ("Dynamic library \n") ;
		#endif
		ptrfunc = (void *) dlsym (lib_handle, "egalite_reward") ;
		if (ptrfunc == NULL){
			printf("Loading egalite_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading egalite_reward(): Successful\n");
		}

		ptraff = (void *) dlsym (lib_handle, "affiche_reward") ;
		if (ptraff == NULL){
			printf("Loading affiche_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading affiche_reward(): Successful\n");
		}
		
		#ifdef DEBUG
		printf ("Pointeur sur la fonction r�up��\n") ;
		printf ("Appel de la fonction\n") ;
		#endif
		  
		printf ("Coupling function initialisation : OK\n") ; 
		free(chemin3);
		free(chficso);
		free(nfin);
		free(nfout);
	}
	
}		
	return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : lire_param_FU
*
* Result     : int
*
* Parameters   :
*
* Nom       Type      Role
*
* nomf  char *     pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -p option, create data structures (with malloc)
*
*----------------------------------------------------------------
*/

int lire_param_FU(char *nomf){

  FILE *fd;      /* pointeur sur descripteur de fichier */
  char commande[BUFSIZ];
 
  
  
  sprintf(commande, "grep -v \"#\" %s",nomf );
	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture du fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
   } else { 
     
      /* lecture variable nombre echantillon ***/
      nbr_echantillon=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_echantillon);

      /* lecture variable nombre echantillon ***/
      nbr_VA=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_VA);

      /* lecture variable longueur trajectoire maxi ****/
      lg_traj_max=(double *)malloc(sizeof(double));
      fscanf(fd,"%lf",lg_traj_max);	
      
      /* creation en memoire du tableau de trajectoires ***/
      trajectoire=(int*)calloc((*lg_traj_max),sizeof(int));
	  
	  
      /* lecture variable nombre echantillon ***/
      germe_gener=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",germe_gener);
	  
	  lire_couplage(fd);
  }
  pclose(fd);
  return(0);
}

	  


