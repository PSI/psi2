#Number of files
3
#Queue capacity
10 10 10
#queue_minimal_initial_state
0 0 0 
#queue_maximal_initial_state
10 10 10
#Number_of_events
5
#Index file - N for No index file
File: N
#Load Sharing file - N for No Load Sharing file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0 2 1.0 3 -1 :  0  -1
1 6 0.6 3  0 :  1  -1
2 6 0.4 3  0 :  2 -1
3 1 0.6 2  1 :  -1
4 1 0.4 2  2 :  -1
