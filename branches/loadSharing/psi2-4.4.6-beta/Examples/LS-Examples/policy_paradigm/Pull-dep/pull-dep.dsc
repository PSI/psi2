#Number of files
8
#Queue capacity
20 20 20 20 20 20 20 20
#queue_minimal_initial_state
0 0 0 0 0 0 0 0
#queue_maximal_initial_state
20 20 20 20 20 20 20 20
#Number_of_events
24
#Index file - N for No index file
File: N
#Load Sharing file - N for No Load Sharing file
File: Pull-dep.ls
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0 2 0.9 3 -1 : 0 -1
1 2 0.9 3 -1 : 1 -1
2 2 0.9 3 -1 : 2 -1
3 2 0.9 3 -1 : 3 -1
4 2 0.9 3 -1 : 4 -1
5 2 0.9 3 -1 : 5 -1
6 2 0.9 3 -1 : 6 -1
7 2 0.9 3 -1 : 7 -1
8 21 1.0 1 0
9 21 1.0 1 1
10 21 1.0 1 2
11 21 1.0 1 3
12 21 1.0 1 4
13 21 1.0 1 5
14 21 1.0 1 6
15 21 1.0 1 7
16 22 1.0 1 0
17 22 1.0 1 1
18 22 1.0 1 2
19 22 1.0 1 3
20 22 1.0 1 4
21 22 1.0 1 5
22 22 1.0 1 6
23 22 1.0 1 7
