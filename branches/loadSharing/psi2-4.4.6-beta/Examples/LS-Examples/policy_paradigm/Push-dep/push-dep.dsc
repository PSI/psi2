#Number of files
8
#Queue capacity
20 20 20 20 20 20 20 20
#queue_minimal_initial_state
0 0 0 0 0 0 0 0
#queue_maximal_initial_state
20 20 20 20 20 20 20 20
#Number_of_events
16
#Index file - N for No index file
File: N
#Work Stealing file - N for No Work Stealing file
File: Push-dep.ls
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0 1 1.0 2 0 : -1
1 1 1.0 2 1 : -1
2 1 1.0 2 2 : -1
3 1 1.0 2 3 : -1
4 1 1.0 2 4 : -1
5 1 1.0 2 5 : -1
6 1 1.0 2 6 : -1
7 1 1.0 2 7 : -1
8 13 0.9 1 0
9 13 0.9 1 1
10 13 0.9 1 2
11 13 0.9 1 3
12 13 0.9 1 4
13 13 0.9 1 5
14 13 0.9 1 6
15 13 0.9 1 7
