/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file transition.h
* \brief Header of transition.c
* \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/

#ifndef _H_transition
#define _H_transition

/****************************************************************/
/*********  SIGNATURE DES FONCTIONS LOCALES  *********************/
/****************************************************************/

/* fonction arrivee exterieur sur une file destination  **************/
int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par);

/***  fonction sortie exterieure depuios une file origine ***********/
int sortie_ext(int etat[],int param1);

/***  fonction routage n files avec rejet ***********/
int *  routage_nfile_rejet (int etat[],int nb_dest,int * par);

/***  fonction routage n files avec blocage ***********/
int *  routage_nfile_bloc (int etat[],int nb_dest,int * par);

/******* fonction JSQ  arrivee  avec rejet *********/
int *  Arrivee_rejet (int etat[],int nb_dest,int * par, int * numf);

/******* fonction JSQ  routage avec rejet *********/
int *  JSQ_rejet (int etat[],int nb_dest,int * par);


/******* fonction multi serveur index avec rejet *********/
int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf, int ** tab);
/******* fonction multi serveur index avec rejet *********/

int * Independent_Pull(int etat[], int num_ori, int * rand_par);

int * Independent_Push(int etat[], int num_ori, int * rand_par);

int * Arrival_Push(int etat[], int num_ori, int * rand_par);

double ws_index(int num_ori, int num_cible, int dist, int ch_cible);


/**** implementation de modele LTM (cf These Marise Beguin) ****/

int * LTM_arrival(int etat[], int num_ori, int* parametre);

int * LTM_departure(int etat[], int num_ori, int* parametre);


/**** non monotone load sharing systems *************/

int Departure_Pull(int etatInf[], int etatSup[], int num_ori, int * rand_par);

int * EmptyQueue_Pull(int etat[], int num_ori, int * rand_par);

/**************************************************************/
/*****  SIGNATURE DES FONCTIONS EXPORTABLES  ******************/
/**************************************************************/

/***  fonction de transition  interface            *****/
extern void transition_interface(int etat[], int evenement, int* parametre);

/***  fonction de transition  interface with envelopes            *****/
extern void transition_interface_envelope(int etatInf[], int etatSup[], int evenement, int* parametre);

#endif             /* _H_transition ***/
