/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */







#ifndef _H_lect_model
#define _H_lect_model

/*! \file lect_model.h
* \brief Header of lect_model.c
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/


/* structure du tableau des evenements lu dans le fichier client ***/

struct st_evt{
    int id_evt;  
    int typ_evt;
    double lambda;
	int nbr_file_evt;
	int *param_evt;
	int *num_fct;
	int num_ori;
} ;


/* structure permettant de decrire une topologie hierarchique de type grille de calcul pour
	 les evenements de Work Stealing */
struct st_hier_node{
	int num; // numero du noeud
	int level; // niveau de hierarchie dans l'arbre
	struct st_hier_node * father; // pointeur vers le noeud pere
	int nbsons; // nombre de noeuds fils
	struct st_linked_sons * sons;
} ;


/* structure permettant le chainage des fils d'un noeud (ajout et retrait dynamique */
struct st_linked_sons{
	struct st_linked_sons * next;
	struct st_hier_node * val;
};


struct st_neighbors {
	int nb_neighbors;
	int * neighbors;
};


struct st_multi{
	int **queue_list;
};



/**********************************************************/
/******** SIGNATURE DES FONCTIONS EXPORTEES ***************/
/**********************************************************/
/*** fonction lire contenu du fichier des parametres *******/
extern int lire_param_FU(char *nomf);

/*** fonction lire contenu du fichier de donnees  *******/
extern int lire_data_FU(char *nomf);

/*** function: Read table of index ***/
extern int read_index_FU(char *nomf);

extern int lire_hier(FILE * f);

extern int make_table_node_dist();

#endif /* _H_lect_model */
