/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file data.h
 * \brief Header of data.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr 
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */

#ifndef _H_Data
#define _H_Data

/***********************************************************/
/********* SIGNATURES DES FONCTIONS LOCALES ****************/
/***********************************************************/


/************************************************************/
/**** SIGNATURE DES FONCTIONS EXPORTABLES  ******************/
/************************************************************/

/**** initialisation **************************/

/* fixe les valeurs initiales etat_inf, etat_sup, couplage */
inline void init_etat();

/* fixe les valeurs initiales couplage */
inline void init_couplage();

/* effectue le calcul de probabilite des evenements a partir des valeurs de lambda saisis */
inline void init_probabilite();

/*  affecte les capacites saisies al'etat_sup intial *****/
inline void set_etat_init_supG();

/*************** fixateurs **************************/

/* fixe l'evenement alternatif pour un evenement ****/
inline void set_evt_alt(int j,int k);

/* fixe le seuil pour un evenement   */
inline void set_seuil(int i,double seuil);

/* affecte le couplage a l'iteration log2tempsarret *****/
inline void set_couplage(int i,int log2tempsarret);

/* affecte une valeur dans le tableau des trajectoires   *****/
inline void set_trajectoire(int i,int j);

/*********** modificateurs ***********************/

/* ajoute un element dans la  file de rang n*/
inline void ajout_file(int n); 

/* supprime un element dans la file derang n */
inline void supp_file(int n);

/**** accesseurs ********************************************/

/*   renvoie l'etat de la file de numero (nombre d'element) */
inline int etat_file(int n);

/* renvoie le nombre de file du systeme */
inline int get_nb_file();

/* renvoie le nombre d'evenements du systeme */
inline int nb_evenements();

/* renvoie le nombre de file d'une eveneement */
inline int get_nbr_file_evt(int i);

/* renvoie l'adresse du premier element du tableau des capacites ****/
inline int * get_capacite();

/* renvoie la valeur de la capacite de la file de numero n */
inline int capacite_file(int n);

/* renvoie la valeur du taux de probabilite de l'evenement i ****/
inline double get_taux(int i);

/* renvoie le seuil d'un evenement */
inline double  get_seuil(int i);

/* renvoie l'evnement alternatif d'un evenement */
inline int get_evt_alt(int i);

/* renvoie le nombre d'echantillon de la simulation  ***/
inline int get_taille_ech();

/*  recupere la trajectoire correspondant a l'etape i ***/
inline int get_trajectoire(int i);

/* renvoie l'adrsse du premier element du tableau des etats sup ****/
inline int * get_etat_sup();

/* renvoie l'adresse du premier element du tableau des etats inf ****/
inline int * get_etat_inf();

/* renvoie le statut de couplage a  l'etape i ******/
inline int get_couplage(int i);

/*  renvoie la longueur maxi du trajet *****/
inline double get_lg_traj_max();

/* renvoie le type d'un evenement *****/
inline int get_type_evt(int evenement);

/* renvoie le numer o de file associe a une evt et un indice ****/
inline int get_param(int evenement ,int indice_file);

/* renvoie le numero de la fonction d'index associe a une evt et un indice ****/
inline int get_numf(int evenement ,int indice_file);

/*  renvoie pour l'evenement i la liste des files src */
inline int get_nbr_file_evt(int evenement);


/* renvoie le nombre de jeton dans l'etat initial inferieur d'une file ****/
inline int get_etat_init_infU(int num_file);

/* renvoie le nombre de jeton dans l'etat initial superieur d'une file *****/
inline int get_etat_init_supU(int num_file);

/*  renvoie pour l'evenement i le tableau des files multiserveurs */
inline int ** get_multi_server(int evenement);

/* affichage ecran  des etats *******/
inline void imprime_etat(int *etat1);

#endif     /* _H_Data  */
