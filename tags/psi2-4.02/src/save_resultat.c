/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file save_resultat.c
 * \brief Functions of save_resultat.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 *
 * \date Juillet 21 , 2004
 */


/*
*------------------------------------------------------------------
* Fichier      : save_resultat.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi
* Creation     : 21 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme secondaire permettant la sauvegarde des resultats de la simulation 
* pour analyse ulterieure  
*  
*-------------------------------------------------------------------------------
*/

/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "save_resultat.h"
#include "data.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/


/***********************************************************/
/********* SIGNATURES DES FONCTIONS LOCALES ****************/
/***********************************************************/


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/**********************************************************/
/******* DECLARATION DES VARIABLES IMPORTEES **************/
/**********************************************************/
extern int rad;


/**********************************************************/
/********* DEFINITIONS DES VARIABLES EXPORTEES  **************/
/**********************************************************/
FILE *descfp;

/**********************************************************/
/********* IMPLANTATION  DES FONCTIONS EXPORTEES *************/
/**********************************************************/

/**
*----------------------------------------------------------------
*
* Function     : sauve_present_RS
*
* Result       :
*
* Parameters   :
*
* Name       Type      Role
* 
* nomd	char *	      pointer on the character strings including the name of the  data file 
*					  with his relative access path
*
* nomp	char *	      pointer on the character strings including the name of the  parameter file 
*					  with his relative access path
*
* nomf  char *       pointer on the character strings including the name of the  save file 
*					  with his relative access path
* 
* 
* Called function : get_nb_file
* 
* Description  :
*	 Save the data of the present simuation on the file passed on parameter with "-o" option    
*
*----------------------------------------------------------------------
*/
int sauve_present_RS(char *nomd,char *nomp, char *nomf){
   int k;
   char ligne[100];
   FILE *entree;
   
   if ((descfp = fopen(nomf,"w")) == NULL ){
      printf("Pb d'ouverture de fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
    }
    else{ 
        if ((entree = fopen(nomd,"r")) == NULL ){
            printf("Pb d'ouverture de fichier '%s' \n",nomf);
            perror("Le Pb est : ");
            return(1);
        }else{
            
            fprintf(descfp,"# Data Network model  ");
            fprintf(descfp,"\n");
            while (fgets(ligne,sizeof(ligne), entree) !=  NULL){
                if(ligne[0]=='#'){
                   fprintf(descfp,"%s",ligne);
                }else{
                    fprintf(descfp,"# %s",ligne);
                }
            }
        
            fclose(entree);
        }
        
        if ((entree = fopen(nomp,"r")) == NULL ){
            printf("Pb d'ouverture de fichier '%s' \n",nomf);
            perror("Le Pb est : ");
            return(1);
        }else{
            //fprintf(descfp,"");
            fprintf(descfp,"# Parameter");
            fprintf(descfp,"\n");
            while (fgets(ligne,sizeof(ligne), entree) !=  NULL){
                if(ligne[0]=='#'){
                   fprintf(descfp,"%s",ligne);
                }else{
                    fprintf(descfp,"# %s",ligne);
                }
            }
        
            fclose(entree);
        }
    //fprintf(descfp,"");
    fprintf(descfp,"# Result");
    fprintf(descfp,"\n");
    fprintf(descfp,"# sample index  S-nb\n");
    fprintf(descfp,"# iteration nb  I-nb\n");
    fprintf(descfp,"# Max state at 0 queue  n  Max   \n");
    fprintf(descfp,"# Min state at 0 queue  n  Min0  \n");
    fprintf(descfp,"# S-nb:");
    fprintf(descfp,"  I-nb:");
    fprintf(descfp,"  Max:");
  
   for(k=0;k<get_nb_file();k++)
     fprintf(descfp,"%4d",k);
   fprintf(descfp,"   Min:");    
   for(k=0;k<get_nb_file();k++)
     fprintf(descfp,"%4d",k);
   
   fprintf(descfp,"\n#===================================================================================================\n");
   
   
   return(0);
    }
   
}

/**
*----------------------------------------------------------------
*
* Function     : sauve_etat
*
* Result       : void 
*
* Parameters   :
*
* Nom       Type      Role
*
* etat1     int  *        pointer on etat vector 
* 
* Called function : get_nb_file
* 
* Description  :
*   
*	write in the file descriptor the state of the vector passed in parameter
*
*----------------------------------------------------------------------
*/


void sauve_etat(int *etat1)
{
  int * etat=etat1;
 int i_boucle;
 int NB_FILE=get_nb_file();
 for (i_boucle = 0; i_boucle < NB_FILE ; i_boucle++)
   fprintf(descfp,"%3d ",etat[i_boucle]);
}

