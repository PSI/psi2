/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file modele.c
 * \brief Functions of modele.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : modele.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme coeur de l'application effectuant les calculs de simulation
*
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "modele.h"
#include "data.h"
#include "transition.h"
#include "lect_model.h"
#include "save_resultat.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/

#define EPS 1e-12      

#define DEBUGT 1
/* #define DEBUG 1 */
/*#define AFFECR  */ /* pour affichage a l'ecran de ce qu'on ecrit dans le ficheir resultat */

/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

int rad;  /* variable initialisant le generateur aleatoire ***/


/***********************************************************/
/******** DECLARATIONS DES VARIABLES EXTERNES **************/
/***********************************************************/

extern FILE *descfp;
extern int (*ptrfunc)();
extern int nbparamlc;
extern int *germe_gener;
extern int num_file_coup_deb;
extern int num_file_coup_fin;   
extern double **table_index;

/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/
/**
*----------------------------------------------------------------
*
* Function     : construction
*
* Result     : void
*
* Parameters   : anything 
*
*
* Called functions : nb_evenements,get_taux,set_seuil,set_evt_alt
*
* Description  :
*
*      structure construction of generation based on Walker algo 
*	   all that in statics because it is pretraitement
*      
*----------------------------------------------------------------------
*/
void construction() /* construction de la structure de generation base sur algo de Walker
		       tout cela en statique car c'est le pretraitement */
{
  int NB_EVTS=nb_evenements();
  
  int   j,k;                /* indice j pointe sur le premier Low et k sur le premier High */
  int   L[NB_EVTS];   /* Liste des objets a traiter (indice suivant dans la liste L et H)*/
  int   j_boucle,l_auxiliaire;
  double R[NB_EVTS];

  j=NB_EVTS;          /* le test L vide se fait par (j==nb_evenements) */
  k=NB_EVTS;          /* le test H vide se fait par (k==nb_evenements) */
  for (j_boucle = 0; j_boucle< NB_EVTS; j_boucle++)
    { 
      R[j_boucle]=get_taux(j_boucle)*NB_EVTS;
      set_seuil(j_boucle,R[j_boucle]);
      /* on initialise les seuils a partir de P */
      if (R[j_boucle] < 1)
	/* l'indice j_boucle est place dans la liste L le premier element est pointe par j*/
	{
	  L[j_boucle]=j;
	  j = j_boucle;
       }        
      else /* l'indice i_boucle est place dans la liste H le premier
	      element est pointe par k */         
	{
	  if (R[j_boucle] > 1)
	    {
	      L[j_boucle]=k;
	      k = j_boucle;
	    }
	}
    }

  while (j != NB_EVTS)
    {
      set_evt_alt(j,k);
      
      R[k]=R[k]+R[j]-1;
      if (fabs(R[k]-1) < EPS)
	{         /* j et k sont bons et on repart avec les suivants */
	  set_seuil(k,1);
	  k=L[k];
	  j=L[j];
	}
      else
	{
	  if (R[k] < 1)
	    { /* on insere k dans la liste L (ici on le place en tete )*/
	      set_seuil(k,R[k]);
	      l_auxiliaire=L[k];
	      L[k]=L[j];
	      j=k;
	      k=l_auxiliaire;
	    }
	  else     /*** R[k] >= 1 *****/
	    { /* on supprime l'element j des listes L et H */
	      j = L[j];
	    }
	}
    }
}/* fin de la generation de la matrice des seuils */

/**
*----------------------------------------------------------------
*
* Function     : u01
*
* Result       : double
*
* Parameters   : anything
*
* Description  :
*
*      uniform law on 0,1
*
*      optimisation pour eviter l'operateur de division qui est couteux 
*      
*----------------------------------------------------------------------
*/

double u01() 
{
 return( (double) random() * .4656612875e-9);
}

/**
*----------------------------------------------------------------
*
* Function     : init_generateur
*
* Result       : void
*
* Parameters   : anything
*
* Description  :
*
*      Initialisation du generateur se fait soit manuellement pour pouvoir regenerer exactement 
* la meme sequence a partir du meme nombre soit aleatoirment a partir de la fonction getpid()
*      
*----------------------------------------------------------------------
*/
void init_generateur() 
{
  
  if( germe_gener != NULL)
    {
      rad=*germe_gener;
      srandom(rad);
    }
  else 
    {
      rad=getpid();
      printf("valeur initialisant le generateur aleatoire %d\n",rad);
      srandom(rad);
    }
}

/**
*----------------------------------------------------------------
*
* Function     : genere_evenement
*
* Result      : int
*
* Parameters   : anything
*
* Called functions : nb_evnements,get_seuil,get_evt_alt
*
* Description  :
*
*       function which generate an event
*
*      optimisation pour eviter l'operateur de division qui est couteux 
*      
*----------------------------------------------------------------------
*/

int genere_evenement()
{
  int evenement;
  double x;
  x = nb_evenements()*u01();
  evenement =(int) x;  
  x = x - evenement;
  if (x <= get_seuil(evenement))
    {
      return evenement;  /**** retourne evenement *****/
    }
  else
    {
      return get_evt_alt(evenement); /* retourne alias de l'evenement si au dessus du seuil */
    }
}



/***********************************************************************/
/**** IMPLANTATION DES FONCTIONS EXPORTABLES   **************************/
/***********************************************************************/


/**
*----------------------------------------------------------------
*
* Function     : test_couplage  
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat1     int *     pointer on etats1 table
*
* etat2     int *     pointer on etats2 table
*
* Called function : get_nb_file
*
* Description  :
*
*     pour tester le couplage  entre 2 vecteurs d'un ensemble de file 
* compris entre le numero i et le numero j 
* renvoie 0 si au moins une file n'a pas encore couple
* et une valeur differente de zero si l'ensemble des files ont couples
*
*----------------------------------------------------------------------
*/

int test_couplage() 
{
  int i=0;
  int j=(get_nb_file()-1);
  int i_boucle,val_test;
  val_test=1;
  for (i_boucle = i; i_boucle<= j; i_boucle++)
    val_test=val_test && (get_couplage(i_boucle));
  return val_test;
}

/**
*----------------------------------------------------------------
*
* Function     : lancement_single
*
* Result       : void
*
* Parameters   : anything
*
* Called functions : init_generateur,construction,get_taille_ech,get_lg_traj_max,get_nb_file,
*       set_trajectoire,genere_evenement,init_etat,transition_interface,get_etat_inf,get_trajectoire,get_etat_sup,
*          test_couplage,get_couplage,set_couplage,cout_etat,sauve_etat  
*
* Description  :
*
*       fonction de lancement du module de simulation fait appel aux fonctions 
*     init_generateur
*     construction
*      pour chaque echantillon effectue des iterations dans le passe jusqu'a ce qu'il y ait couplage
*     ou que le nombre maxi de trajectoire soit atteint
*     affiche le resultat pour chaque echantillon
*          soit il y a eu couplage et dans ce cas affiche l'etat de chaque file au moment du couplage
*          soit il n'y a pas eu couplage nombre de trajectoire maxi atteint et dans ce cas 
* affiche echec
*
*     affiche a la fin la duree moyenne d'un tirage 
*      
*---------------------------------------------------------------------------------
*/
void lancement_single()
{
  init_generateur();
  construction();
  
  
  int echantillon;           /* parametres de la simulation */
  int temps_arret,indice,log2temps_arret,i;
  int fin;                   /* teste si on a couplage */
  //int value;
  struct timeval ti,tf; /* structure de mesure de la duree de simulation */

  int NB_ECH = get_taille_ech();
  int LG_TRAJ_MAX = get_lg_traj_max();
  int NB_FILE=get_nb_file();
  int * etat_inf;
  int * etat_sup;
  
  printf("\n");
  printf(" Perfect Simulation started \n");
  gettimeofday(&ti,NULL);
 
  for (echantillon = 0 ; echantillon < NB_ECH ; echantillon++){    
      
      temps_arret = 1;
      log2temps_arret = 0;    /* initialisation du nombre d'iteration */
      set_trajectoire(0,genere_evenement()); 
	  
	  init_couplage();
      do{
		init_etat();
		indice=temps_arret;
		/* generer les evenements de l'indice temps_arret a 2*temps_arret */
		/* de indice 2*temps_arret a  temp_arret  on genere de nouvelles trajectoires */
		
		set_trajectoire(indice,genere_evenement());  /* renvoie un evenement ******/
		transition_interface(get_etat_inf(),get_trajectoire(indice)); 
		/* appel a fonction de transition qui a un etat_inf  evenement associe un etat_inf*/
		transition_interface(get_etat_sup(),get_trajectoire(indice)); 
		/* appel a fonction de transition qui a un etat_sup  evenement associe un etat_sup */
	   
	    /*  de indice temps_arret-1 a  O on reprend les memes trajectoires *********************/
		for (indice = temps_arret-1; indice>=0; indice--){
			transition_interface(get_etat_inf(),get_trajectoire(indice));
			transition_interface(get_etat_sup(),get_trajectoire(indice));
	    }        
		temps_arret=temps_arret+1;
		log2temps_arret++;
		for (i=0; i<NB_FILE; i++){
			etat_inf=get_etat_inf();
			etat_sup=get_etat_sup();
			/* on mesure pour chaque file d'attente le temps de couplage   *******/
			if ( (get_couplage(i) == 0) && (etat_inf[i] == etat_sup[i])){
				//printf("Echantillon %d la file %d couple avec un temps %d\n", echantillon, i, log2temps_arret);
				set_couplage(i, log2temps_arret);
				
			}
		}   /* fin du for *****/
		/* on verifie si l'ensemble des files d'attente ont couples    ********/
		if(ptrfunc == NULL)
			fin = test_couplage();
		else
			fin = (*ptrfunc)();      
		} while((temps_arret < LG_TRAJ_MAX) && (! fin));   /**** do while ********/
     
		if (fin){
			fprintf(descfp,"%6d    ",echantillon);
			fprintf(descfp," %3d ",log2temps_arret);
			for (i=0; i<NB_FILE; i++){
				fprintf(descfp," %3d ", get_couplage(i));
			}
			fprintf(descfp,"           ");
			sauve_etat(get_etat_sup());
			fprintf(descfp,"          ");
			sauve_etat(get_etat_inf());
			fprintf(descfp,"\n");
		} else {
			fprintf(descfp,"%d echec %d \n",echantillon,log2temps_arret);
		}
	}
	
	gettimeofday(&tf,NULL);
	fprintf(descfp,"# taille %4d  duree d'un tirage  : %f micro-secondes\n",NB_ECH,
	  ((double) ((tf.tv_sec-ti.tv_sec)*1000000 + (tf.tv_usec-ti.tv_usec)))/NB_ECH);
	fprintf(descfp,"# valeur initiale du randomize %d\n",rad);
}


/**
*----------------------------------------------------------------
*
* Function     : lancement
*
* Result       : void
*
* Parametres   : anything
*
* Called functions : init_generateur,construction,get_taille_ech,get_lg_traj_max,get_nb_file,
*       set_trajectoire,genere_evenement,init_etat,transition_interface,get_etat_inf,get_trajectoire,get_etat_sup,
*          test_couplage,get_couplage,set_couplage,cout_etat,sauve_etat  
*
* Description  :
*
*       fonction de lancement du module de simulation fait appel aux fonctions 
*     init_generateur
*     construction
*      pour chaque echantillon effectue des iterations dans le passe jusqu'a ce qu'il y ait couplage
*     ou que le nombre maxi de trajectoire soit atteint
*     affiche le resultat pour chaque echantillon
*          soit il y a eu couplage et dans ce cas affiche l'etat de chaque file au moment du couplage
*          soit il n'y a pas eu couplage nombre de trajectoire maxi atteint et dans ce cas 
* affiche echec
*
*     affiche a la fin la duree moyenne d'un tirage 
*      
*---------------------------------------------------------------------------------
*/
void lancement()
{
  init_generateur();
  construction();
  
  
  int echantillon;           /* parametres de la simulation */
  int temps_arret,indice,log2temps_arret,i;
  int fin;                   /* teste si on a couplage */
  struct timeval ti,tf; /* structure de mesure de la duree de simulation */

  int NB_ECH = get_taille_ech();
  int LG_TRAJ_MAX = get_lg_traj_max();
  int NB_FILE=get_nb_file();
  int * etat_inf;
  int * etat_sup;
  printf("\n");
  printf("Perfect Simulation (with doubling period) started \n");

  gettimeofday(&ti,NULL);
 
  for (echantillon = 0 ; echantillon < NB_ECH ; echantillon++)
    {    
      
	  temps_arret = 1;
      log2temps_arret = 0;    /* initialisation du nombre d'iteration */
      set_trajectoire(0,genere_evenement()); 
	
	 init_couplage();
      do
	{
	  init_etat();
	  indice=0;
	  /* generer les evenements de l'indice temps_arret a 2*temps_arret */
	  /* de indice 2*temps_arret a  temp_arret  on genere de nouvelles trajectoires */
	  for (indice = 2*temps_arret-1; indice >= temps_arret; indice--)
	    {
	      set_trajectoire(indice,genere_evenement());  /* renvoie un evenement ******/
	      transition_interface(get_etat_inf(),get_trajectoire(indice)); 
	         /* appel a fonction de transition qui a un etat_inf  evenement associe un etat_inf*/
	      transition_interface(get_etat_sup(),get_trajectoire(indice)); 
	         /* appel a fonction de transition qui a un etat_sup  evenement associe un etat_sup */
	    }
	    /*  de indice temps_arret-1 a  O on reprend les memes trajectoires *********************/
	  for (indice = temps_arret-1; indice>=0; indice--)
	    {
	      transition_interface(get_etat_inf(),get_trajectoire(indice));
	      transition_interface(get_etat_sup(),get_trajectoire(indice));
	    }        
	  temps_arret=2*temps_arret;
	  log2temps_arret++;
	  for (i=0; i<NB_FILE; i++)
	    {
	      etat_inf=get_etat_inf();
	      etat_sup=get_etat_sup();
	      /* on mesure pour chaque file d'attente le temps de couplage   *******/
	      if ((get_couplage(i) == 0) && (etat_inf[i] == etat_sup[i]))
		set_couplage(i, log2temps_arret);
	    }   /* fin du for *****/
	  /* on verifie si l'ensemble des files d'attente ont couples    ********/
	  if(ptrfunc == NULL)
	    fin = test_couplage();
	  else
	    fin = (*ptrfunc)();      
	} 
      while((2*temps_arret < LG_TRAJ_MAX) && (! fin));   /**** do while ********/
     
      if (fin)
	{
	  fprintf(descfp,"%6d    ",echantillon);
	  fprintf(descfp," %3d ",log2temps_arret);
	  fprintf(descfp,"           ");
	  sauve_etat(get_etat_sup());
	  fprintf(descfp,"          ");
	  sauve_etat(get_etat_inf());
	  fprintf(descfp,"\n");
	}
      else 
	{
	  fprintf(descfp,"%d echec %d \n",echantillon,log2temps_arret);
	}
    }
  gettimeofday(&tf,NULL);
  fprintf(descfp,"# taille %4d  duree d'un tirage  : %f micro-secondes\n",NB_ECH,
	  ((double) ((tf.tv_sec-ti.tv_sec)*1000000 + (tf.tv_usec-ti.tv_usec)))/NB_ECH);
  fprintf(descfp,"# valeur initiale du randomize %d\n",rad);
}

