/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file psi2.c
 * \brief Functions of psi2.c . 
 * \author Bernard.Tanzi@imag.fr
 * \author Jean-Marc.Vincent@imag.fr 
 * \author Jerome.vienne@imag.fr
 *
 * \version 4.0
 * \date 2004-2006
 */

/*
*------------------------------------------------------------------
* Fichier      : psi2.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme principal contenant le main permettant de lancer l'execution 
*   des differents traitements de l'application
*-------------------------------------------------------------------------------
*/

#ifdef HAVE_CONFIG_H
#include "../config.h"
#endif


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>

/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "data.h"
#include "modele.h"
#include "lect_model.h"
#include "save_resultat.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/



/***********************************************************/
/********* SIGNATURES DES FONCTIONS LOCALES ****************/
/***********************************************************/


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/*************************************************************/
/******* DECLARATION DES VARIABLES EXTERNES AU MODULE ********/
/************************************************************/
extern int *lg_traj_max;
extern int *trajectoire;
extern int *nbr_echantillon;

extern int *capfile;
extern int *etat_init_supU;
extern int *etat_init_infU;

extern int *etat_inf;
extern int *etat_sup;
extern int *nb_jeton;
extern int *couplage;

extern int *nb_evt;

/** pointeurs sur tableaux de tailles nombres evenements modeles statistiques ***/
extern double *P;
extern double *R;
extern int *A;

extern struct st_evt *evt;
extern FILE *descfp;


/******************************************************************/
/***** DECLARATION DES VARIABLES EXPORTEES *************************/
/*******************************************************************/

void *lib ;
int (*ptrfunc)(int i,int j);
int nbparamlc;

/*! Executable name. */
char *prog;   


/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/*!
* \fn help_sample
* \brief Print on screen the help message. 
*/

void help_sample()
{
  printf(" USAGE : %s [-ipoc] argument [-hv]\n",prog);
  printf("\n  -i : input file in ext directory \n");
  printf("\n  -p : parameter file in ext directory\n");
  printf("\n  -o : output file in ext directory \n");
  printf("\n       By default, output file has outputtest.txt name in ext directory\n");
  printf("\n  -c : input test_couplage file \n");
  printf("\n       If -c option is not specified, test_couplage is the default couplage function.\n");
  printf("\n  -t : Without doubling period, show coupling time of each queue\n");
  printf("\n  -h : help.\n");
  printf("\n  -v : version\n"); 
  printf(" \n");
}

/*!
* \fn version_sample
* \brief Print on screen the soft version
*/

void version_sample()
{
  printf(" P.S.I.2 version 4.02 - Perfect Simulator 2 \n");
  printf(" Authors : Bernard.Tanzi@imag.fr\n");
  printf("           Jean-Marc.Vincent@imag.fr\n");
  printf("           Jerome.Vienne@imag.fr\n");
  printf(" Web : http://www-id.imag.fr \n");
  printf(" \n");
}



/******************************************************************/
/******** IMPLANTATION DES FONCTIONS EXPORTEES ********************/
/******************************************************************/




/*!
* \fn main
* \brief main function of psi2
* \param int argc: number of parameter of the function
* \param char*[] argv table of pointer which contain all the different parameters
* \return int 0 if all is ok
*/

int main(int argc,char **argv) {
  nbparamlc=argc; 

  char c;
  char *opt_i=NULL,*opt_o=NULL,*opt_c=NULL;
  char *opt_p=NULL,*opt_t=NULL;
  prog=argv[0];
  int compt_o = 0;
  int compt_c = 0;
  int compt_h = 0;
  int compt_i = 0;
  int compt_t = 0;

  printf(" \n");
  while((c = getopt(argc,argv,"hvti:o:p:c:")) != -1){
    switch(c)
      {
	case 'h': help_sample(); 
	   compt_h ++;
	   compt_i ++; // to avoid repetition help message
	   break;

       case 'v': version_sample();
	   compt_h ++;
	   compt_i ++; // to avoid repetition help message
	   break;
	 
	 case 'i' : opt_i = optarg;
	   compt_i ++;
	   break;
	
	 case 'o' : opt_o = optarg;
	   compt_o ++; // utilization of -o option
	   break;
	
	 case 'p' : opt_p = optarg;
	   break;
	
	 case 'c' : opt_c = optarg;
	   compt_c ++; // utilization of -c option
	   break;

	 case 't' : opt_t = optarg;
	   compt_t ++; // utilization of -s option
	   break;
	 }
    }

  if((c == -1) && (compt_i == 0)) // no option specified
    {
      help_sample();
      compt_h++;
    }
  else
    {
      if(compt_h == 0)  // backward simulation called
	{
	  /* cas fichier input output param et test_couplage fournis */
	  if (compt_o == 1 && compt_c == 1)
	    {
	      char *chemin1;
	      char *chemin2;
	      char *chemin3;
	      char *chemin4;
	      char *nfin;
	      char *nfout;
	      char *chficso;

	      /* pour stocker le nom de fichier sans son extension */

	      nfin = (char *) malloc(40*sizeof(char));
	      nfout = (char *) malloc(40*sizeof(char)); 
	      strcpy(nfin,"");
	      strcat(nfin,opt_c);
	      strcat(nfin,"\0");
#ifdef DEBUG
	      printf("nom de fichier : %s\n",nfin);
#endif
	      int lgfile = strlen(nfin);
#ifdef DEBUG
	      printf("longueur du fichier %d\n",lgfile);
#endif
	      int i;
	      for(i=0;i<(lgfile-1);i++){
		*(nfout+i)=nfin[i];
	      }
	      strcat(nfout,"\0");
#ifdef DEBUG
	      printf("nom de fichier : %s\n",nfout);
#endif

	      /* pour cr�r la commande system ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
	      chemin3=(char *)malloc(100*sizeof(char));
	      strcpy(chemin3,"");
	      strcat(chemin3,"gcc -c ");
	      strcat(chemin3,opt_c);
	      strcat(chemin3,";ld -shared -o ");
	      strcat(chemin3,nfout);
	      strcat(chemin3,"so ");
	      strcat (chemin3,nfout);
	      strcat(chemin3,"o ");
#ifdef DEBUG
	      printf("commande system %s\n",chemin3);

	      printf ("C'est le programme main qui s'execute\n") ;

	      printf ("Compilation/creation de la bibliotheque \n") ;
#endif 
	      /* pour executer la commande */
	      system(chemin3);
  
#ifdef DEBUG
	      printf ("Ouverture de la bibliotheque partagee\n") ;
#endif

	      chficso = (char *)malloc(30*sizeof(char));
	      strcat(chficso,"");
	      strcat(chficso,nfout);
	      strcat(chficso,"so");
#ifdef DEBUG
	      printf("chemin fichier so %s\n",chficso);
#endif
	      lib = dlopen (chficso, RTLD_LAZY) ;
	      if (lib == NULL)
		{
		  printf ("%s \n", dlerror ()) ;
		  exit (-1) ;
		}
#ifdef DEBUG
	      printf ("Bibliotheque partagee ouverte\n") ;
#endif
	      ptrfunc = (void *) dlsym (lib, "test_couplage") ;
	      if (ptrfunc == NULL)
		{
		  perror ("dlsym ") ;
		  exit (-1) ;
		}
#ifdef DEBUG
	      printf ("Pointeur sur la fonction r�up��\n") ;
	      printf ("Appel de la fonction\n") ;
#endif
	      chemin1=(char *)malloc(150*sizeof(char)); 
	      strcpy(chemin1,"");
	      strcat(chemin1,opt_i);    
	      lire_data_FU(chemin1); 

	      chemin4=(char *)malloc(150*sizeof(char));
	      strcpy(chemin4,"");
	      strcat(chemin4,opt_p);
	      lire_param_FU(chemin4); 
              
		  chemin2=(char *)malloc(150*sizeof(char));
	      strcpy(chemin2,"");
	      strcat(chemin2,opt_o);
	      sauve_present_RS(chemin1, chemin4, chemin2);
              
	      init_probabilite();
	      
	      if (compt_t == 0){
			lancement();
	      } else {
			lancement_single();
	      }

	      free(lg_traj_max);
	      free(trajectoire);
	      free(nbr_echantillon);
	      free(chemin1);
	      free(chemin2);
	      free(capfile);
	      free(etat_init_supU);
	      free(etat_init_infU);
	      free(etat_inf);
	      free(etat_sup);
	      free(nb_jeton);
	      free(couplage);
	      free(nb_evt);
    
	      free(P);
	      free(R);
	      free(A);
	      free(evt);
	      fclose(descfp);

	      free(chemin4);
	      free(chemin3);
	      free(nfin);
	      free(nfout);
	      free(chficso);
#ifdef DEBUG
	      printf ("Fermeture de la bibloth�ue partag�\n") ;
#endif
	      dlclose (lib) ;
#ifdef DEBUG
	      printf ("La biblioth�ue partag� est ferm�\n") ;
#endif
	    }
	  /* cas pas de fonction test_couplage fourni */
	  else if (compt_o == 1 && compt_c  == 0)
	    {
	      char *chemin1;
	      char *chemin2;
	      char *chemin4;
    
 
	      chemin1=(char *)malloc(150*sizeof(char)); 
	      strcpy(chemin1,"");
	      strcat(chemin1,opt_i);    
	      lire_data_FU(chemin1); 
	      
	      chemin4=(char *)malloc(150*sizeof(char));
	      strcpy(chemin4,"");
	      strcat(chemin4,opt_p);
	      lire_param_FU(chemin4); 
	      
		  chemin2=(char *)malloc(150*sizeof(char));
	      strcpy(chemin2,"");
	      strcat(chemin2,opt_o);
	      sauve_present_RS(chemin1, chemin4, chemin2);
              
	      init_probabilite();
	      
	      if (compt_t == 0){
			lancement();
	      } else {
			lancement_single();
	      }
	      
	      
	      free(lg_traj_max);
	      free(trajectoire);
	      
	      free(nbr_echantillon);
	      free(chemin1);
	      free(chemin2);
	  
	      free(capfile);
	      free(etat_init_supU);
	      free(etat_init_infU);
	      /*free(etat_inf);
		free(etat_sup);*/
	      free(nb_jeton);
	      free(couplage);
	      free(nb_evt);
	      
	      free(chemin4);
	     
	      free(P);
	      free(R);
		  free(A);
	      free(evt);
	      fclose(descfp);
	    }
	  /* cas pas de fichier output fourni  pas de fonction test_couplage fourni */
	  else if ( compt_o == 0 && compt_c == 0)
	    {
	      char *chemin1;
	      char *chemin2;
	      char *chemin4;
	      
	      
	      chemin1=(char *)malloc(150*sizeof(char)); 
	      strcpy(chemin1,"");
	      strcat(chemin1,opt_i);    
	      lire_data_FU(chemin1); 
	      
	      chemin4=(char *)malloc(150*sizeof(char));
	      strcpy(chemin4,"");
	      strcat(chemin4,opt_p);
	      lire_param_FU(chemin4); 
	      
	      chemin2=(char *)malloc(150*sizeof(char));
	      strcpy(chemin2,"");
	      strcat(chemin2,"./outputtest.txt");
	      sauve_present_RS(chemin1, chemin4, chemin2);
              
	      init_probabilite();
	      
	      if (compt_t == 0){
			lancement();
	      } else {
			lancement_single();
	      }
	      
	      free(lg_traj_max);
	      free(trajectoire);
	      free(nbr_echantillon);
	      free(chemin1);
	      free(chemin2);
	      free(chemin4);
	      free(capfile);
	      free(etat_init_supU);
	      free(etat_init_infU);
	      /* free(etat_inf);
		 free(etat_sup); */
	      free(nb_jeton);
	      free(couplage);
	      free(nb_evt);

	      
	      free(P);
	      free(R);
	      free(A);
	      free(evt);
	      fclose(descfp);
	      
	    }
	  
	}
    }
  return 0;     
}
  



