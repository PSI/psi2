/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file transition.h
* \brief Header of transition.c
* \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/

#ifndef _H_transition
#define _H_transition

/****************************************************************/
/*********  SIGNATURE DES FONCTIONS LOCALES  *********************/
/****************************************************************/

/* fonction arrivee exterieur sur une file destination  **************/
int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par);

/***  fonction sortie exterieure depuios une file origine ***********/
int sortie_ext(int etat[],int param1);

/***  fonction routage n files avec rejet ***********/
int *  routage_nfile_rejet (int etat[],int nb_dest,int * par);

/***  fonction routage n files avec blocage ***********/
int *  routage_nfile_bloc (int etat[],int nb_dest,int * par);

/******* fonction JSQ  arrivee  avec rejet *********/
int *  Arrivee_rejet (int etat[],int nb_dest,int * par, int * numf);

/******* fonction JSQ  routage avec rejet *********/
int *  JSQ_rejet (int etat[],int nb_dest,int * par);


/******* fonction multi serveur index avec rejet *********/
int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf, int ** tab);
/******* fonction multi serveur index avec rejet *********/

int * Independent_Pull(int etat[], int num_ori, int * rand_par);

int * Independent_Push(int etat[], int num_ori, int * rand_par);

int * Arrival_Push(int etat[], int num_ori, int * rand_par);


/**** LTM model implementation (see Marise Beguin PhD Thesis) ****/

int * LTM_arrival(int etat[], int num_ori, int* parametre);

int * LTM_departure(int etat[], int num_ori, int* parametre);



/********************************************************************************/
/*******************          NON MONOTONE EVENTS             *******************/
/********************************************************************************/

/******* function negative customer with routing *********/
int  negative_customer(int etatInf[], int etatSup[], int *par);

/******* auxiliary functions for batch arrival *********/
int inf_batch(int x, int y, int C, int k);
int sup_batch(int x, int y, int C, int k);

/******* function batch *********/
int batch(int etatInf[], int etatSup[], int *par, int size);

/******* function decomposable batch (monotonic) *********/
int dec_batch(int etat[], int *par, int size);

/******* function batch with index routing *********/
int batch_index_routing(int etatInf[], int etatSup[], int nb_dest,int * par, int * numf, int size);


/**** non monotone load sharing systems *************/

int * TF_Departure_Pull(int etat[], int num_ori, int * rand_par);

int ENV_Departure_Pull(int etatInf[], int etatSup[], int num_ori, int * rand_par);

int * EmptyQueue_Pull(int etat[], int num_ori, int * rand_par);

/**** non monotone TQN with 2 pahse coxian service ********/

int * TQN_phase1_service(int etat[], int par[]);

int * TF_TQN_phase1_service_skip_phase2(int etat[], int par[]);

int * ENV_TQN_phase1_service_skip_phase2(int etatInf[], int etatSup[], int par[]);

int * TF_TQN_phase2_service(int etat[], int par[]);

int * ENV_TQN_phase2_service(int etatInf[], int etatSup[], int par[]);

/**************************************************************/
/*****  SIGNATURE DES FONCTIONS EXPORTABLES  ******************/
/**************************************************************/

/***  fonction de transition  interface            *****/
extern void transition_interface(int etat[], int evenement, int* parametre);

/***  fonction de transition  interface with envelopes            *****/
extern void transition_interface_envelope(int etatInf[], int etatSup[], int evenement, int* parametre);

#endif             /* _H_transition ***/
