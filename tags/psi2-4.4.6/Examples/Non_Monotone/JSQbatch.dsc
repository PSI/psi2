#Number of files
3
#Queue capacity
10 10 10
#queue_minimal_initial_state
0 0 0
#queue_maximal_initial_state
10 10 10
#Number_of_events
5
#Index file
File: JSQbatch.idx
#Load Sharing file - N for No Load Sharing file
File: N
#table_of_events
#evt_id-evt_typ-rate-(batch_size)-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0       123      5.0      3	4      -1      :	(0,1)	(1,2)	(2,3)
1       123      5.0      2	4      -1      :	(0,1)	(1,2)	(2,3)
2       1       2.0      	2      0       :       -1
3       1       2.0      	2      1       :       -1
4       1       2.0      	2      2       :       -1
