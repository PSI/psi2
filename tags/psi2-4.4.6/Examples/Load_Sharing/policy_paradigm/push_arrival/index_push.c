#include <stdio.h>
#include <stdlib.h>

#define MIN_VAL -1.0
#define MAX_VAL 100.0
#define MAX_MAX_VAL 1000.0
#define THR 1

double ws_index(int num_ori, int num_cible, int dist, int ch_cible, int * rand_permut){
	if(num_ori == num_cible){
		if(ch_cible > THR)
			return MAX_VAL;
		else
			return MIN_VAL;
		
	}
	else{ //num_ori != num_cible
		if(ch_cible >= THR)
			return MAX_MAX_VAL;
		else
			return (double)rand_permut[num_cible];
	}
}
