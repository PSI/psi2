#Number of files
8
#Queue capacity
20 20 20 20 20 20 20 20
#queue_minimal_initial_state
0 0 0 0 0 0 0 0
#queue_maximal_initial_state
20 20 20 20 20 20 20 20
#Number_of_events
24
#Index file - N for No index file
File: N
#Load Sharing file - N for No Load Sharing file
File: pull.ls
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0 2 0.8 3 -1 : 0 -1
1 2 0.8 3 -1 : 1 -1
2 2 0.8 3 -1 : 2 -1
3 2 0.8 3 -1 : 3 -1
4 2 0.8 3 -1 : 4 -1
5 2 0.8 3 -1 : 5 -1
6 2 0.8 3 -1 : 6 -1
7 2 0.8 3 -1 : 7 -1
8 1 1.0 2 0 : -1
9 1 1.0 2 1 : -1
10 1 1.0 2 2 : -1
11 1 1.0 2 3 : -1
12 1 1.0 2 4 : -1
13 1 1.0 2 5 : -1
14 1 1.0 2 6 : -1
15 1 1.0 2 7 : -1
16 50 1.0 1 0
17 50 1.0 1 1
18 50 1.0 1 2
19 50 1.0 1 3
20 50 1.0 1 4
21 50 1.0 1 5
22 50 1.0 1 6
23 50 1.0 1 7
