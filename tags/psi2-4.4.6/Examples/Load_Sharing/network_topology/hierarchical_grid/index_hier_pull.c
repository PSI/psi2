#include <stdio.h>
#include <stdlib.h>


#define MIN_VAL -1.0
#define MIN_MIN_VAL -2.0
#define MAX_VAL 10000.0


/* Overload/Under-load Threshold for each hierarchy level*/
#define THR_1 1
#define THR_2 3
#define THR_3 5

/*pre-defined index values for each hierarchy level*/
#define IDX_1 3.0
#define IDX_2 2.0
#define IDX_3 1.0


double ws_index(int num_ori, int num_cible, int dist, int ch_cible, int * priority){
	if(num_ori == num_cible){
		if(ch_cible < THR_1)
			return MIN_VAL;
		else
			return MAX_VAL;
		
	}
	else{ //num_ori != num_cible
		if(dist == 2 && ch_cible > THR_1)
			return IDX_1 + (double)num_cible/get_nb_file();
		
		else if(dist == 4 && ch_cible > THR_2)
			return IDX_2 + (double)num_cible/get_nb_file();
		
		else if(dist = 6 && ch_cible > THR_3)
			return IDX_3 + (double)num_cible/get_nb_file();

		else
			return MIN_MIN_VAL;
	}
}
