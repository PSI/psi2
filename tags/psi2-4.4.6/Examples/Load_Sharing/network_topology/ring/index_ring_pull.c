#include <stdio.h>
#include <stdlib.h>

#define MIN_VAL -1.0
#define MIN_MIN_VAL -2.0
#define MAX_VAL 10000.0
#define THR 1

double ws_index(int num_ori, int num_cible, int dist, int ch_cible, int * rand_permut){
	if(num_ori == num_cible){
		if(ch_cible < THR)
			return MIN_VAL;
		else
			return MAX_VAL;
		
	}
	else{ //num_ori != num_cible
		if(ch_cible <= THR || dist > 2)
			return MIN_MIN_VAL;
		else
			return (double)ch_cible + (double)num_cible/get_nb_file();
	}
}
