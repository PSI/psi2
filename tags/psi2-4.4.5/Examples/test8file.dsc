#Number of files
8
#Queue capacity
100 100 100 100 100 100 100 100
#queue_minimal_initial_state
0 0 0 0 0 0 0 0
#queue_maximal_initial_state
100 100 100 100 100 100 100 100
#Number_of_events
16
#Index file - N for No index file
File: N
#table_of_events
#evt_id-evt_typ-rate-nb_queue_evt-origin-desti1-desti2-desti3-dest4
0	2  1.0 3 -1 : 0 -1	
1	2  1.0 3 -1 : 1 -1	
2	2  1.0 3 -1 : 2 -1	
3	2  1.0 3 -1 : 3 -1	
4	6  0.8 3 0  : 4 -1	
5	6  0.6 3 0  : 5 -1	
6	6  0.8 3 1  : 4 -1	
7	6  0.6 3 1  : 5 -1
8	6  0.8 3 2  : 6 -1
9	6  0.6 3 2  : 7 -1
10	6  0.8 3 3  : 6 -1	
11	6  0.6 3 3  : 7 -1	
12	1  2.0 2 4  : -1	
13	1  2.0 2 5  : -1	
14	1  2.0 2 6  : -1	
15	1  2.0 2 7  : -1	
