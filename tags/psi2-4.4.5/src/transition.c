/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "data.c" /* force le module data �etre inline [MQ] */


/*! \file transition.c
 * \brief Functions of transition.c . 
 * \author  Bernard.Tanzi@imag.fr
 * \author  Jean-Marc.Vincent@imag.fr 
 * \author  Jerome.Vienne@imag.fr
 * \date 2004-2006
 */

/*
*------------------------------------------------------------------
* Fichier      : transition.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme secondaire contenant les fonctions de transitions elementaires 
*   ainsi que la fonction de transition interface avec les fonction elementaires
* fichier pouvant ulterieurement etre eclate  en 2 fichiers 
* une bibliotheque de fonctions de transition et la fonction globale interface elle-meme
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "transition.h"

#include "data.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/



/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/**********************************************************/
/********* DEFINITIONS DES VARIABLES EXPORTEES **************/
/**********************************************************/
extern double **table_index;
extern struct st_evt *evt;
extern struct st_multi *multis;

/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : arrivee_ext_deb_rejet
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* nb_dest   int       destination Queue number associate with event
*
* par       int *     pointer on the destination queue number table
*
*
* Called function : get_capacite
*
* Description  :
*
*      function exterior arrived on a queue
*	   starting from the destination queue number vector state 
*      and with destination queue number table
*      return vector state updated
*      
*----------------------------------------------------------------------
*/

int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k=1;
  //printf("Nombre de destinataire %d", nb_dest);
  while ((etat[*(par+k)] == capacite[*(par+k)]) && k<(nb_dest-2))
    /** parcours des files destinations
	tant que la file destination est pleine on passe a la suivante */
    k++;
  /* on verifie si la derniere file est pleine  */
  if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
    /** toutes les files destinations sont pleines on ne fait rien */
    ;
  else
    /** sinon on ajoute dans la file destination */
    etat[*(par+k)]=etat[*(par+k)]+1;     
  /** on renvoie l'adresse du vecteur d'etat */
  return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Function     : sortie_ext
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* numFile   int       queue number
*
* Description  :
*
*      function exit to a queue from exterior
*	   starting from the destination queue number vector state 
*	   return an integer giving the queue client remaining in the queue 
*      
*----------------------------------------------------------------------
*/

int sortie_ext (int etat[],int numFile){ 
   if (etat[numFile] > 0 ) 
     etat[numFile]=etat[numFile]-1;
   return etat[numFile];
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_rejet
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* nb_dest   int      destination+origin queue number associate with event
*
* par       int *     pointer on the origin, destination number table
*
* Called function : get_capacite,get_nb_file
*
* Description  :
*  
*      routage avec debordement sur ne files  avec rejet
*      fonction de routage d'une file origine vers n files destination 
*  au cas ou toutes les files destinations sont pleines, le paquet emis est perdu 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  
  if (etat[*par] > 0){
      k=1;
      etat[*par]=(etat[*par]-1);
      while (k<(nb_dest-1) && etat[*(par+k)] == capacite[*(par+k)]) 
	k++;
      /* on verifie si la derniere file est pleine  */
      if( k == (nb_dest-1) || etat[*(par+k)] == capacite[*(par+k)])
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
      else
		etat[*(par+k)]=etat[*(par+k)]+1;     
    }
   return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_bloc
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adresse de debut du vecteur d'etat
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*      routage avec debordemetn avec rejet
*      fonction de routage d'une file origine vers une premiere file destination et si elle
* est pleine vers une deuxieme file destination 
*  au cas ou la deuxieme file destination  est pleine le paquet emis est perdu
*      a partir du vecteur d'etat d'un numero de file origine et d'un
*     numero de 1ere file destiantaire d'un numero de deuxieme file destinataire 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_bloc (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  

  if (etat[*par] > 0)
    {
      k=1; 
      while (etat[*(par+k)] == capacite[*(par+k)] && k<(nb_dest-2))
	k++;
      if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
	else
	  {
	    etat[*par]=(etat[*par]-1);
	    etat[*(par+k)]=etat[*(par+k)]+1; 
	  }    
    }
  return (&etat[0]);
  }


/**
*----------------------------------------------------------------
*
* Fonction     : JSQ_rejet
*
* Resultat     : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  JSQ_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int num_file_min;
  int k=1;
  
  /* file origine != -1*/
  if(*par != -1){
  	
        if( etat[*par] == 0){
		return (&etat[0]);
	} else {	
		etat[*par]-=1;
		// On cherche la premiere file non pleine et on verifie que l'on ne depasse pas nbdest
    		while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    		}

            	// num_file_min prend la valeur de la 1ere file non pleine trouv
            	num_file_min=*(par+k);
		k++;
		while(k<(nb_dest-1)){		
                    if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
			num_file_min=*(par+k);
		    k++;
		} ;
		if(etat[num_file_min] < capacite[num_file_min]){
			etat[num_file_min]+=1;
		}
	}	return (&etat[0]);
  } else {
  	/* File d'origine == -1 */
	while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    	}

        // num_file_min prend la valeur de la 1ere file non pleine trouv
        num_file_min=*(par+k);
	k++;
	while(k<(nb_dest-1)){		
        if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
		num_file_min=*(par+k);
		k++;
	} ;
	if(etat[num_file_min] < capacite[num_file_min]){
		etat[num_file_min]+=1;
	}
	return (&etat[0]);
  }
}



/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_index(int etat[],int nb_dest,int * par, int * numf){ 
  
  int *  capacite = get_capacite();
  int num_file_min, j;
  int k=1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  
   // We look at the first not empty queue in the list of destination 
   while (k<nb_dest-1 && *(par+k)!=-1 &&!(etat[*(par+k)] < capacite[*(par+k)]) ){
		k++;
   }
   
   // num_file_min has the value of the first not empty queue
   num_file_min=*(par+k);
   j=k;	
   while(k<nb_dest){
				
	if (  *(par+k)!=-1 && (table_index[*(numf+k)][etat[*(par+k)]] < table_index[*(numf+j)][etat[num_file_min]]) && (etat[*(par+k)] < capacite[*(par+k)]) ){
		num_file_min=*(par+k);
		j=k;
	}
	if( *(par+k)== -1 && (table_index[*(numf+k)][0] < ( (table_index[*(numf+j)][etat[num_file_min]]) +1)  ) ){
		num_file_min = -1;
	}
	k++;
   };
   
   //Check if the queue is really not empty
   if( (num_file_min != -1) && etat[num_file_min] < capacite[num_file_min] ){
	etat[num_file_min]=etat[num_file_min]+1;
   }
   
   return (&etat[0]); 
}

/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_multi_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf, int ** tab){ 
  
  int *  capacite = get_capacite();
  int i, j;
  int Setat=0, Scapa=0;
  int nb_file;
  double val_min=100000000;
  int file_min=-1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  //printf("\n\n\n");
  
  for(i=1; i<nb_dest;i++){
	Setat=0;
	Scapa=0;
	nb_file = tab[i][0];
	//printf("Il y a %d file sur le serveur %d\n", nb_file, i);
	
	//si on n'est pas avec la file -1
	if(nb_file != -1){
		for (j=1; j<(nb_file+1); j++){
		    //printf("File: %d en %d, %d\n", tab[i][j], i, j);
			// on accumule le nb de client
			//printf("On ajoute %d a Setat (file %d)\n", etat[tab[i][j]], tab[i][j]);
			Setat += etat[tab[i][j]]; 
			// on accumule le nb de capa
			//printf("On ajoute %d a Scapa (file %d)\n", capacite[tab[i][j]], tab[i][j]);
			Scapa += capacite[tab[i][j]];
		}
		
		if(Setat<Scapa){
			// compare valeur min
			// Si plus petit, on change et on garde
			//printf("On a file = %d Se<Sc, valeur d'index %lf\n", *(numf+i), table_index[*(numf+i)][Setat]); 
			if(table_index[*(numf+i)][Setat]<val_min){
				val_min = table_index[*(numf+i)][Setat];
				//printf("On a file_min = %d\n", *(numf+i));
				file_min = i;
			}
		}
	} else {
		//printf("On est en -1 et on a numf+i = %d\n", *(numf+i));
		if(table_index[*(numf+i)][0]<val_min)
			file_min = -1;
	}
	
  }
  
  //printf("On a file_min: %d \n", file_min);
  
  // on a trouv le multi-serveur  index min
  // Maintenant, on va ajout le client
  if( file_min != -1){
	nb_file = tab[file_min][0];
	
	for (j=1; j<(nb_file+1); j++){
		if (etat[tab[file_min][j]] < capacite[tab[file_min][j]]){
			//printf("On va incrmenter la file %d\n", tab[file_min][j]);
			etat[tab[file_min][j]] +=1;
			return (&etat[0]); 
		}
	}
  }else {
	return (&etat[0]); 
  }
  
  printf("Pb ds multi_serveur index");
  return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_multi_serveur
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_multi_serveur (int etat[],int nb_dest,int * par){ 

int *  capacite = get_capacite();
int i=2;
// If there are 1 client or more in the "cache" queue, we extrat one of them 
if (etat[*par]!=0 && etat[*par+1]!=0 ){
    etat[*par]-=1;
// else we check if there is a client in the queue
}else{
    // If It's the case, we extract the client from the queue
    if (etat[*par]==0 && etat[*(par+1)] !=0){
        etat[*(par+1)]-=1;
    } else { 
		if (etat[*par]!=0 && etat[*(par+1)] ==0){
			 etat[*par]-=1;
			   etat[*(par+1)]+=1;
			return (&etat[0]);
		}else{
			return (&etat[0]);
		}
	}
}
// On parcours la liste des distantion, on fait +1 à la 1ere destination non pleine


// if the destination queue is not -1 & i < nb_dest
while(i< nb_dest && *(par+i)!=-1){
	if(etat[*(par+i)] < capacite[*(par+i)]){
		etat[*(par+i)]+=1;
		i=nb_dest;
	}
	i++;
}
// if all queues are full, there is rejection of client
return (&etat[0]);

}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_call_center
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* nb_ori	int		number of origin queue
* 
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_call_center (int etat[],int nb_dest,int * par, int nb_ori){ 

	int i=0;
	int *  capacite = get_capacite();

	
	
	//On cherche la premiere file non vide
	while (i < nb_ori && etat[*(par+i)]==0){
		i++;
	} 
	
	
	//Test au cas si on est sur une file origine et non -1 (i =nb_ori)
	if( *(par+i)!=-1 ){
			//test pour eviter erreur dut a x: (alors qu'il faut x : sinon nb_ori =0) pour eviter de vider une file vide
	    if (capacite[*(par+i)]<=0) {
			printf("Problem in description file !\n");
		} else {
			//printf("On vide %d de capacite %d \n", *(par+i), etat[*(par+i)]);
			etat[*(par+i)]-=1;
		}
	}
		
	return (&etat[0]);
	

}


/**
*----------------------------------------------------------------
*
* Function     : transition_interface
*
* Result     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of vector etat
*
* event		int		  event
*
* fonctions appelees : get_type_evt,arrivee_ext,sortie_ext,routage_rejet,routage_blocage,routage_debord_bloc,routage_debord_rejet
*
* Description  :
* 
*      en fonction des parametres route vers la fonction de transition adhoc
*      et met a jour le vecteur d'etat pour les files correspondantes
*     
*      
*----------------------------------------------------------------------
*/

void transition_interface(etat,evenement)
     int etat[];
     int evenement;
{
   int **tab;
   int nbdest; 
   static int *par = NULL;
   static int *numf = NULL;
   int type_evt;
   int nb_ori; 
   type_evt=get_type_evt(evenement);
   nbdest=get_nbr_file_evt(evenement);
   int m;
   
   par=malloc(nbdest*sizeof(int));
   
   for(m=0;m<nbdest;m++){
      *(par+m)= get_param(evenement,m);
   }
   
   
   //printf(" Evt: %d \n", evenement);
   switch(type_evt){   
	   case 1: /* sortie externe */
		   etat[*par] = sortie_ext(etat,*par); 
  		   break;

	   case 2: /* arrivee externe */
   		   /* caracteristique 1er parametre numero de file = -1 dernier parametre numero de file = -1 */
      		   arrivee_ext_deb_rejet(etat,nbdest,par); 
   		   break;

	   case 3: /* When a client leave a Multi-server network */
      		   Depart_multi_serveur(etat, nbdest, par);
	   	   break;

	   case 4: /* Join the shortest queue arrivee avec rejet */
		   JSQ_rejet(etat,nbdest,par);
 	   	   break;
  
	   case 5: /* Index avec rejet */
		   numf=malloc(nbdest*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   Arrivee_rejet_index(etat, nbdest, par, numf);
		   free(numf);
 	   	   break;
  
	   case 6: /* routage vers n files avec debordement avec rejet */
      		   /* caracteristique 1er parametre numero de file != avant dernier param numero de file  et dernier parametre numero de file == -1 */
      		   //printf("ROUTAGE nbdest : %d *par %d \n",nbdest,*par);
      		   routage_nfile_rejet(etat,nbdest,par);
 	   	   break;
  
	   case 7: /* routage vers n files avec debordement avec blocage */
      		   /* caracteristique 1er parametre numero de file == avant dernier param numero de file et dernier parametre numero de file == -1 */
      		   routage_nfile_bloc(etat,nbdest,par);
 	   	   break;
  
	   case 8: /* Arrive dans un multi serveur avec index */
		   numf=malloc(nbdest*sizeof(int));
		   for(m=0;m<nbdest;m++){
			   *(numf+m)= get_numf(evenement,m);
		   }
		   
		   tab = get_multi_server(evenement);	
		   Arrivee_rejet_multi_index(etat, nbdest, par, numf, tab);
		   free(numf);
 	   	   break;
  
	   case 9: /* depart call center */
		   nb_ori= get_nb_ori(evenement);
	  	   Depart_call_center(etat, nbdest, par, nb_ori);
 	   	   break;
  
	   default : /*On a reconnu aucun type */
		   printf("erreur de donnee dans le fichier d'import veuillez controler ce fichier \n"); 
		   exit(0);
   }
   free(par);
  
   //printf("Nouvel etat =: %d \n",etat[0]); 
}




 
