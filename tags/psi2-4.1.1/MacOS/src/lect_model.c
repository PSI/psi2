 /* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file lect_model.c
 * \brief Functions of lect_model.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 *
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : lect_model.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme permettant la lecture des donn�s d utilsateurs 
* ainsi que la creation dynamique a partir des donnees lues des 
* structures de donnees qui seront utiliser par l'application 
*  
*-------------------------------------------------------------------------------
*/

/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>

/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "lect_model.h"
#include "data.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/
/* #define DEBUG 1 */

/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/


/********************************************************/
/** DECLARATION VARIABLES GLOBALES A L'APPLICATION  *****/
/********************************************************/

/* donn�s lues du fichier utilisateur */
 
/**  pointer on the maximal length on a trajectory */
double *lg_traj_max; 

/** pointer on the table of trajectories */ 
int *trajectoire;   
 
/** pointer on the number of sample */   
int *nbr_echantillon; 

/** pointer on the number of antithetic variable */ 
int *nbr_VA;

/** pointer on seed  */   
int *germe_gener; 
 

/** pointer on file number */
int *nbr_file;   

/**** tableaux dynamiques de longueur le nombre de files ****/

/** pointer on queue capacity */
int *capfile;  
/** pointer on etat_init_sup */
int *etat_init_supU;

/** pointeur on etat_init_inf */ 
int *etat_init_infU;


/** pointer etat inf */ 
int *etat_inf;
 
/** pointer etat sup */     
int *etat_sup;  

/** pointer on token number of a queue */    
int *nb_jeton; 
/** pointer coupling of a queue  */     
int **couplage;
      
/** pointer on event number */
int *nb_evt;   

int dym_on;


/*** tableaux dynamiques de longueur le nombre d'evnements ***/

/** pointer on events rate*/
double *P;
/** pointer on thresholds */     
double *R;
/** pointer on alternative events */      
int *A;       

/** pointer on structure event */
struct st_evt *evt;  

/** Pointer on the list of servers for multi servers*/
struct st_multi *multis;

/** Pointer on the table which contain index function values .*/
double **table_index;   

void *lib_handle ;
int (*ptrfunc)(int * etat_inf, int * etat_sup);
int (*ptraff)(int * etat); 
 
void purger(void){
    int c;

    while ((c = getchar()) != '\n' && c != EOF)
    {}
}

void clean (char *chaine){
    char *p = strchr(chaine, '\n');


    if (p)
    {
        *p = 0;
    }

    else
   {
        purger();
    }
} 
 
 
 
 
/**
*----------------------------------------------------------------
*
* Function     : lire_data_FU
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* nomf  char *    pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -i option, create data structures (with malloc) 
*		and read index file if there is one.
*		return 1 if there is a problem or 0 if all is ok
*
*  
*----------------------------------------------------------------------
*/


int lire_data_FU(char *nomf){

  
  int i, j, k, error;
  int cpt;
  int  nb_file_multi;
  char var[10];   // utilise qd on lit la liste des files
  FILE *fd,*fd2;      /* pointeur sur descripteur du fichier */
  
  int nb_function;
  int *value_number=NULL;
  int value; 
 
  char file_index[100];
  char commande[BUFSIZ];
  

  //On etire toute les lignes qui commence par un #
  sprintf(commande, "grep -v \"#\" %s",nomf );
  	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture de fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
    }
  else{ 
      printf("Reading of the data file : %s\n", nomf);
      /* lecture variable nombre de files ****/  
      nbr_file=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_file);  
          
#ifdef DEBUG  /* pour affichage nombre de files *****/
      printf("nombre de file : %d\n",*nbr_file);
#endif
      
      capfile=(int *)malloc((*nbr_file) * sizeof(int)); 
	  /* alloue �chaque pointeur sur entier une adresse memoire ******************/
      for(j=0;j<*nbr_file;j++){ 
		fscanf(fd,"%d",capfile+j);  /* stocke a cette @ la valeur lue ****/

 #ifdef DEBUG /* pour affichage capacite file ****/
	printf("capacite  file %d : %d\n",j,*(capfile+j));
#endif
      
      }
      /* lecture etat_inf initial et stockage dans tableau allou�dynamiquement ***/      
      etat_init_infU=(int *)malloc((*nbr_file)*sizeof(int));
	  
	for(j=0;j<*nbr_file;j++){ 
		fscanf(fd,"%d",etat_init_infU+j);
#ifdef DEBUG
      //printf("mini file %d  : %d\n",j,etat_init_infU[j]);
#endif
      }
     /* lecture etat_sup initial et stockage dans tableau allou�dynamiquement ***/   
      etat_init_supU=(int *)malloc((*nbr_file)*sizeof(int));
	  
      for(j=0;j<*nbr_file;j++){
		fscanf(fd,"%d",etat_init_supU+j);
#ifdef DEBUG
//      printf("maxi file %d  : %d\n",j,*etat_init_supU[j]);
#endif   
      }
      /* creation dynamique d'un tableau pour stocker les etats inf  **/
	etat_inf=(int *)malloc((*nbr_file)*sizeof(int));
	  
	  
      /* initialisation de ce tableau avec les valeurs initiales ***/
	for(j=0;j<*nbr_file;j++){
		etat_inf[j]=etat_init_infU[j];
      	} 

      /* creation dynamique d'un tableau pour stocker les etats sup  **/
	etat_sup=(int *)malloc((*nbr_file)*sizeof(int));
	  
	  /* initilaisation de ce tableau avec les valeurs initiales ***/
	for(j=0;j<*nbr_file;j++){
			etat_sup[j]=etat_init_supU[j];
	}
      

      /* creation dynamique d'un tableau pour stocker l'etat des files **/
      nb_jeton=(int *)malloc((*nbr_file)*sizeof(int));
     
      /* creation dynamique d'un tableau pour stocker l'etat du couplage des files **/
      couplage=(int**)calloc(*nbr_VA, sizeof(int));
	  for (i=0; i< *nbr_VA; i++){
		couplage[i]=(int *)calloc((*nbr_file),sizeof(int));
      }
	  
      /* lecture du nombre d'evenement **/
      nb_evt=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nb_evt);  
      
	  /////////////////////////////////////////////////////////////////////////////
	  // Lecture du nom du fichier 
	  fscanf(fd,"%s",&var);
	  if(strcmp("File:", var)){
		printf("Error: \"File:\" waited, we have %s\n", var);
	  } else {
	  
		fscanf(fd,"%s", file_index);
	  
		if(!strcmp("N", file_index)){
			printf("There is no index file\n");
		} else {
			printf("Reading of the index file : %s\n", file_index);
	  
			
			sprintf(commande, "grep -v \"#\" %s",file_index );
	
				if ((fd2 = popen(commande,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",nomf);
					perror("Le Pb est : ");
					return(1);
				} else {
					// On recupere le nombre de fonction
					fscanf(fd2,"%d", &nb_function);
					//printf("On a %d fonctions d'index \n", nb_function);		

					value_number=(int *)malloc((nb_function)*sizeof(int));
					// On recupère le nombre de valeur pour chaque file
					for(i=0;i<nb_function; i++){
						fscanf(fd2,"%d\n", (value_number+i));
						//printf("Nombre de valeur: %d\n", *(value_number+i));
					} 
	
					// Creation of the table
					printf("Creation of the index function table......\n");
					table_index = (double **) calloc(nb_function, sizeof(double));
					if (table_index != NULL){
						error=0;
						cpt =0;
				// On rempli la table d'index par l'index des files dest
				for(i=0; i<nb_function; i++){
					table_index[i] = (double *) calloc(*(value_number+i), sizeof(double));
					if (table_index[i] == NULL){
						error = 1;
					}
					cpt++;
				}
				
				if (error){
					while (cpt){
						cpt--;
						free(table_index[cpt]);
					}
					perror("Error not enough memory ");
					return(1);
				}
			} else {
				printf("Not enough memory\n");
			}
			
			// Remplissage du tableau
			
			for(i=0; i<nb_function; i++){
				
				for (j=0; j<*(value_number+i); j++){
					
					fscanf(fd2, "%d", &value);
					
					if(value != j){
						printf("Error: there is a problem on the line where there is the number of the index function, it should be %d\n", j);
					}
					fscanf(fd2, "%lf", &table_index[i][j]);
					//printf("On a mis %lf en (%d, %d)\n",table_index[i][j], i, j);
				}
			}
				
		}
		pclose(fd2);
	  	printf("Reading of the index file %s over\n", file_index);
	}
	}
	  /////////////////////////////////////////////////////////////////////////////
      
      /* creation dynamique d'un tableau pour les probabilites des evenements ***/
      P=(double *)malloc((*nb_evt)*sizeof(double));

      /* creation dynamique d'un tableau pour les seuils ***/
      R=(double *)malloc((*nb_evt)*sizeof(double));

      /* creation dynamique d'un tableau pour les evenements alternatifs ***/
      A=(int *)malloc((*nb_evt)*sizeof(int));

      /* pour importer le tableau  des evenements dans une structure ******/
      evt=(struct st_evt *)malloc((*nb_evt)*sizeof(struct st_evt));
     
	  multis=(struct st_multi *)calloc((*nb_evt), sizeof(struct st_multi));
	   
      for(i=0;i<(*nb_evt);i++){
		
	  
		fscanf(fd,"%d %d %lf %d ",&(evt+i)->id_evt,&(evt+i)->typ_evt,&(evt+i)->lambda,&(evt+i)->nbr_file_evt);
		
		//printf("\n id: %2d       type: %2d    lambda: %f   nbr_file evt:%2d \n",(evt+i)->id_evt,(evt+i)->typ_evt,(evt+i)->lambda,(evt+i)->nbr_file_evt); 
		
		((evt+i)->param_evt)=malloc(((evt+i)->nbr_file_evt)*sizeof(int));
		((evt+i)->num_fct)=malloc(((evt+i)->nbr_file_evt)*sizeof(int));
	    
		
		if ((evt+i)->typ_evt == 5){
			// On est ds le type 5
			fscanf(fd,"%s",&var);
			*((evt+i)->param_evt)=atoi(var);
			
			fscanf(fd,"%s",&var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			for (j=1;j<((evt+i)->nbr_file_evt);j++){
				
				fscanf(fd," (%d,%d)", ((evt+i)->param_evt+j), ((evt+i)->num_fct+j));
				//printf("J'ai lu: %d et %d\n", *((evt+i)->param_evt+j), *((evt+i)->num_fct+j));
				// on vérifie d'on a assez de valeur de fonction d'index pour chaque fonction
				// Capacité de la file doit être < ou = à Nombre de valeur de la fonction d'index associé
				//printf("Capacité de la file: %d\n", *(capfile+*((evt+i)->param_evt+j)));
				//printf("Nombre de valeur de la fonction d'index: %d\n", *(value_number+*((evt+i)->num_fct+j)));

				if( (*((evt+i)->param_evt+j)) != -1 ){
					//printf("On est diff de -1\n");
					if ( *(capfile+*((evt+i)->param_evt+j)) > (*(value_number+*((evt+i)->num_fct+j)) - 1) ){
					
						printf(" Function index %d is not defined for all the value of the queue %d \n", *((evt+i)->num_fct+j), *((evt+i)->param_evt+j));
						printf("There is %d index values for a queue with a capacity of %d\n", *(value_number+*((evt+i)->num_fct+j)) , *(capfile+*((evt+i)->param_evt+j)) );
					}
				} else {
					if( (*(value_number+*((evt+i)->num_fct+j))) < 1 )
						printf("You need at least 1 value for the exit (-1) on index function");
				}	

			}
			
		}
		
		else if ((evt+i)->typ_evt == 8){
			// On est ds le type 8
			
			fscanf(fd,"%s",&var);
			*((evt+i)->param_evt)=atoi(var);
			
			fscanf(fd,"%s",&var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			multis[i].queue_list = (int **) calloc((evt+i)->nbr_file_evt, sizeof(int*));
			
			for (j=1;j<((evt+i)->nbr_file_evt);j++){
				
				fscanf(fd," (%d,", &nb_file_multi);
				//printf("Il y a  %d file \n", nb_file_multi);
				
				if( nb_file_multi != -1){
					multis[i].queue_list[j] = (int *) calloc((nb_file_multi+1), sizeof(int));
					multis[i].queue_list[j][0] = nb_file_multi;
				
					for(k=0; k<nb_file_multi; k++){
						
						fscanf(fd,"%d,", &(multis[i].queue_list[j][k+1]));
						//printf("On a la file %d en %d, %d\n", multis[i].queue_list[j][k+1], j, (k+1) );
				
					}
				
					fscanf(fd,"%d)", ((evt+i)->num_fct+j) );
					//printf("La file d'index est: %d \n",*((evt+i)->num_fct+j) );
				}else{
					multis[i].queue_list[j] = (int *) calloc(1, sizeof(int));
					multis[i].queue_list[j][0] = -1;
					
					//printf("On a la file %d en %d, O\n", multis[i].queue_list[j][0], j );
					
					fscanf(fd,"%d)", ((evt+i)->num_fct+j) );
					//printf("La file d'index est %d \n",*((evt+i)->num_fct+j) );
				}
			
			}
		} 
		else if ((evt+i)->typ_evt == 9){	
			// On est ds le type 9 -> call center ^^
			
			
			for (j=0;j<((evt+i)->nbr_file_evt);j++){
				fscanf(fd,"%s",&var);
				
				if(strcmp(":", var) ){ 
					*((evt+i)->param_evt+j)=atoi(var);
				}else{
					(evt+i)->num_ori=j;
					//printf("Evt: %d Nombre de file origine: %d \n",i, (evt+i)->num_ori );
					// Ici on a rencontre le caractere :
					j--;
				}
			}
		}
		else{
			// On n' est pas dans le type 5 ou 8
			for (j=0;j<((evt+i)->nbr_file_evt);j++){
				fscanf(fd,"%s",&var);
				
				if(strcmp(":", var) ){ 
					*((evt+i)->param_evt+j)=atoi(var);
				}else{
					// Ici on a rencontre le caractere :
					j--;
				}
			}
		}
    
	
	
	}
  }
  pclose(fd);
  printf("Reading of the data file : %s over\n", nomf);
  return(0);
}

/**
*----------------------------------------------------------------
*
* Function     : lire_couplage
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* 
*
* Description  :
*
*	Use to open dynamic library
*
*  
*----------------------------------------------------------------------
*/

int lire_couplage(FILE *fd){

char var[10];   // utilis pour lire le nom du fichier  
char file_coupl[100];
char *chemin3;
char *nfin;  
char *nfout;
char *chficso;
	  
fscanf(fd,"%s",&var);
//fgets(var, 100 , fd);
	  
// On supprime le \n
//	clean(var);
	  
if(strcmp("File:", var)){
	printf("Error: \"File:\" waited, we have %s\n", var);
} else {
	  
	fscanf(fd,"%s", &file_coupl);
	// on recupre le nom du fichier
	//fgets(file_coupl, 100 , fd);
		
	// On supprime le \n
	//clean(file_coupl);	
	 
	if(!strcmp("No", file_coupl)){
		printf("There is no coupling file\n");
		dym_on=0;
	} else {
		dym_on=1;
		printf("Reading of the coupling file : %s\n", file_coupl);
		 /* pour stocker le nom de fichier sans son extension */

		nfin = (char *) malloc(40*sizeof(char));
		nfout = (char *) malloc(40*sizeof(char)); 
		strcpy(nfin,"");
		strcat(nfin,file_coupl);
		strcat(nfin,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfin);
		#endif

		int lgfile = strlen(nfin);

		#ifdef DEBUG
		printf("longueur du fichier %d\n",lgfile);
		#endif

		int i;
		for(i=0;i<(lgfile-1);i++){
			*(nfout+i)=nfin[i];
		}
		strcat(nfout,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfout);
		#endif
	     
		  /* pour cr�r la commande system ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
		chemin3=(char *)malloc(100*sizeof(char));
		strcpy(chemin3,"");
		strcat(chemin3,"gcc -dynamiclib -flat_namespace -undefined suppress ");
		strcat(chemin3,file_coupl);
		strcat(chemin3," -o lib");
		strcat(chemin3,nfout);
		strcat(chemin3,"dylib ");
		

		#ifdef DEBUG
		printf("commande system %s\n",chemin3);

		printf ("C'est le programme main qui s'execute\n") ;

		printf ("Compilation/creation de la bibliotheque \n") ;
		#endif 
	      
		/* pour executer la commande */
		system(chemin3);
  
		#ifdef DEBUG
		printf ("Ouverture de la bibliotheque partagee\n") ;
		#endif

		chficso = (char *)malloc(30*sizeof(char));
		strcat(chficso,"lib");
		strcat(chficso,nfout);
		strcat(chficso,"dylib");
		  
		#ifdef DEBUG
		printf("chemin fichier so %s\n",chficso);
		#endif
	      
		lib_handle = dlopen (chficso, RTLD_LAZY) ;
		if (lib_handle){
			printf("Dynamic library opening: Successful\n");	
		} else {
			printf ("[%s] Unable to open library: %s \n", chficso, dlerror ()) ;
			exit (-1) ;
		}
		  
		#ifdef DEBUG
		printf ("Dynamic library \n") ;
		#endif
		ptrfunc = (void *) dlsym (lib_handle, "egalite_reward") ;
		if (ptrfunc == NULL){
			printf("Loading egalite_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading egalite_reward(): Successful\n");
		}

		ptraff = (void *) dlsym (lib_handle, "affiche_reward") ;
		if (ptraff == NULL){
			printf("Loading affiche_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading affiche_reward(): Successful\n");
		}
		
		#ifdef DEBUG
		printf ("Pointeur sur la fonction r�up��\n") ;
		printf ("Appel de la fonction\n") ;
		#endif
		  
		printf ("Coupling function initialisation : OK\n") ; 
	}
	
}
	return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : lire_param_FU
*
* Result     : int
*
* Parameters   :
*
* Nom       Type      Role
*
* nomf  char *     pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -p option, create data structures (with malloc)
*
*----------------------------------------------------------------
*/

int lire_param_FU(char *nomf){

  FILE *fd;      /* pointeur sur descripteur de fichier */
  char commande[BUFSIZ];
 
  
  
  sprintf(commande, "grep -v \"#\" %s",nomf );
	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture du fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
   } else { 
     
      /* lecture variable nombre echantillon ***/
      nbr_echantillon=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_echantillon);

      /* lecture variable nombre echantillon ***/
      nbr_VA=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_VA);

      /* lecture variable longueur trajectoire maxi ****/
      lg_traj_max=(double *)malloc(sizeof(double));
      fscanf(fd,"%lf",lg_traj_max);	
      
      /* creation en memoire du tableau de trajectoires ***/
      trajectoire=(int*)calloc((*lg_traj_max),sizeof(int));
	  
	  
      /* lecture variable nombre echantillon ***/
      germe_gener=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",germe_gener);
	  
	  lire_couplage(fd);
  }
  pclose(fd);
  return(0);
}

	  


