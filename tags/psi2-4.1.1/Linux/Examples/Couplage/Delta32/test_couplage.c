/* Copyright(C) (2007) (ID - IMAG) <jerome.vienne@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \descfp test_couplage.c
 * \brief Fonctions de test_couplage.c . 
 * \author - jerome.vienne@imag.fr
 *
 * \date Janvier 18 , 2007
 */



/*
*------------------------------------------------------------------
* Fichier      : test_couplage.c
* Langage      : C ANSI
* Auteur       : 
* Creation     : 
*--------------------------------------------------------------------
* Description  :
*          Programme secondaire comprenant la bibliotheque des fonctions de couplage
*
*-------------------------------------------------------------------------------
*/

#include <stdlib.h>
#include <stdio.h>




int egalite_reward(int * etat_inf, int * etat_sup){ 

    return ((etat_inf[24]) == (etat_sup[24]) &&
	(etat_inf[25]) == (etat_sup[25]) &&
	(etat_inf[26]) == (etat_sup[26]) &&
	(etat_inf[27]) == (etat_sup[27]) &&
	(etat_inf[28]) == (etat_sup[28]) &&
	(etat_inf[29]) == (etat_sup[29]) &&
	(etat_inf[30]) == (etat_sup[30]) &&
	(etat_inf[31]) == (etat_sup[31])
);
}



void affiche_reward(FILE * file, int * etat){

	fprintf(file, "%d %d %d %d %d %d %d %d", etat[24], etat[25],etat[26],etat[27],etat[28],etat[29],etat[30],etat[31]);

}


