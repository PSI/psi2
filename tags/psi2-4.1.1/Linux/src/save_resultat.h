/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */




/*! \file save_resultat.h
* \brief Header of save_resultat.c
* \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
*/

#ifndef _H_save_resultat
#define _H_save_resultat

/***********************************************************/
/***** SIGNATURE DES FONCTIONS LOCALES *********************/
/***********************************************************/

/***********************************************************/
/******** SIGNATURE DES FONCTIONS EXPORTEES ****************/
/***********************************************************/

/********* sauve presentation resultat simulation *********/
extern int sauve_present_RS(char *nomd,char *nomp, char *nomf);

/*********** sauve etat resultat siluation ****************/
extern void sauve_etat(int *etat1);

#endif  /** _H_save_resultat ***/ 
