/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file modele.c
 * \brief Functions of modele.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : modele.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme coeur de l'application effectuant les calculs de simulation
*
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "modele.h"
#include "data.h"
#include "transition.h"
#include "lect_model.h"
#include "save_resultat.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/

#define EPS 1e-12      

/* #define DEBUG 1 */
/*#define AFFECR  */ /* pour affichage a l'ecran de ce qu'on ecrit dans le ficheir resultat */

/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

int rad;  /* variable initialisant le generateur aleatoire ***/


/***********************************************************/
/******** DECLARATIONS DES VARIABLES EXTERNES **************/
/***********************************************************/

extern FILE *descfp;
extern int (*ptrfunc)(int * etat_inf, int * etat_sup);
extern int (*ptraff)(FILE *file, int * etat);
extern int nbparamlc;
extern int *germe_gener;
extern int num_file_coup_deb;
extern int num_file_coup_fin;   
extern double **table_index;


/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/
/**
*----------------------------------------------------------------
*
* Function     : construction
*
* Result     : void
*
* Parameters   : anything 
*
*
* Called functions : nb_evenements,get_taux,set_seuil,set_evt_alt
*
* Description  :
*
*      structure construction of generation based on Walker algo 
*	   all that in statics because it is pretraitement
*      
*----------------------------------------------------------------------
*/
void construction() /* construction de la structure de generation base sur algo de Walker
		       tout cela en statique car c'est le pretraitement */
{
  int NB_EVTS=nb_evenements();
  
  int   j,k;                /* indice j pointe sur le premier Low et k sur le premier High */
  int   L[NB_EVTS];   /* Liste des objets a traiter (indice suivant dans la liste L et H)*/
  int   j_boucle,l_auxiliaire;
  double R[NB_EVTS];

  j=NB_EVTS;          /* le test L vide se fait par (j==nb_evenements) */
  k=NB_EVTS;          /* le test H vide se fait par (k==nb_evenements) */
  for (j_boucle = 0; j_boucle< NB_EVTS; j_boucle++)
    { 
      R[j_boucle]=get_taux(j_boucle)*NB_EVTS;
      set_seuil(j_boucle,R[j_boucle]);
      /* on initialise les seuils a partir de P */
      if (R[j_boucle] < 1)
	/* l'indice j_boucle est place dans la liste L le premier element est pointe par j*/
	{
	  L[j_boucle]=j;
	  j = j_boucle;
       }        
      else /* l'indice i_boucle est place dans la liste H le premier
	      element est pointe par k */         
	{
	  if (R[j_boucle] > 1)
	    {
	      L[j_boucle]=k;
	      k = j_boucle;
	    }
	}
    }

  while (j != NB_EVTS)
    {
      set_evt_alt(j,k);
      
      R[k]=R[k]+R[j]-1;
      if (fabs(R[k]-1) < EPS)
	{         /* j et k sont bons et on repart avec les suivants */
	  set_seuil(k,1);
	  k=L[k];
	  j=L[j];
	}
      else
	{
	  if (R[k] < 1)
	    { /* on insere k dans la liste L (ici on le place en tete )*/
	      set_seuil(k,R[k]);
	      l_auxiliaire=L[k];
	      L[k]=L[j];
	      j=k;
	      k=l_auxiliaire;
	    }
	  else     /*** R[k] >= 1 *****/
	    { /* on supprime l'element j des listes L et H */
	      j = L[j];
	    }
	}
    }
}/* fin de la generation de la matrice des seuils */

/**
*----------------------------------------------------------------
*
* Function     : u01
*
* Result       : double
*
* Parameters   : anything
*
* Description  :
*
*      uniform law on 0,1
*
*      optimisation pour eviter l'operateur de division qui est couteux 
*      
*----------------------------------------------------------------------
*/

double u01() 
{
 return( (double) random() * .4656612875e-9);
}

/**
*----------------------------------------------------------------
*
* Function     : init_generateur
*
* Result       : void
*
* Parameters   : anything
*
* Description  :
*
*      Initialisation du generateur se fait soit manuellement pour pouvoir regenerer exactement 
* la meme sequence a partir du meme nombre soit aleatoirment a partir de la fonction getpid()
*      
*----------------------------------------------------------------------
*/
void init_generateur() 
{
  
  if( germe_gener != NULL)
    {
      rad=*germe_gener;
      srandom(rad);
    }
  else 
    {
      rad=getpid();
      printf("valeur initialisant le generateur aleatoire %d\n",rad);
      srandom(rad);
    }
}

/**
*----------------------------------------------------------------
*
* Function     : genere_evenement
*
* Result      : int
*
* Parameters   : anything
*
* Called functions : nb_evnements,get_seuil,get_evt_alt
*
* Description  :
*
*       function which generate an event
*
*      optimisation pour eviter l'operateur de division qui est couteux 
*      
*----------------------------------------------------------------------
*/

int genere_evenement(double u)
{
  int evenement;
  double x;
  x = nb_evenements()*u;
  evenement =(int) x;  
  x = x - evenement;
  if (x <= get_seuil(evenement))
    {
      return evenement;  /**** retourne evenement *****/
    }
  else
    {
      return get_evt_alt(evenement); /* retourne alias de l'evenement si au dessus du seuil */
    }
}



/***********************************************************************/
/**** IMPLANTATION DES FONCTIONS EXPORTABLES   **************************/
/***********************************************************************/


/**
*----------------------------------------------------------------
*
* Function     : test_tot_couplage  
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat1     int *     pointer on etats1 table
*
* etat2     int *     pointer on etats2 table
*
* Called function : get_nb_file
*
* Description  :
*
*     pour tester le couplage  entre 2 vecteurs d'un ensemble de file 
* compris entre le numero i et le numero j 
* renvoie 0 si au moins une file n'a pas encore couple
* et une valeur differente de zero si l'ensemble des files ont couples
*
*----------------------------------------------------------------------
*/

int test_couplage(num){
  int i=0;
  int j=(get_nb_file()-1);
  int i_boucle,val_test;
  
  val_test=1;
 
   
	for (i_boucle = i; i_boucle<= j; i_boucle++){
		val_test=val_test && (get_couplage(num, i_boucle));
	}
   
  
  return val_test;
}

/**
*----------------------------------------------------------------
*
* Function     : affichage 
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* descfp     
*
*     
*
*----------------------------------------------------------------------
*/

void affichage(int detail, int echantillon, int NB_FILE, int nb_ant, int ** etat_min_saved, int ** etat_max_saved, int * log2temps_arret){

	int num, i;

	fprintf(descfp,"%6d    ",echantillon);
	//printf("On écrit %d\n", echantillon);
	if(!detail){
		fprintf(descfp," [ ");
		for (num=0; num<nb_ant; num++){
			fprintf(descfp," [");
			sauve_etat(etat_min_saved[num]);
			fprintf(descfp,"] ");
		}
		
		fprintf(descfp," ] ");
		fprintf(descfp,"\n");

	} else {
			
		for (num=0; num<nb_ant; num++){
			fprintf(descfp," %3d ",log2temps_arret[num]);
			for (i=0; i<NB_FILE; i++){
				fprintf(descfp," %3d ", get_couplage(num, i));
			}
			
			fprintf(descfp," [");
			sauve_etat(etat_min_saved[num]);
			fprintf(descfp,"] ");
		
		} 
		fprintf(descfp,"\n");
	}
}

/**
*----------------------------------------------------------------
*
* Function     : affichage 
*
* Result       : void
*
* Parameters   : 
*
* Name       Type      Role
*
* descfp     
*
*     
*
*----------------------------------------------------------------------
*/

void affichage_dynamique(int  ** etat_min_saved, int echantillon, int nb_ant ){

	int num;

	fprintf(descfp,"%6d    ",echantillon);
	//printf("On �rit %d\n", echantillon);
	
	fprintf(descfp," [ ");	
	for (num=0; num<nb_ant; num++){
		
		fprintf(descfp," [ ");
		(*ptraff)(descfp, etat_min_saved[num]);
		fprintf(descfp," ] ");
		
	} 
	fprintf(descfp," ] ");
	fprintf(descfp,"\n");
	
}
/**
*----------------------------------------------------------------
*
* Function     : lancement_single_antithetic
*
* Result       : void
*
* Parameters   : nb_ant : Number of variables use for antithetic process 
*
* Called functions : init_generateur,construction,get_taille_ech,get_lg_traj_max,get_nb_file,
*       set_trajectoire,genere_evenement,init_etat,transition_interface,get_etat_inf,get_trajectoire,get_etat_sup,
*          test_couplage,get_couplage,set_couplage,cout_etat,sauve_etat  
*
* Description  :
*
*       fonction de lancement du module de simulation fait appel aux fonctions 
*     init_generateur
*     construction
*      pour chaque echantillon effectue des iterations dans le passe jusqu a ce qu il y ait couplage
*     ou que le nombre maxi de trajectoire soit atteint
*     affiche le resultat pour chaque echantillon
*          soit il y a eu couplage et dans ce cas affiche l etat de chaque file au moment du couplage
*          soit il n y a pas eu couplage nombre de trajectoire maxi atteint et dans ce cas 
* affiche echec
*
*     affiche a la fin la duree moyenne d un tirage 
*      
*---------------------------------------------------------------------------------
*/ 

void lancement_single(int detail){
  init_generateur();
  construction();
  
  double val;
  int echantillon;           /* parametres de la simulation */
  int temps_arret,indice, indice2, i, num, n, max_temps;
  int fin=0;                   /* teste si on a couplage */
  //int value;
  struct timeval ti,tf; /* structure de mesure de la duree de simulation */

  int NB_ECH = get_taille_ech();
  int nb_ant = get_nb_VA();
  int LG_TRAJ_MAX = get_lg_traj_max();
  int NB_FILE=get_nb_file();
  int * etat_inf=NULL;
  int * etat_sup=NULL;
  int * log2temps_arret;
  int ** etat_min_saved;
  int ** etat_max_saved;
  int ** Tab_VA;
  double *stat;				/* Contient les u gnrs*/
  unsigned int place;
  int temp;  

  printf("\n");
  if(nb_ant == 1){
	printf(" Perfect Simulation started \n");
  } else {
	printf(" Perfect Simulation -= Antithetic with %d variables =- started \n" ,nb_ant);
  }
  
  etat_min_saved = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	etat_min_saved[i]=(int*)calloc(NB_FILE,sizeof(int));
  }

  etat_max_saved = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	etat_max_saved[i]=(int*)calloc(NB_FILE,sizeof(int));
  }
  
    Tab_VA = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	Tab_VA[i]=(int*)calloc(LG_TRAJ_MAX,sizeof(int));
  }
  
  for(num=0; num<nb_ant; num++){
	for (i=0; i<LG_TRAJ_MAX; i++){
		Tab_VA[num][i]=num;
	}
  }

  log2temps_arret=(int*)calloc(nb_ant,sizeof(int));
  
  stat=(double*)calloc(LG_TRAJ_MAX,sizeof(double));
  
 
  gettimeofday(&ti,NULL);
  
  for (echantillon = 0 ; echantillon < NB_ECH ; echantillon++){    
	   for (num=0; num<nb_ant; num++){
			log2temps_arret[num] = 0;  /* initialisation du nombre d'iteration */
		}
		
	  if((echantillon+1)%NB_ECH == 0){
		printf("\r");
		printf("Running %d", NB_ECH / echantillon+1  );
		fflush(stdout);
	  }
	  
	  max_temps = 0;
	  
	  temps_arret = 1;
	 
	  init_couplage(num);
    	  for (num=0; num<nb_ant ; num++){
		
		if (num == 0){
			stat[0] = u01();
			for(n=0; n<nb_ant; n++){
				Tab_VA[n][0]=n;
			}		
		
			for (i=1; i<nb_ant; i++){
				place = ((int) (u01()*(nb_ant-1)))+1;
				temp = Tab_VA[place][0];
				Tab_VA[place][0] = Tab_VA[num][0]; 
				Tab_VA[num][0] = temp;
			}
		}
		
	  // x modulo y == x - ( ( (int)x/(int)y ) * y )
		val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_ant) - ( ( (int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_ant) /1 )*1 ); 
		
		set_trajectoire(0, genere_evenement(val));

		fin=0;
		temps_arret = 1;  
		do{
				
			init_etat(num);
			indice=temps_arret;
		
			/* generer les evenements de l'indice temps_arret a 2*temps_arret */
			/* de indice 2*temps_arret a  temp_arret  on genere de nouvelles trajectoires */
			if(max_temps < temps_arret){
				stat[temps_arret] = u01();
				max_temps = temps_arret;
					
				indice2=max_temps;
				//GENERE VAL
				for(n=0; n<nb_ant; n++){
					Tab_VA[n][indice2]=n;
  				}

				for (i=1; i<nb_ant; i++){
					place = ((int) (u01()*(nb_ant-1)))+1;
					temp = Tab_VA[place][indice2];
					Tab_VA[place][indice2] = Tab_VA[num][0]; 
					Tab_VA[num][indice2] = temp;
				}
					
			}
			
			val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_ant) - (( (int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_ant )/1)*1);

			set_trajectoire(indice, genere_evenement(val));  /* renvoie un evenement ******/
		
			transition_interface(get_etat_inf(),get_trajectoire(indice) ) ; 
				/* appel a fonction de transition qui a un etat_inf  evenement associe un etat_inf*/
			transition_interface(get_etat_sup(),get_trajectoire(indice) ); 
				/* appel a fonction de transition qui a un etat_sup  evenement associe un etat_sup */
				   
	   
				/*  de indice temps_arret-1 a  O on reprend les memes trajectoires *********************/
			for (indice = temps_arret-1; indice>=0; indice--){
				transition_interface(get_etat_inf(),get_trajectoire(indice));
				transition_interface(get_etat_sup(),get_trajectoire(indice));
			} // fin for
				//printf("Fin\n");
			temps_arret=temps_arret+1;
			log2temps_arret[num]+=1;
				
			etat_inf=get_etat_inf();
			etat_sup=get_etat_sup();
			
			if(ptrfunc == NULL){
				for (i=0; i<NB_FILE; i++){
					
					/* on mesure pour chaque file d'attente le temps de couplage   *******/
					
					if ( (get_couplage(num, i) == 0) && (etat_inf[i] == etat_sup[i]) ){
						//printf("Echantillon %d la file %d couple avec un temps %d\n", echantillon, i, log2temps_arret);
						// set_couplage(num, i, log2temps_arret[num]);
						//printf("%d a couple en %d \n\n\n\n", num, log2temps_arret[num]);
						etat_min_saved[num][i]=etat_inf[i];
						etat_max_saved[num][i]=etat_sup[i];
				
					}// fin if
				}// fin du for
				/* on verifie si l'ensemble des files d'attente ont couples    ********/
			
				if(test_couplage(num))
					fin++;
			}else{
				if((*ptrfunc)(etat_inf, etat_sup)){
					fin++;
					etat_min_saved[num]=etat_inf;
					etat_max_saved[num]=etat_sup;
				}
			}
		} while((temps_arret < LG_TRAJ_MAX) && (fin == num));   /**** do while ********/
		
		 
	  } // for num
	  if (fin){
		if(ptraff == NULL)
			affichage(detail, echantillon, NB_FILE, nb_ant, etat_min_saved, etat_max_saved, log2temps_arret);
		else{
			
			affichage_dynamique(etat_min_saved , echantillon, nb_ant);
		}
	   }else {
		fprintf(descfp,"%d echec %d \n",echantillon,log2temps_arret[0]);
	   }
   } // echantillon
   printf("\n");
   gettimeofday(&tf,NULL);

   fprintf(descfp,"# Size %4d  Sampling time  : %f micro-seconds\n",NB_ECH,
   ((double) ((tf.tv_sec-ti.tv_sec)*1000000 + (tf.tv_usec-ti.tv_usec)))/NB_ECH);
   fprintf(descfp,"# Seed Value %d\n",rad);

   fclose(descfp);
	
   free(stat);
   free(log2temps_arret);
   free(etat_min_saved);
   free(etat_max_saved);
   free(Tab_VA );
}


/**
*----------------------------------------------------------------
*
* Function     : lancement
*
* Result       : void
*
* Parametres   : anything
*
* Called functions : init_generateur,construction,get_taille_ech,get_lg_traj_max,get_nb_file,
*       set_trajectoire,genere_evenement,init_etat,transition_interface,get_etat_inf,get_trajectoire,get_etat_sup,
*          test_couplage,get_couplage,set_couplage,cout_etat,sauve_etat  
*
* Description  :
*
*       fonction de lancement du module de simulation fait appel aux fonctions 
*     init_generateur
*     construction
*      pour chaque echantillon effectue des iterations dans le passe jusqu'a ce qu'il y ait couplage
*     ou que le nombre maxi de trajectoire soit atteint
*     affiche le resultat pour chaque echantillon
*          soit il y a eu couplage et dans ce cas affiche l'etat de chaque file au moment du couplage
*          soit il n'y a pas eu couplage nombre de trajectoire maxi atteint et dans ce cas 
* affiche echec
*
*     affiche a la fin la duree moyenne d'un tirage 
*      
*---------------------------------------------------------------------------------
*/
void lancement(int detail){
  
  init_generateur();
  construction();
  
  double val;
  int echantillon;           /* parametres de la simulation */
  int temps_arret,indice, indice2, i,n, num, max_temps;
  int fin=0;                   /* teste si on a couplage */
  struct timeval ti,tf; /* structure de mesure de la duree de simulation */

  int NB_ECH = get_taille_ech();
  int nb_ant = get_nb_VA();
  int LG_TRAJ_MAX = get_lg_traj_max();
  int NB_FILE=get_nb_file();
  int * etat_inf=NULL;
  int * etat_sup=NULL;
  int * log2temps_arret;
  int ** etat_min_saved;
  int ** etat_max_saved;
  int ** Tab_VA;	/* Contient le tableau de translation pour chaque VA */
  double *stat;				/* Contient les u gnrs*/
  unsigned int place;
  int temp;  

  printf("\n");
  if(nb_ant == 1){
	printf("Perfect Simulation (with doubling period) started \n");
   } else {
	printf(" Perfect Simulation (with doubling period)  -= Antithetic with %d variables =- started \n" ,nb_ant);
  }
  

  etat_min_saved = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	etat_min_saved[i]=(int*)calloc(NB_FILE,sizeof(int));
  }

  etat_max_saved = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	etat_max_saved[i]=(int*)calloc(NB_FILE,sizeof(int));
  }
  
  Tab_VA = (int **) calloc(nb_ant, sizeof(int));
  for(i=0; i<nb_ant; i++){
 	Tab_VA[i]=(int*)calloc(LG_TRAJ_MAX,sizeof(int));
  }

  log2temps_arret=(int*)calloc(nb_ant,sizeof(int));
  
  
  
  stat=(double*)calloc(LG_TRAJ_MAX,sizeof(double));
  
  gettimeofday(&ti,NULL);
 
  for (echantillon = 0 ; echantillon < NB_ECH ; echantillon++){
	
	for (num=0; num<nb_ant; num++){
		log2temps_arret[num] = 0;  /* initialisation du nombre d'iteration */
	}	
	
	if((echantillon+1) % 100 == 0){
		printf("\r");
		printf("%d on %d  done", (echantillon+1), NB_ECH);
		fflush(stdout); 
	}

	max_temps = 0;
	temps_arret = 1;
	
	
	init_couplage(nb_ant);
	
	//On genere la variable et le tableau pour la premiere variable
	stat[0] = u01();
	for(n=0; n<nb_ant; n++){
		Tab_VA[n][0]=n;
  	}
	
	//printf("Initialisation des valeurs\n");
	//printf("Tableau en t=0\n");
	//for(n=0; n<nb_ant; n++){
	//	printf(" %d ", Tab_VA[n][0]);
	//}
        
	//printf("\n\n");
	//printf("\n\n Algo de melange des donnees \n\n");

	for (i=1; i<nb_ant; i++){
			place = ((int) (u01()*(nb_ant-1)))+1;
			//printf("On tire l'echange de %d avec %d\n", i, place);
			temp = Tab_VA[place][0];
			
			Tab_VA[place][0] = Tab_VA[i][0];
			
			Tab_VA[i][0] = temp;
			//printf("On a TA_VA[%d][0]=%d\n", place ,Tab_VA[place][0]);
			//printf("On a TA_VA[%d][0]=%d\n", i ,Tab_VA[i][0]);	
	}
	

	//printf("Tableau apres melange\n");
	//for(n=0; n<nb_ant; n++){
	//	printf(" %d ", Tab_VA[n][0]);
	//}        
	//printf("\n\n");

	for (num=0; num<nb_ant ; num++){
		
		
		// x modulo y == x - ( ( (int)x/(int)y ) * y )
		val = (stat[0]+(double)Tab_VA[num][0]/(double)nb_ant) - ( ( (int)(stat[0]+(double)Tab_VA[num][0]/(double)nb_ant) /1 )*1 ); 
		
		//printf("On a val = %lf pour num = %d\n", val, num);

		set_trajectoire(0, genere_evenement(val)); 
		
		fin=0;
		temps_arret = 1;  
        
		do{
			init_etat();
			indice=temps_arret;
			
			for (indice = 2*temps_arret-1; indice >= temps_arret; indice--){
			
				if(max_temps < indice){
					for (indice2 = 2*temps_arret-1; indice2 >= temps_arret; indice2--){
						stat[indice2] = u01();
						
						//GENERE VAL
						for(n=0; n<nb_ant; n++){
							Tab_VA[n][indice2]=n;
  						}
						//printf("\n\nTableau en t=%d\n", indice2);
						//for(n=0; n<nb_ant; n++){
						//	printf(" %d ", Tab_VA[n][indice2]);
						//}

						for (i=1; i<nb_ant; i++){
							place = ((int) (u01()*(nb_ant-1)))+1;
							//printf("\nOn tire l'echange de %d avec %d\n", i, place);
							temp = Tab_VA[place][indice2];
							Tab_VA[place][indice2] = Tab_VA[i][indice2]; 
							Tab_VA[i][indice2] = temp;
							//printf("On a TA_VA[%d][%d]=%d\n", place, indice2, Tab_VA[place][indice2]);
							//printf("On a TA_VA[%d][%d]=%d\n", i, indice2, Tab_VA[i][indice2]);	
						}
						
						//printf("\nTableau apres melange\n");
						//for(n=0; n<nb_ant; n++){
						//	printf(" %d ", Tab_VA[n][indice2]);
						//}
					}
					max_temps = indice;
				}
				
				val = (stat[indice]+(double)Tab_VA[num][indice]/(double)nb_ant) - ( ( (int)(stat[indice]+(double)Tab_VA[num][indice]/(double)nb_ant )/1)*1);

				set_trajectoire(indice, genere_evenement(val));  /* renvoie un evenement ******/
						
				transition_interface(get_etat_inf(),get_trajectoire(indice) ) ; 
				/* appel a fonction de transition qui a un etat_inf  evenement associe un etat_inf*/
				transition_interface(get_etat_sup(),get_trajectoire(indice) ); 
				/* appel a fonction de transition qui a un etat_sup  evenement associe un etat_sup */
			}
			/*  de indice temps_arret-1 a  O on reprend les memes trajectoires *********************/
	  
			for (indice = temps_arret-1; indice>=0; indice--){
					
				transition_interface(get_etat_inf(),get_trajectoire(indice));
				
				transition_interface(get_etat_sup(),get_trajectoire(indice));
			}        
		
			
			temps_arret=2*temps_arret;
			log2temps_arret[num]+=1;
	  		etat_inf=get_etat_inf();
			etat_sup=get_etat_sup();

			if(ptrfunc == NULL){
				for (i=0; i<NB_FILE; i++){
			
					/* on mesure pour chaque file d'attente le temps de couplage   *******/
					
					if ( (get_couplage(num, i) == 0) && (etat_inf[i] == etat_sup[i]) ){
						//printf("Echantillon %d la file %d couple avec un temps %d\n", echantillon, i, log2temps_arret);
						set_couplage(num, i, log2temps_arret[num]);
						//printf("%d a couple en %d \n\n\n\n", num, log2temps_arret[num]);
						etat_min_saved[num][i]=etat_inf[i];
						etat_max_saved[num][i]=etat_sup[i];
				
					}// fin if
				}// fin du for
				/* on verifie si l'ensemble des files d'attente ont couples    ********/
			
				if(test_couplage(num))
					fin++;
			}else{
				if((*ptrfunc)(etat_inf, etat_sup)){
					fin++;
					for (i=0; i<NB_FILE; i++){
						etat_min_saved[num][i]=etat_inf[i];
					}
				}
			}
      		}while((2*temps_arret < LG_TRAJ_MAX) && (! fin));   /**** do while ********/

      

     	} // for num 
        if (fin){
		if(ptraff == NULL)
			affichage(detail, echantillon, NB_FILE, nb_ant, etat_min_saved, etat_max_saved, log2temps_arret);
		else{
			
			affichage_dynamique(etat_min_saved , echantillon, nb_ant);
		}
	}else {
		fprintf(descfp,"%d echec %d \n",echantillon,log2temps_arret[0]);
	}
   } // echantillon
   printf("\n");
   gettimeofday(&tf,NULL);

   fprintf(descfp,"# Size %4d  Sampling time  : %f micro-seconds\n",NB_ECH,
   ((double) ((tf.tv_sec-ti.tv_sec)*1000000 + (tf.tv_usec-ti.tv_usec)))/NB_ECH);
   fprintf(descfp,"# Seed Value %d\n",rad);

	
   free(stat);
   free(log2temps_arret);
   
   for(i=0; i<nb_ant; i++){
        free(etat_max_saved[i]);	
        free(etat_min_saved[i]);
 	free(Tab_VA[i]);
   }
   free(etat_max_saved); 
   free(etat_min_saved);
   free(Tab_VA );
}
