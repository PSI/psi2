
/* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

//#include "data.c" /* force le module data �etre inline [MQ] */


/*! \file transition.c
 * \brief Functions of transition.c . 
 * \author  Bernard.Tanzi@imag.fr
 * \author  Jean-Marc.Vincent@imag.fr 
 * \author  Jerome.Vienne@imag.fr
 * \author  Gael.Gorgo@imag.fr
 * \date 2004-2006
 */

/*
*------------------------------------------------------------------
* Fichier      : transition.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme secondaire contenant les fonctions de transitions elementaires 
*   ainsi que la fonction de transition interface avec les fonction elementaires
* fichier pouvant ulterieurement etre eclate  en 2 fichiers 
* une bibliotheque de fonctions de transition et la fonction globale interface elle-meme
*-------------------------------------------------------------------------------
*/


/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>

/************************************************************/
/********* INCLUSION DES INTERFACES APPLICATIVES ************/
/************************************************************/

#include "transition.h"

#include "data.h"


/*********************************************************/
/*****  Constantes, MACROS et TYPES LOCAUX  *************/
/********************************************************/

/*** used in LOAD SHARING EVENT / INDEX FUCNCTIONS ***/
#define NEW -1 
#define INFINITY 1000000 
#define PHASE_1 1
#define PHASE_2 2

/*****************************************************/

//#define DEBUG 1

#ifdef DEBUG
FILE * log_fd = NULL; // text file used for execution log
#endif


/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/

/******* LOAD SHARING DATA *******/
int * priority; // priority functions used in index calculation [LOAD SHARING]
int * candidates; // set of candidates for priority functions calculation [LOAD SHARING]

/******* LTM DATA *******/
int * cur_neighbors; // set of neighbors of a the current node in systems with a general topology

/**********************************************************/
/********* DEFINITIONS DES VARIABLES EXPORTEES **************/
/**********************************************************/
extern double **table_index;
extern struct st_evt *evt;
extern struct st_multi *multis;



/******* LOAD SHARING DATA *******/

extern double (*ptr_ws_index)(int num_ori, int target_num, int dist, int ch_cible, int * priority);

/***********************************************************/
/********* IMPLANTATIONS DES FONCTIONS LOCALES **************/
/************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : arrivee_ext_deb_rejet
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* nb_dest   int       destination Queue number associate with event
*
* par       int *     pointer on the destination queue number table
*
*
* Called function : get_capacite
*
* Description  :
*
*      function exterior arrived on a queue
*	   starting from the destination queue number vector state 
*      and with destination queue number table
*      return vector state updated
*      
*----------------------------------------------------------------------
*/

int * arrivee_ext_deb_rejet(int etat[],int nb_dest,int * par){ 
  

	/*#ifdef DEBUG
	if(log_fd == NULL){
		log_fd = fopen("./test.log","w");
		
		if(log_fd == NULL)
			perror("pb ouverture fichier log");
	}
#endif

#ifdef DEBUG
	fprintf(log_fd,"\n\narrive sur %d", *(par+1));
	#endif*/

	//printf("kiki");
	int *  capacite = get_capacite();
  int k=1;
  //printf("Nombre de destinataire %d", nb_dest);
  while ((etat[*(par+k)] == capacite[*(par+k)]) && k<(nb_dest-2))
    /** parcours des files destinations
	tant que la file destination est pleine on passe a la suivante */
    k++;
  /* on verifie si la derniere file est pleine  */
  if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
    /** toutes les files destinations sont pleines on ne fait rien */
    ;
  else
    /** sinon on ajoute dans la file destination */
    etat[*(par+k)]=etat[*(par+k)]+1;     
  /** on renvoie l'adresse du vecteur d'etat */
  return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Function     : sortie_ext
*
* Result       : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* numFile   int       queue number
*
* Description  :
*
*      function exit to a queue from exterior
*	   starting from the destination queue number vector state 
*	   return an integer giving the queue client remaining in the queue 
*      
*----------------------------------------------------------------------
*/

int sortie_ext (int etat[],int numFile){ 
   if (etat[numFile] > 0 ) 
     etat[numFile]=etat[numFile]-1;
   return etat[numFile];
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_rejet
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adress of the begining of state vector
*
* nb_dest   int      destination+origin queue number associate with event
*
* par       int *     pointer on the origin, destination number table
*
* Called function : get_capacite,get_nb_file
*
* Description  :
*  
*      routage avec debordement sur ne files  avec rejet
*      fonction de routage d'une file origine vers n files destination 
*  au cas ou toutes les files destinations sont pleines, le paquet emis est perdu 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  
  if (etat[*par] > 0){
      k=1;
      etat[*par]=(etat[*par]-1);
      while (k<(nb_dest-1) && etat[*(par+k)] == capacite[*(par+k)]) 
	k++;
      /* on verifie si la derniere file est pleine  */
      if( k == (nb_dest-1) || etat[*(par+k)] == capacite[*(par+k)])
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
      else
		etat[*(par+k)]=etat[*(par+k)]+1;     
    }
   return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Function     : routage_nfile_bloc
*
* Result       : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat      int []    adresse de debut du vecteur d'etat
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*      routage avec debordemetn avec rejet
*      fonction de routage d'une file origine vers une premiere file destination et si elle
* est pleine vers une deuxieme file destination 
*  au cas ou la deuxieme file destination  est pleine le paquet emis est perdu
*      a partir du vecteur d'etat d'un numero de file origine et d'un
*     numero de 1ere file destiantaire d'un numero de deuxieme file destinataire 
*         renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  routage_nfile_bloc (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int k;
  

  if (etat[*par] > 0)
    {
      k=1; 
      while (etat[*(par+k)] == capacite[*(par+k)] && k<(nb_dest-2))
	k++;
      if((k == (nb_dest-2)) && (etat[*(par+k)] == capacite[*(par+k)]))
	/** toutes les files destinations sont pleines on ne fait rien */
	  ;
	else
	  {
	    etat[*par]=(etat[*par]-1);
	    etat[*(par+k)]=etat[*(par+k)]+1; 
	  }    
    }
  return (&etat[0]);
  }


/**
*----------------------------------------------------------------
*
* Fonction     : JSQ_rejet
*
* Resultat     : int *
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Called function : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  JSQ_rejet (int etat[],int nb_dest,int * par){ 
  int *  capacite = get_capacite();
  int num_file_min;
  int k=1;
  
  /* file origine != -1*/
  if(*par != -1){
  	
		if( etat[*par] == 0){
		return (&etat[0]);
	} else {	
		etat[*par]-=1;
		// On cherche la premiere file non pleine et on verifie que l'on ne depasse pas nbdest
    		while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    		}

            	// num_file_min prend la valeur de la 1ere file non pleine trouv
            	num_file_min=*(par+k);
		k++;
		while(k<(nb_dest-1)){		
                    if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
			num_file_min=*(par+k);
		    k++;
		} ;
		if(etat[num_file_min] < capacite[num_file_min]){
			etat[num_file_min]+=1;
		}
	}	return (&etat[0]);
  } else {
  	/* File d'origine == -1 */
	while ( k<(nb_dest-1) && !(etat[*(par+k)] < capacite[*(par+k)])){
			k++;
    	}

        // num_file_min prend la valeur de la 1ere file non pleine trouv
        num_file_min=*(par+k);
	k++;
	while(k<(nb_dest-1)){		
        if ( (etat[*(par+k)]<etat[num_file_min]) && (etat[*(par+k)] < capacite[*(par+k)]))
		num_file_min=*(par+k);
		k++;
	} ;
	if(etat[num_file_min] < capacite[num_file_min]){
		etat[num_file_min]+=1;
	}
	return (&etat[0]);
  }
}



/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_index(int etat[],int nb_dest,int * par, int * numf){ 
  
  int *  capacite = get_capacite();
  int num_file_min, j;
  int k=1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  
   // We look at the first not empty queue in the list of destination 
   while (k<nb_dest-1 && *(par+k)!=-1 &&!(etat[*(par+k)] < capacite[*(par+k)]) ){
		k++;
   }
   
   // num_file_min has the value of the first not empty queue
   num_file_min=*(par+k);
   j=k;	
   while(k<nb_dest){
				
	if (  *(par+k)!=-1 && (table_index[*(numf+k)][etat[*(par+k)]] < table_index[*(numf+j)][etat[num_file_min]]) && (etat[*(par+k)] < capacite[*(par+k)]) ){
		num_file_min=*(par+k);
		j=k;
	}
	if( *(par+k)== -1 && (table_index[*(numf+k)][0] < ( (table_index[*(numf+j)][etat[num_file_min]]) +1)  ) ){
		num_file_min = -1;
	}
	k++;
   };
   
   //Check if the queue is really not empty
   if( (num_file_min != -1) && etat[num_file_min] < capacite[num_file_min] ){
	etat[num_file_min]=etat[num_file_min]+1;
   }
   
   return (&etat[0]); 
}

/**
*----------------------------------------------------------------
*
* Fonction     :  Arrivee_rejet_multi_index
*
* Resultat     : int *
*
* Parameters   : 
*
* Name      Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
* 
* Function called  : get_capacite
*
* Description  :
*  
*   routage vers la file ayant le moins de client avec rejet si toutes les files destination sont pleines
*              renvoie un pointeur sur le vecteur d'etat mis a jour 
*      
*----------------------------------------------------------------------
*/

int *  Arrivee_rejet_multi_index(int etat[],int nb_dest,int * par, int * numf, int ** tab){ 
  
  int *  capacite = get_capacite();
  int i, j;
  int Setat=0, Scapa=0;
  int nb_file;
  double val_min=100000000;
  int file_min=-1;

  //If the first file is different of -1
  if(*par != -1){
	//Check if the queue is empty 
	if (etat[*par]> 0){
	    //If it's not empty, a client leave the original queue
		etat[*par]-=1;
	} else {
	    // If the queue is empty we return the original state
		return (&etat[0]);
	}
  }
  
  //printf("\n\n\n");
  
  for(i=1; i<nb_dest;i++){
	Setat=0;
	Scapa=0;
	nb_file = tab[i][0];
	//printf("Il y a %d file sur le serveur %d\n", nb_file, i);
	
	//si on n'est pas avec la file -1
	if(nb_file != -1){
		for (j=1; j<(nb_file+1); j++){
		    //printf("File: %d en %d, %d\n", tab[i][j], i, j);
			// on accumule le nb de client
			//printf("On ajoute %d a Setat (file %d)\n", etat[tab[i][j]], tab[i][j]);
			Setat += etat[tab[i][j]]; 
			// on accumule le nb de capa
			//printf("On ajoute %d a Scapa (file %d)\n", capacite[tab[i][j]], tab[i][j]);
			Scapa += capacite[tab[i][j]];
		}
		
		if(Setat<Scapa){
			// compare valeur min
			// Si plus petit, on change et on garde
			//printf("On a file = %d Se<Sc, valeur d'index %lf\n", *(numf+i), table_index[*(numf+i)][Setat]); 
			if(table_index[*(numf+i)][Setat]<val_min){
				val_min = table_index[*(numf+i)][Setat];
				//printf("On a file_min = %d\n", *(numf+i));
				file_min = i;
			}
		}
	} else {
		//printf("On est en -1 et on a numf+i = %d\n", *(numf+i));
		if(table_index[*(numf+i)][0]<val_min)
			file_min = -1;
	}
	
  }
  
  //printf("On a file_min: %d \n", file_min);
  
  // on a trouv le multi-serveur  index min
  // Maintenant, on va ajout le client
  if( file_min != -1){
	nb_file = tab[file_min][0];
	
	for (j=1; j<(nb_file+1); j++){
		if (etat[tab[file_min][j]] < capacite[tab[file_min][j]]){
			//printf("On va incrmenter la file %d\n", tab[file_min][j]);
			etat[tab[file_min][j]] +=1;
			return (&etat[0]); 
		}
	}
  }else {
	return (&etat[0]); 
  }
  
  printf("Pb ds multi_serveur index");
  return (&etat[0]);
}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_multi_serveur
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_multi_serveur (int etat[],int nb_dest,int * par){ 

int *  capacite = get_capacite();
int i=2;
// If there are 1 client or more in the "cache" queue, we extrat one of them 
if (etat[*par]!=0 && etat[*par+1]!=0 ){
    etat[*par]-=1;
// else we check if there is a client in the queue
}else{
    // If It's the case, we extract the client from the queue
    if (etat[*par]==0 && etat[*(par+1)] !=0){
        etat[*(par+1)]-=1;
    } else { 
		if (etat[*par]!=0 && etat[*(par+1)] ==0){
			 etat[*par]-=1;
			   etat[*(par+1)]+=1;
			return (&etat[0]);
		}else{
			return (&etat[0]);
		}
	}
}
// On parcours la liste des distantion, on fait +1 à la 1ere destination non pleine


// if the destination queue is not -1 & i < nb_dest
while(i< nb_dest && *(par+i)!=-1){
	if(etat[*(par+i)] < capacite[*(par+i)]){
		etat[*(par+i)]+=1;
		i=nb_dest;
	}
	i++;
}
// if all queues are full, there is rejection of client
return (&etat[0]);

}

/**
*----------------------------------------------------------------
*
* Fonction     : Depart_call_center
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat		int[]	adress of the begining of vector etat
* 
* nb_dest	int		number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contain the number of the origin & destination queue
*
* nb_ori	int		number of origin queue
* 
* Description  :
*  
*   
*              
*             
*----------------------------------------------------------------------
*/
int *  Depart_call_center (int etat[],int nb_dest,int * par, int nb_ori){ 

	int i=0;
	int *  capacite = get_capacite();

	
	
	//On cherche la premiere file non vide
	while (i < nb_ori && etat[*(par+i)]==0){
		i++;
	} 
	
	
	//Test au cas si on est sur une file origine et non -1 (i =nb_ori)
	if( *(par+i)!=-1 ){
			//test pour eviter erreur dut a x: (alors qu'il faut x : sinon nb_ori =0) pour eviter de vider une file vide
	    if (capacite[*(par+i)]<=0) {
			printf("Problem in description file !\n");
		} else {
			//printf("On vide %d de capacite %d \n", *(par+i), etat[*(par+i)]);
			etat[*(par+i)]-=1;
		}
	}
		
	return (&etat[0]);
	

}



/**
*----------------------------------------------------------------
*
* Fonction     : Independent_Pull
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* num_ori	   int		numero of the origin queue
* 
* rand_par   int *  table of random numbers (simulation of random choices) 
* 
* Description  :
*  
*   Independent Pull control event.
*   the origin queue where the event occurs tries to steal a task on another target queue 
*   according to index functions specified in index.c.
*              
*             
*----------------------------------------------------------------------
*/

int * Independent_Pull(int etat[], int num_ori, int * rand_par){

	int i, next_val, max_value, target_num; // for index argmax calculation

	int nb_candidates = get_nb_file(); // to manage a set of potential target
                                     // with the global int[] candidates array

	int rand_candidate; // a randomly choosen candidate

	/* allocation of data structures if never allocated by some other event before*/
	if(candidates == NULL)
		candidates = (int *)malloc(get_nb_file() * sizeof(int));
	if(priority == NULL)
		priority = (int *)malloc(get_nb_file() * sizeof(int));
	/******************************************************************************/

	/* initialization of random parameter*/
	if(rand_par[0] == NEW){
		for(i=0;i<get_prob_limit();i++){
			rand_par[i] = random();
		}
 	}
	/*************************************/
	
	
	for(i=0;i<get_nb_file();i++){
		priority[i] = INFINITY;
	}
	for(i=0;i<nb_candidates;i++){
		candidates[i] = i;
	}

	nb_candidates--;
	candidates[num_ori] = candidates[nb_candidates]; // num_ori is removed from the set of candidates
	

	/* allocation of a priority to a sub-set of candidate of size prob_limit */ 
	i = 0;
	while(i<get_prob_limit() && nb_candidates >= 0){
		
		rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
		priority[candidates[rand_candidate]] = i;
		
		nb_candidates--;
		candidates[rand_candidate] = candidates[nb_candidates];

		i++;
	}
	/***************************************************************************/
	
	/* calculation of the target for a task theft by index functions (index.c)*/
	target_num = 0;
	max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etat[0], priority);

	for(i=1;i<get_nb_file();i++){
		next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etat[i], priority);
		
		if(next_val > max_value){
			max_value = next_val;
			target_num = i;
		}
		
	}
	/**************************************************************************/
		
	/* the origin steals a task on the target*/
	if(target_num == num_ori){
		;
	}
	else{
		etat[target_num] -= 1;
		etat[num_ori] += 1;
	}	

	return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Fonction     : Independent_Push
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* num_ori	   int		numero of the origin queue
* 
* Description  :
*  
*   Independent push control event.
*   The origin queue tries to transfer a task 
*   from itself to another queue according to index functions defined in            
*   index.c.
*              
*             
*----------------------------------------------------------------------
*/

int * Independent_Push(int etat[], int num_ori, int * rand_par){

	int i, next_val, min_value, target_num; // for index argmin calculation
	
	
	int nb_candidates = get_nb_file(); // for managing a set of potential target
                                     // with the global int[] candidates array

	int rand_candidate; // a randomly choosen candidate
	

	/* allocation of data structures if never allocated by some other event before*/
	if(candidates == NULL)
		candidates = (int *)malloc(get_nb_file() * sizeof(int));
	
	if(priority == NULL)
		priority = (int *)malloc(get_nb_file() * sizeof(int));
	/*******************************************************************************/

	/* initialization of random parameter*/
	if(rand_par[0] == NEW){
		for(i=0;i<get_prob_limit();i++){
			rand_par[i] = random();
		}
 	}
	/*****************************************************************************/

	
	for(i=0;i<get_nb_file();i++){
		priority[i] = INFINITY;
	}
	for(i=0;i<nb_candidates;i++){
		candidates[i] = i;
	}


	nb_candidates--;
	candidates[num_ori] = candidates[nb_candidates]; // num_ori is removed from the set of candidates

	/* allocation of a priority to a sub-set of candidate of size prob_limit */ 	
	i = 0;
	while(i<get_prob_limit() && nb_candidates >= 0){
		
		rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
		priority[candidates[rand_candidate]] = i;
		
		nb_candidates--;
		candidates[rand_candidate] = candidates[nb_candidates];

		i++;
	}
	/*************************************************************************/

	/* calculation of the target for a task transfer by index functions (index.c)*/
	target_num = 0;
	min_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etat[0], priority);

	for(i=1;i<get_nb_file();i++){
		next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etat[i], priority);
		
		if(next_val < min_value){
			min_value = next_val;
			target_num = i;
		}
		
	}
	/*************************************************************************/

#ifdef DEBUG
	if(log_fd == NULL){
		log_fd = fopen("./test.log","w");
		
		if(log_fd == NULL)
			perror("pb ouverture fichier log");
	}


		fprintf(log_fd,"\n\netat du système:\n");
		for(i=0;i<get_nb_file();i++){
			fprintf(log_fd,"%d ",etat[i]);			
		}
#endif
		
		if(target_num == num_ori){
			;
		}
		else{
			etat[target_num] += 1;
			etat[num_ori] -= 1;
		}
		

#ifdef DEBUG	
	fprintf(log_fd,"\n choix pour control sur %d : %d\n", num_ori, target_num);
	fprintf(log_fd,"\netat prime:\n");
	for(i=0;i<get_nb_file();i++){
		fprintf(log_fd,"%d ",etat[i]);			
	}
	
	fprintf(log_fd,"\nrand permut:\n");
	for(i=0;i<get_nb_file();i++){
		fprintf(log_fd,"%d ",priority[i]);			
	}
#endif
	
	return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Fonction     : Arrival_Push
*
* Resultat     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* num_ori	   int		numero of the origin queue
* 
* Description  :
*  
*   Push on arrival load sharing event
*   On the arrival of a task, the origin queue tries to transfer a task 
*   from itself to another queue according to index functions defined in            
*   index.c          
*----------------------------------------------------------------------
*/

int * Arrival_Push(int etat[], int num_ori, int * rand_par){

	int i, next_val, min_value, target_num; // for index argmin calculation

	int prev_state_ori; // to save the origin state before the task arrival	
                      // enables to distinguish the cases etat[num_ori] = capacite_file(num_ori)
	                    // and etat[num_ori] = capacite_file(num_ori) - 1.
	
	int nb_candidates = get_nb_file(); // for managing a set of potential target
                                     // with the global int[] candidates array

	int rand_candidate; // a randomly choosen candidate
	
	/* allocation of data structures if never allocated by some other event before*/
	if(candidates == NULL)
		candidates = (int *)malloc(get_nb_file() * sizeof(int));
	
	if(priority == NULL)
		priority = (int *)malloc(get_nb_file() * sizeof(int));
	/******************************************************************************/

	/* initialization of random parameter*/
	if(rand_par[0] == NEW){
		for(i=0;i<get_prob_limit();i++){
			rand_par[i] = random();
		}
 	}
	/*************************************/

	prev_state_ori = etat[num_ori];

	if(etat[num_ori] < capacite_file(num_ori))
		etat[num_ori] += 1;// arrival on the origin queue


	for(i=0;i<get_nb_file();i++){
		priority[i] = INFINITY;
	}
	for(i=0;i<nb_candidates;i++){
		candidates[i] = i;
	}


	nb_candidates--;
	candidates[num_ori] = candidates[nb_candidates]; // num_ori is removed from the set of candidates
	
	/* allocation of a priority to a sub-set of candidate of size prob_limit */ 
	i = 0;
	while(i<get_prob_limit() && nb_candidates>=0){
		
		rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
		priority[candidates[rand_candidate]] = i;
		
		nb_candidates--;
		candidates[rand_candidate] = candidates[nb_candidates];
		
		i++;
	}
	/*************************************************************************/
	
	/* calculation of the target for a task theft by index functions (index.c)*/
	target_num = 0;
	min_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etat[0], priority);

	for(i=1;i<get_nb_file();i++){
		next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etat[i], priority);
		
		if(next_val < min_value){
			min_value = next_val;
			target_num = i;
		}
	}
	/**************************************************************************/


	if(target_num == num_ori){
		;
	}
	else{
		if(etat[target_num] < capacite_file(target_num)){
			etat[target_num] += 1;
			if(prev_state_ori < etat[num_ori]) // the task don't make an overflow on the origin queue
				etat[num_ori] -= 1;
		}
	}
	
	return (&etat[0]);
}


/**
*----------------------------------------------------------------
*
* Function     : LTM_arrival
*
* Result       : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* num_ori   int       numero of the origin queue
*
* parametre   int *    pointer on the array memorizing random numbers for this event
*
*
* Called function : capacite_file, get_neighbors, get_nb_neighbors, 
*
* Description  :
*
*   exterior arrival on the origin queue with random cascading
*	  to the final destination according to the random numbers
*   memorized in rand_par
*     
*      
*----------------------------------------------------------------------
*/

int * LTM_arrival(int etat[], int num_ori, int* rand_par){
	
	int i,j; // loop counters
	int cur_node; // the current evaluated node in the main loop
	int next_target; // the next neighbor where we try to push the arriving task 
	int next_u; // the next memorized random number
	int index_u, index_target; // the next index of next_u, next_target
	int cpt_neighbors; // variable counting the number of current non visited neighbors
	
#ifdef DEBUG
	if(log_fd == NULL){
		log_fd = fopen("./test.log","w");
		
		if(log_fd == NULL)
			perror("pb ouverture fichier log");
	}
#endif

	if(etat[num_ori] == capacite_file(num_ori)){
		return etat;
	}
	else{ // we have to find a hole where to push the arriving task
		
		/****** initialisation *********/
		cur_node = num_ori;
		
		if(cur_neighbors == NULL)
			cur_neighbors = (int *)malloc(get_nb_file() * sizeof(int));
		//size of cur_neighbors is bounded by the number of queues
		//but we use values of which index are between 0 and cpt_neighbors-1
		if(cur_neighbors == NULL)
			perror("cur_neighbors allocation");
		
		
		memcpy(cur_neighbors, get_neighbors(num_ori), get_nb_neighbors(num_ori)*sizeof(int));
		
		cpt_neighbors = get_nb_neighbors(num_ori);

		index_u = 0;
		
		/*******************************/
		
		while(cpt_neighbors > 0){
			
			if(rand_par[index_u] == NEW){
				rand_par[index_u] = random();
			}
			next_u = rand_par[index_u];
		 
			index_target = (int)((double)next_u/(double)RAND_MAX * cpt_neighbors);
			
			next_target = cur_neighbors[index_target];
			
			if(etat[next_target] < etat[cur_node]){
				cur_node = next_target;
				cpt_neighbors = get_nb_neighbors(cur_node);
				memcpy(cur_neighbors, get_neighbors(cur_node), get_nb_neighbors(cur_node)*sizeof(int));
			}
			else{ // etat[next_target] >= etat[cur_node]
				cpt_neighbors--;

				cur_neighbors[index_target] = cur_neighbors[cpt_neighbors];
			}
			
			index_u++;
		}
	

#ifdef DEBUG
		fprintf(log_fd,"etat du système:\n");
		for(i=0;i<get_nb_file();i++){
			fprintf(log_fd,"%d ",etat[i]);			
		}
		fprintf(log_fd,"\n choix pour arrivee sur %d : %d\n", num_ori, cur_node);
#endif

		/*** update of the load of cur_node ***/
		etat[cur_node] += 1;
		
		/**************************************/

		return etat;
	}
}


/**
*----------------------------------------------------------------
*
* Function     : LTM_departure
*
* Result       : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* num_ori   int       numero of the origin queue
*
* rand_par   int *    pointer on the array memorizing random numbers for this event
*
*
* Called function : capacite_file, get_neighbors, get_nb_neighbors, 
*
* Description  :
*
*   departure on the origin queue with random cascading
*	  from the final destination according to the random numbers
*   memorized in rand_par
*     
*      
*----------------------------------------------------------------------
*/
int * LTM_departure(int etat[], int num_ori, int* rand_par){
	
	int i,j; // loop counters
	int cur_node; // the current evaluated node in the main loop
	int next_target; // the next neighbor where we try to pull the leaving task 
	int next_u; // the next memorized random number
	int index_u, index_target; // the next index of next_u, next_target
	int cpt_neighbors; // variable counting the number of current non visited neighbors
	
#ifdef DEBUG
	if(log_fd == NULL){
		log_fd = fopen("./test.log","w");
		
		if(log_fd == NULL)
			perror("pb ouverture fichier log");
	}
#endif

	if(etat[num_ori] == 0){
		return etat;
	}
	else{ // we have to find a hole where to push the arriving task
		
		/****** initialisation *********/
		cur_node = num_ori;
		
		if(cur_neighbors == NULL)
			cur_neighbors = (int *)malloc(get_nb_file() * sizeof(int));
		//size of cur_neighbors is bounded by the number of queues
		//but we use values of which index are between 0 and cpt_neighbors-1
		if(cur_neighbors == NULL)
			perror("cur_neighbors allocation");
		
		
		memcpy(cur_neighbors, get_neighbors(num_ori), get_nb_neighbors(num_ori)*sizeof(int));
		
		cpt_neighbors = get_nb_neighbors(num_ori);

		index_u = 0;
		
		/*******************************/
		
		while(cpt_neighbors > 0){
			
			if(rand_par[index_u] == NEW){
				rand_par[index_u] = random();
			}
			next_u = rand_par[index_u];
		 
			index_target = (int)((double)next_u/(double)RAND_MAX * cpt_neighbors);
			
			next_target = cur_neighbors[index_target];
			
			if(etat[next_target] > etat[cur_node]){
				cur_node = next_target;
				cpt_neighbors = get_nb_neighbors(cur_node);
				memcpy(cur_neighbors, get_neighbors(cur_node), get_nb_neighbors(cur_node)*sizeof(int));
			}
			else{ // etat[next_target] >= etat[cur_node]
				cpt_neighbors--;

				cur_neighbors[index_target] = cur_neighbors[cpt_neighbors];
			}
			
			index_u++;
		}
	

#ifdef DEBUG
		fprintf(log_fd,"etat du système:\n");
		for(i=0;i<get_nb_file();i++){
			fprintf(log_fd,"%d ",etat[i]);			
		}
		fprintf(log_fd,"\n choix pour depart sur %d : %d\n", num_ori, cur_node);
#endif

		/*** update of the load of cur_node ***/
		etat[cur_node] -= 1;
		
		/**************************************/

		return etat;
	}
}


/*************************************************************************************************************/
/******************************   NON MONOTONE SYSTEMS   ****************************************************/
/*************************************************************************************************************/

/**
*----------------------------------------------------------------
*
* Function     : negative_customer
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etatInf   int []    address of the begining of state Inf vector
*
* etatSup   int []    address of the begining of state Sup vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (>= 0)
*
*
* Called function :
*
* Description  :
*  
*      Negative customer with routing 
*             
*      
*      
*----------------------------------------------------------------------
*/

int  negative_customer(int etatInf[], int etatSup[], int *par){ 

  int source = *par;
  int dest = *(par+1);

  if (dest == -1) {
	printf("Error: negative customer destination cannot be exterior.\n");
  } 

  if (etatSup==NULL) {
	/* negative customer external arrival (source == -1) or impatience (source == dest) */
  
  	if ((source == -1) || (source==dest)){ //simple departure
  		if (etatInf[dest] > 0) etatInf[dest] = etatInf[dest]-1;
  	}

  	else { /* negative customer routing (source != dest) */
		if (etatInf[source] && etatInf[dest] > 0 ) etatInf[dest]--;
		if (etatInf[source] > 0) etatInf[source]--;
	}

  }
  else{

  	/* negative customer external arrival (source == -1) or impatience (source == dest) */
  
  	if ((source == -1) || (source==dest)){ //simple departure
  		if (etatInf[dest] > 0) etatInf[dest] = etatInf[dest]-1;
		if (etatSup[dest] > 0) etatSup[dest] = etatSup[dest]-1;
  	}

  	else { /* negative customer routing (source != dest) */

  		if (etatInf[source]>0) { /* negative customer in both Inf and Sup state */
			etatInf[source]=etatInf[source]-1;
			if (etatInf[dest]>0) etatInf[dest]=etatInf[dest]-1;

			etatSup[source]=etatSup[source]-1;
			if (etatSup[dest]>0) etatSup[dest]=etatSup[dest]-1;
  		}
  		else if (etatSup[source]==0); /* negative customer in neither Inf nor Sup state */
  		else { /* negative customer only in Sup state */
		       /* the only case that breaks the monotonicity property */
			etatSup[source] = etatSup[source]-1;
			if (etatInf[dest] < etatSup[dest]) etatSup[dest] = etatSup[dest]-1;
			else if (etatInf[dest] > 0) { 
				etatInf[dest] = etatInf[dest]-1;
			}
  		}
  	}
  }

  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : batch
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etatInf   int []    address of the begining of state Inf vector
*
* etatSup   int []    address of the begining of state Sup vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (-1 if exterior)
*
* size	    int      batch size
*
* Called function :
*
* Description  :
*  
*      Batch arrival/routing/service (non decomposable)
*             
*      
*      
*----------------------------------------------------------------------
*/

int batch(int etatInf[], int etatSup[], int *par, int size){ 

  int source = *par;
  int dest = *(++par);
  int tmp;
  int *  capacite = get_capacite();
  int flagInf=0;

  if (etatSup==NULL) {

	if (source != -1) {
		//Check if the queue has at least size clients
		if (etatInf[source]>=size){
			//If yes, size clients leave the original queue
			etatInf[source]-=size;
		} 
		else {
	    		// If the queue has less than size clients, we return the original state
			return (0);
		}
  	} 

  	if ((dest != -1) && (etatInf[dest] + size <= capacite[dest])) etatInf[dest]+=size;
  }
  else {
	//If the first queue is different of -1
  	if(source != -1){
		//Check if the Sup queue has at least size clients 
		if (etatSup[source]>=size){ //If it's not empty, a client leave the original queue
                        if (etatInf[source]>= size) {
				etatInf[source]-=size;
				etatSup[source]-=size;
			}
			else {
				etatInf[source]=0; 
				etatSup[source] = size-1;
				flagInf = 1;  //There is at least one state for which there is no batch
			} 
		}
		else {
	    	// If the Sup queue doesn't have at least size clients, we return the original states
			return (0);
		}
  	}

        if (dest != -1){

        	if (etatSup[dest] + size <= capacite[dest]) {
			if (flagInf==0) etatInf[dest]+=size;
			etatSup[dest]+=size;
  		}
  		else if (etatInf[dest] + size > capacite[dest]) {}
  		else {
			if (flagInf==0) {
				etatInf[dest]+=size;
				tmp=capacite[dest]-size+1;
				if (tmp<etatInf[dest]) etatInf[dest]=tmp;
			}
			etatSup[dest]=capacite[dest];
  		}
	}
  }
  
  return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : inf_batch
*
* Result       : int
*
* Parameters   : 
*
* Name      Type      Role
*
* x         int       inf state
* y         int       sup state 
* C	    int       buffer capacity
* k         int       batch size
*
* Description  :
*
*     Returns the new inf state
*
*----------------------------------------------------------------------
*/

int inf_batch(int x, int y, int C, int k){

	#ifdef DEBUG 
		printf("(inf) batch size=%d\n", k);
	#endif
	if (y+k <= C) return x+k;
	else if (x+k > C) return x;
	else if (x+k <= C-k+1) return x+k;
	else return C-k+1; 
}

/**
*----------------------------------------------------------------
*
* Function     : sup_batch
*
* Result       : int
*
* Parameters   : 
*
* Name      Type      Role
*
* x         int       inf state
* y         int       sup state 
* C	    int       buffer capacity
* size      int       batch size
*
* Description  :
*
*     Returns the new sup state
*
*----------------------------------------------------------------------
*/

int sup_batch(int x, int y, int C, int k){
	
	if (y+k <= C) return y+k;
	else if (x+k > C) return y;
	else return C; 
}


/**
*----------------------------------------------------------------
*
* Fonction     :  batch_index_routing
*
* Resultat     : int 
*
* Parameters   : 
*
* Name		Type	Role
*
* etatInf	int []    address of the begining of state Inf vector
*
* etatSup	int []    address of the begining of state Sup vector
* 
* nb_dest	int	number of files associated to the event (origin+destination queue)
*
* par		int*	pointer of the table which contains the number of the origin & destination queue
*
* numf		int*	pointer of the table which contains the information on index functions to be used with each queue	
*
* size		int     batch size
* 
* Function called  : get_capacite, get_nb_file
*
* Description  :
*  
*   batch index routing with rejection if the batch cannot be accepted due to the finite capacity (no partial acceptance) 
*   
*      
*----------------------------------------------------------------------
*/

int batch_index_routing(int etatInf[], int etatSup[], int nb_dest, int * par, int * numf, int size){ 
  
  int *  capacite = get_capacite();
  int minInf, minSup, jMinInf, jMinSup;
  int k;
  int x, y, C;
  int b, flag, val, i, j, tmp;
  int *etatInfOld, *etatSupOld;
  int nbfile = get_nb_file();
  int flagInf = 0;

  if (etatSup==NULL) { 

	#ifdef DEBUG 
		printf("BIR: one state\n");
	#endif
	
	//Create copy of Inf 
	etatInfOld = (int *) calloc(nbfile, sizeof(int));
	
	for (i=0; i<nbfile; i++) {
		etatInfOld[i] = etatInf[i];
	}

	//If the first queue is different of -1
  	if(*par != -1){
		//Check if the queue has at least size clients
		if (etatInfOld[*par]>=size){
	    	//If yes, size clients leave the original queue
			etatInf[*par]-=size;
		} else {
	    	// If the queue has less than size clients, we return the original state
			return (0);
		}
  	}
     
   	// minInf has the value of the first queue with minimal value of index function
   	k=1;
	minInf=*(par+k);
   	jMinInf=k;	
   	while(k<nb_dest){
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] < table_index[*(numf+jMinInf)][etatInfOld[minInf]]) ){
			minInf=*(par+k);
			jMinInf=k;
		}

		if (  *(par+k)==-1 ) printf("Warning: batch index routing ignores -1 as destination\n");
		k++;
   	};
   
   	//Check if the queue is really not empty
   	if( (minInf != -1) && etatInfOld[minInf] + size <= capacite[minInf] ){
		etatInf[minInf]=etatInfOld[minInf]+size;
   	}

	free(etatInfOld);
   }
   else{
	#ifdef DEBUG 
		printf("BIR: (Inf, Sup)\n");
	#endif

	//Create copies of Inf and Sup
	etatInfOld = (int *) calloc(nbfile, sizeof(int));
	etatSupOld = (int *) calloc(nbfile, sizeof(int));

	for (i=0; i<nbfile; i++) {
		etatInfOld[i] = etatInf[i];
		etatSupOld[i] = etatSup[i];
	}

	
	//If the first queue is different of -1
  	if(*par != -1){
		//Check if the Sup queue has at least size clients 
		if (etatSupOld[*par]>=size){ //If it's not empty, a client leave the original queue
                        if (etatInfOld[*par]>= size) {
				etatInf[*par]-=size;
				etatSup[*par]-=size;
			}
			else {
				etatInf[*par]=0; 
				etatSup[*par] = size-1;
				flagInf = 1;  //There is at least one state for which there is no batch
			} 
		}
		else {
	    	// If the Sup queue doesn't have at least size clients, we return the original states
			return (0);
		}
  	}

        // minInf (minSup) has the value of the first queue with minimal value of index function for state Inf (Sup)
   	k=1;
	minInf=*(par+k);
	minSup=*(par+k); 
   	jMinInf=k; 
	jMinSup=k;
	while(k<nb_dest){
		//Inf		
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] < table_index[*(numf+jMinInf)][etatInfOld[minInf]]) ){
			minInf=*(par+k);
			jMinInf=k;
		}
		
		//Sup
		if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatSupOld[*(par+k)]] < table_index[*(numf+jMinSup)][etatSupOld[minSup]]) ){
			minSup=*(par+k);
			jMinSup=k;
		}
		
		if (  *(par+k)==-1 ) printf("Warning: batch index routing ignores -1 as destination\n");
		k++;
   	};

	

	#ifdef DEBUG 
		printf("Inf: %d Sup: %d\n", minInf, minSup);
	#endif

/*	//First case: indexInf[x] >= indexSup[minSup], for all x
	if (table_index[*(numf+jMinInf)][etatInfOld[minInf]] >= table_index[*(numf+jMinSup)][etatSupOld[minSup]]){
		#ifdef DEBUG 
			printf("case 1\n");
		#endif
		x = etatInfOld[minSup]; y = etatSupOld[minSup];
		C = capacite[minSup];
		etatInf[minSup] = inf_batch(x, y, C, size);
		etatSup[minSup] = sup_batch(x, y, C, size);
	}
	else{
		#ifdef DEBUG 
			printf("case 2\n");
		#endif*/
		k=1;
		while(k<nb_dest){
			if (  *(par+k)!=-1 && (table_index[*(numf+k)][etatInfOld[*(par+k)]] <= table_index[*(numf+jMinSup)][etatSupOld[minSup]]) ) {
/*				#ifdef DEBUG 
					printf("file: %d\n", *(par+k));
					printf("Sup\n");
				#endif*/

				//Sup
				if (k==jMinSup) {
/*					#ifdef DEBUG 
						printf("case a\n");
					#endif*/
					etatSup[minSup] = sup_batch(etatInfOld[minSup], etatSupOld[minSup], capacite[minSup], size);
				}
				else {
/*					#ifdef DEBUG 
						printf("case b\n");
					#endif*/
					b = 0; flag=1;
					while (flag){
						b++;
/*					 	#ifdef DEBUG 
							printf("b=%d, x=%d, y(min)=%d\n",b, etatInf[*(par+k)]+b, etatSup[minSup]);
						#endif*/
						if ((etatInfOld[*(par+k)]+b > etatSupOld[*(par+k)]) || (table_index[*(numf+k)][etatInfOld[*(par+k)]+b] > table_index[*(numf+jMinSup)][etatSupOld[minSup]])){
							flag=0;
							b--;
						}	
					}
					etatSup[*(par+k)] = sup_batch(etatInfOld[*(par+k)], etatInfOld[*(par+k)]+b, capacite[*(par+k)], size);
					if (etatSup[*(par+k)] < etatSupOld[*(par+k)]) etatSup[*(par+k)] = etatSupOld[*(par+k)];
				}
/*				#ifdef DEBUG 
					printf("Inf\n");
				#endif*/

				//Inf
				if (k==jMinInf && flagInf==0) {
					j = 1; 
					//second smallest
					if (jMinInf!=1) val = table_index[*(numf+1)][etatInfOld[*(par+1)]];
					else val = table_index[*(numf+2)][etatInfOld[*(par+2)]];
					while(j<nb_dest){
						if (j!=jMinInf && table_index[*(numf+j)][etatInfOld[*(par+j)]] < val){
							val = table_index[*(numf+j)][etatInfOld[*(par+j)]];
						}
						j++;
					}					
					if (table_index[*(numf+jMinInf)][etatSupOld[*(par+jMinInf)]] <= val) {
						etatInf[minInf] = inf_batch(etatInfOld[minInf], etatSupOld[minInf], capacite[minInf], size);
						#ifdef DEBUG
							printf("inf(inf=%d, sup=%d, cap=%d, size=%d)=%d\n", etatInfOld[minInf], etatSupOld[minInf], capacite[minInf], size, etatInf[minInf]);
						#endif 
					}
					else {
						b = 0; flag=1;
						while (flag){
							b++;
					 		if (table_index[*(numf+jMinInf)][etatInfOld[*(par+jMinInf)]+b] > val){
								flag=0;
								b--;
							}	
						}
						tmp = inf_batch(etatInfOld[*(par+jMinInf)], etatInfOld[*(par+jMinInf)]+b, capacite[*(par+jMinInf)], size);
						if (tmp <= etatInfOld[*(par+jMinInf)] + b + 1) etatInf[*(par+jMinInf)] = tmp;
						else etatInf[*(par+jMinInf)] = etatInfOld[*(par+jMinInf)] + b + 1;
						#ifdef DEBUG
							printf("inf=%d\n", etatInf[minInf]);
						#endif
					}
				}
			}
			k++;
		}
		free(etatInfOld);
		free(etatSupOld);
//	}
   }
	
   return (0); 
}

/**
*----------------------------------------------------------------
*
* Function     : dec_batch (monotonic)
*
* Result       : int 
*
* Parameters   : 
*
* Nom       Type      Role
*
* etat	    int []    address of the begining of state vector
*
* source    int      source queue number (-1 if exterior)
*
* dest	    int      destination queue number (-1 if exterior)
*
* size	    int      batch size
*
* Called function :
*
* Description  :
*  
*      Decomposable batch arrival/routing/service
*             
*      
*      
*----------------------------------------------------------------------
*/

int dec_batch(int etat[], int *par, int size){ 

  int source = *par;
  int dest = *(++par);
  int tmp;
  int *  capacite = get_capacite();

  if (source != -1) {
	if (etat[source] - size >= 0) etat[source]-=size;
        else {
		size = etat[source]; 
		etat[source] = 0;
	}
  } 

  if (dest != -1) {
	if (etat[dest] + size <= capacite[dest]) etat[dest]+=size;
  	else etat[dest] = capacite[dest];
  }
  
  return 0;
}



/****************** Non  monotone Pull on departure (load sharing) events **************************************************/

/**
*----------------------------------------------------------------
*
* Function     : TF_Departure_Pull
*
* Result       : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the beginning of state vector
*
* num_ori   int       numero of the origin queue
*
* rand_par   int *    pointer on the array memorizing random numbers for this event
*
*
* Called function : capacite_file, get_neighbors, get_nb_neighbors, 
*
* Description  :
*
*   Transition function of the non monotone, pull on departure, load sharing event.
*   On a task completion (client departure), the origin queue where the event occurs tries 
*   to steal a task on another queue according to index functions specified in index.c.
*   If the queue is empty, no attempt of transfer is done (this is done by the event EmptyQueue_Pull)   
*      
*----------------------------------------------------------------------
*/
int * TF_Departure_Pull(int etat[], int num_ori, int * rand_par){
	
	int i, next_val, max_value, target_num; // for index argmax calculation

	int nb_candidates = get_nb_file(); // for managing a set of potential target
                                     // with the global int[] candidates array
	
	int rand_candidate; // a randomly choosen candidate


	if(etat[num_ori] > 0){ // event application condition
	
		etat[num_ori] -= 1; // end of service on origin
		
		/* allocation of data structures if never allocated by some other event before*/
		if(candidates == NULL)
			candidates = (int *)malloc(get_nb_file() * sizeof(int));
	
		if(priority == NULL)
			priority = (int *)malloc(get_nb_file() * sizeof(int));
		/******************************************************************************/
		
		/* initialization of random parameter*/
		if(rand_par[0] == NEW){
			for(i=0;i<get_prob_limit();i++){
				rand_par[i] = random();
			}
		}
		/**********************************************************************/
	
	
		for(i=0;i<get_nb_file();i++){
			priority[i] = INFINITY;
		}

		for(i=0;i<nb_candidates;i++){
			candidates[i] = i;
		}

		nb_candidates--;
		candidates[num_ori] = candidates[nb_candidates]; // num_ori is removed from the set of candidates
	
		i = 0;
		while(i<get_prob_limit() && nb_candidates >= 0){
		
			rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
			priority[candidates[rand_candidate]] = i;
		
			nb_candidates--;
			candidates[rand_candidate] = candidates[nb_candidates];
			
			i++;
		}


		/* calculation of the target for a task theft by index functions (index.c)*/
		target_num = 0;
		max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etat[0], priority);
		
		for(i=1;i<get_nb_file();i++){
			
			next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etat[i], priority);

			if(next_val > max_value){
				max_value = next_val;
				target_num = i;
			}
		}

		/**************************************************/

		/* task theft if the target is not the origin */ 
		if(target_num != num_ori){
			etat[target_num] -= 1;
			etat[num_ori] += 1;
		}
	}

	return etat;
	
}

/**
*----------------------------------------------------------------
*
* Function     : ENV_Departure_Pull
*
* Result     : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etatInf      int []    adress of the begining of vector etatInf
*
* etatSup      int []    adress of the begining of vector etatSup
* event		int		  event
*
* fonctions appelees : 
* Description  :
* 
*    envelope of the non-monotone event pull on departure, load sharing event 
*    (see Busic, Gaujal, Vincent;Perfect Simulation and Non-monotone Markovian Systems. 
*    On a task completion (client departure), the origin queue where the event occurs tries 
*    to steal a task on another queue according to index functions specified in index.c.
*    If the queue is empty, no attempt of transfer is done (this is done by the event EmptyQueue_Pull)
* 
*     
*      
*----------------------------------------------------------------------
*/
int ENV_Departure_Pull(int etatInf[], int etatSup[], int num_ori, int * rand_par){
	
	int i, next_val, max_value; // for index argmax calculation
	int target_num_Sup; // the target of the theft for etatSup
	int target_num_NearInf; // the target of the theft for the state where all value are equal
	                       // to etatInf except for the origin queue

	int nb_candidates = get_nb_file(); // for managing a set of potential target
                                     // with the global int[] candidates array
	int rand_candidate;

	if(etatSup[num_ori] > 0 && etatInf[num_ori] == 0){ // envelope case

		etatSup[num_ori] -= 1; // end of service on origin

		/* allocation of data structures if never allocated by some other event before*/
		if(candidates == NULL)
			candidates = (int *)malloc(get_nb_file() * sizeof(int));
	
		if(priority == NULL)
			priority = (int *)malloc(get_nb_file() * sizeof(int));
		/******************************************************************************/
		
		/* initialization of random parameter*/
		if(rand_par[0] == NEW){
			for(i=0;i<get_prob_limit();i++){
				rand_par[i] = random();
			}
			
		}
		/************************************/
	
		for(i=0;i<get_nb_file();i++){
			priority[i] = INFINITY;
		}

		for(i=0;i<nb_candidates;i++){
			candidates[i] = i;
		}

		nb_candidates--;
		candidates[num_ori] = candidates[nb_candidates];
	
		i = 0;
		while(i<get_prob_limit() && nb_candidates >= 0){
		
			rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
			priority[candidates[rand_candidate]] = i;
		
			nb_candidates--;
			candidates[rand_candidate] = candidates[nb_candidates];
		
			i++;
		}

		/* calculation of the target for a task theft by index functions (index.c)*/
		target_num_Sup = 0;
		max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etatSup[0], priority);
		
		for(i=1;i<get_nb_file();i++){
			
			next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etatSup[i], priority);

			if(next_val > max_value){
				max_value = next_val;
				target_num_Sup = i;
			}
		}
		
		
		if(target_num_Sup != num_ori){
			etatSup[num_ori] += 1; // like if a task had been stolen from target_num_Sup 
			// uppper envelope let etatSup[target_num_Sup] unchanged
			 
		}

		/* calculation of the target for a task theft by index functions (index.c)*/
		target_num_NearInf = 0;
		if(num_ori == 0)
			max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), 1, priority);
		else
			max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etatInf[0], priority);
		
		for(i=1;i<get_nb_file();i++){
			
			if(num_ori == i)
				next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), 1, priority);
			else
				next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etatInf[i], priority);
			
			if(next_val > max_value){
				max_value = next_val;
				target_num_NearInf = i;
			}
		}

		if(target_num_NearInf != num_ori){
			etatInf[target_num_NearInf] -= 1; // lower envelope
		}

	}
	else{ // normal case

		TF_Departure_Pull(etatSup, num_ori, rand_par);
		TF_Departure_Pull(etatInf, num_ori, rand_par);
	}
	
	return 0;

}

/**
*----------------------------------------------------------------
*
* Function     : EmptyQueue_Pull
*
* Result     : int
*
* Parameters   : 
*
* Name       Type      Role
*
* etatInf      int []    adress of the begining of vector etatInf
*
* etatSup      int []    adress of the begining of vector etatSup
* event		int		  event
*
* fonctions appelees : 
* Description  :
* 
*    Monotone event associated to TF_Departure_Pull() and ENV_Departure_Pull() 
*    to cover the case of pull control when the queue is empty.
*    On a task completion (client departure), if the origin queue is idle, it tries 
*    to steal a task on another queue according to index functions specified in index.c.
*     
*      
*----------------------------------------------------------------------
*/
int * EmptyQueue_Pull(int etat[], int num_ori, int * rand_par){
	
	int i, next_val, max_value, target_num; // for index argmax calculation

	int nb_candidates = get_nb_file();// for managing a set of potential target
                                     // with the global int[] candidates array
	int rand_candidate; // a randomly choosen candidate

	if(etat[num_ori] == 0){ // event application condition
		
		/* allocation of data structures if never allocated by some other event before*/		
		if(candidates == NULL)
			candidates = (int *)malloc(get_nb_file() * sizeof(int));
	
		if(priority == NULL)
			priority = (int *)malloc(get_nb_file() * sizeof(int));
		/******************************************************************************/

		/* initialization of random parameter */
		if(rand_par[0] == NEW){
			for(i=0;i<get_prob_limit();i++){
				rand_par[i] = random();
			}
		}
		/**************************************/
	

		for(i=0;i<get_nb_file();i++){
			priority[i] = INFINITY;
		}
		for(i=0;i<nb_candidates;i++){
			candidates[i] = i;
		}

		nb_candidates--;
		candidates[num_ori] = candidates[nb_candidates];
	
		i = 0;
		while(i<get_prob_limit() && nb_candidates >= 0){
		
			rand_candidate = (int)((double)rand_par[i]/(double)RAND_MAX * nb_candidates);
			priority[candidates[rand_candidate]] = i;
		
			nb_candidates--;
			candidates[rand_candidate] = candidates[nb_candidates];
			
			i++;
		}


		/* calculation of the target for a task theft by index functions (index.c)*/		
		target_num = 0;
		max_value = (*ptr_ws_index)(num_ori, 0, get_node_dist(num_ori, 0), etat[0], priority);
		for(i=1;i<get_nb_file();i++){
			
			next_val = (*ptr_ws_index)(num_ori, i, get_node_dist(num_ori, i), etat[i], priority);

			if(next_val > max_value){
				max_value = next_val;
				target_num = i;
			}
		}
		/************************************************************************/
		
		if(target_num != num_ori){
			etat[target_num] -= 1;
			etat[num_ori] += 1;
		}
	}

	return etat;
	
}



int * TQN_phase1_service(int etat[], int par[]){
	
	int queue1 = par[0];
	int phase = par[1];
	int queue2 = par[2];

	if(etat[phase] == PHASE_1 && etat[queue1] > 0){
		etat[phase] = PHASE_2;
		/*etat[queue1] -= 1;*/
	}
	

	return etat;
}

int * TF_TQN_phase1_service_skip_phase2(int etat[], int par[]){

	int queue1 = par[0];
	int phase = par[1];
	int queue2 = par[2];

	//if(etat[phase] == PHASE_1 && etat[queue1] > 0){ // Overflow
	if(etat[phase] == PHASE_1 && etat[queue1] > 0 && etat[queue2] < capacite_file(queue2)){ // Blocking
		etat[queue1] -= 1;
		//if(etat[queue2] < capacite_file(queue2)) // Overflow
		etat[queue2] += 1;
	}

	
	return etat;
}

int ENV_TQN_phase1_service_skip_phase2(int etatInf[], int etatSup[], int par[]){

	int queue1 = par[0];
	int phase = par[1];
	int queue2 = par[2];
	
	//	if(etatSup[phase] == PHASE_2 && etatInf[phase] == PHASE_1 && etatInf[queue1] > 0 && etatInf[queue2] < capacite_file(queue2)){ 

	if(etatSup[phase] == PHASE_2 && etatInf[phase] == PHASE_1){

		if(etatSup[queue1] > 0 &&  
			 etatSup[queue2] < capacite_file(queue2)) // there exists a non monotone case near etatSup
			{ 
				/* Sup envelope */
				etatSup[queue2] += 1;
				/****************/
			}
		else{
			TF_TQN_phase1_service_skip_phase2(etatSup, par);
		}
		
		
		if(etatInf[queue1] > 0 &&
			 etatInf[queue2] < capacite_file(queue2)) // there exists a non monotone case near etatInf
			{
				/* Inf envelope */
				etatInf[queue1] -= 1; 
				// etatInf[queue2] += 1; is not applied
				/*****************/
			}
		else{
			TF_TQN_phase1_service_skip_phase2(etatInf, par);
		}

	}else{
		TF_TQN_phase1_service_skip_phase2(etatInf, par);
		TF_TQN_phase1_service_skip_phase2(etatSup, par);
	}

	return 0;
}


int * TF_TQN_phase2_service(int etat[], int par[]){

	int queue1 = par[0];
	int phase = par[1];
	int queue2 = par[2];

	if(etat[phase] == PHASE_2 && etat[queue2] < capacite_file(queue2)){
		etat[phase] = PHASE_1;
		
		if(etat[queue1] <= 0){
			printf("problème !\n"); 
			exit(0);
		}		
		etat[queue1] -= 1;

		//if(etat[queue2] < capacite_file(queue2)) // Overflow	
		etat[queue2] += 1;
	}


	return etat;
}

int ENV_TQN_phase2_service(int etatInf[], int etatSup[], int par[]){

	int queue1 = par[0];
	int phase = par[1];
	int queue2 = par[2];
	
	//if(etatSup[phase] == 2 && etatInf[phase] == 1 && etatInf[queue1] > 0){ // non monotone case
	//if(etatSup[phase] == PHASE_2 && etatInf[phase] == PHASE_1 && etatSup[queue2] < capacite_file(queue2)){ // non monotone case
	if(etatSup[phase] == PHASE_2 && etatInf[phase] == PHASE_1){

		if(etatSup[queue2] < capacite_file(queue2)){ // there exists a non monotone case near etatSup
			/* Sup envelope */
			etatSup[phase] = PHASE_1;
			etatSup[queue2] += 1; 
			//etatSup[queue1] -= 1; is not applied
			/****************/
		}
		else{
			TF_TQN_phase2_service(etatSup, par);
		}


		if(etatInf[queue1] > 0 && etatInf[queue2] < capacite_file(queue2)){ // there exists a non monotone case near etatSup
			/* Inf envelope */
			etatInf[queue1] -= 1; 
			/****************/
		}
		else{
			TF_TQN_phase2_service(etatInf, par);
		}

	}else{
		TF_TQN_phase2_service(etatInf, par);
		TF_TQN_phase2_service(etatSup, par);
	}

	return 0;
}



/**
*----------------------------------------------------------------
*
* Function     : transition_interface
*
* Result     : int *
*
* Parameters   : 
*
* Name       Type      Role
*
* etat      int []    adress of the begining of vector etat
*
* event		int		  event
*
* fonctions appelees : get_type_evt,arrivee_ext,sortie_ext,routage_rejet,routage_blocage,routage_debord_bloc,routage_debord_rejet
*
* Description  :
* 
*      en fonction des parametres route vers la fonction de transition adhoc
*      et met a jour le vecteur d'etat pour les files correspondantes
*     
*      
*----------------------------------------------------------------------
*/

void transition_interface(etat,evenement,parametre)
     int etat[];
     int evenement;
		 int* parametre;
{
	int **tab;
	int nbdest; 
	int nbtarget;
	static int *par = NULL;
	static int *numf = NULL;
	int type_evt;
	int nb_ori; 
	type_evt=get_type_evt(evenement);
	nbdest=get_nbr_file_evt(evenement);
   
	int m;
	int batch_size;
   
	par=malloc(nbdest*sizeof(int));
   
	for(m=0;m<nbdest;m++){
		*(par+m)= get_param(evenement,m);
	}
   
	//printf(" Evt: %d \n", evenement);
	switch(type_evt){   
	case 1: /* sortie externe */
		//etat[*par] = sortie_ext(etat,*par);
		sortie_ext(etat,*par); 
		break;

	case 2: /* arrivee externe */
		/* caracteristique 1er parametre numero de file = -1 dernier parametre numero de file = -1 */
		arrivee_ext_deb_rejet(etat,nbdest,par); 
		break;

	case 3: /* When a client leave a Multi-server network */
		Depart_multi_serveur(etat, nbdest, par);
		break;

	case 4: /* Join the shortest queue arrivee avec rejet */
		JSQ_rejet(etat,nbdest,par);
		break;
  
	case 5: /* Index avec rejet */
		numf=malloc(nbdest*sizeof(int));
		for(m=0;m<nbdest;m++){
			*(numf+m)= get_numf(evenement,m);
		}
		Arrivee_rejet_index(etat, nbdest, par, numf);
		free(numf);
		break;
  
	case 6: /* routage vers n files avec debordement avec rejet */
		/* caracteristique 1er parametre numero de file != avant dernier param numero de file  et dernier parametre numero de file == -1 */
		//printf("ROAGE nbdest : %d *par %d \n",nbdest,*par);
		routage_nfile_rejet(etat,nbdest,par);
		break;
  
	case 7: /* routage vers n files avec debordement avec blocage */
		/* caracteristique 1er parametre numero de file == avant dernier param numero de file et dernier parametre numero de file == -1 */
		routage_nfile_bloc(etat,nbdest,par);
		break;
  
	case 8: /* Arrive dans un multi serveur avec index */
		numf=malloc(nbdest*sizeof(int));
		for(m=0;m<nbdest;m++){
			*(numf+m)= get_numf(evenement,m);
		}
		   
		tab = get_multi_server(evenement);	
		Arrivee_rejet_multi_index(etat, nbdest, par, numf, tab);
		free(numf);
		break;
  
	case 9: /* depart call center */
		nb_ori= get_nb_ori(evenement);
		Depart_call_center(etat, nbdest, par, nb_ori);
		break;

	case 22:  /*decomposable batch*/
		batch_size = get_batch_size(evenement);
		dec_batch(etat, par, batch_size);
		break;
		
	case 50: 
		Independent_Pull(etat, *par, parametre);
		break;

	case 51:
		Independent_Push(etat, *par, parametre);
		break;
		 
	case 52:
		Arrival_Push(etat, *par, parametre);
		break;
		 
	case 54:
		LTM_arrival(etat, *par, parametre);
		break;

	case 55:
		LTM_departure(etat, *par, parametre);
		break;
		 
	default : /*On a reconnu aucun type */
		printf("\nerror in data description file\ndetail : unknown event type %d\n", type_evt);
		printf("Option -e may be used if the system is non monontone (envelope technique)\n");
		exit(0);
	}
	free(par);
	 
	//printf("Nouvel etat =: %d \n",etat[0]); 
}


void transition_interface_envelope(etatInf,etatSup,evenement,parametre)
     int etatInf[], etatSup[];
     int evenement;
		 int * parametre;
{
	int **tab;
	int nbdest; 
	static int *par = NULL;
	static int *numf = NULL;
	int type_evt;
	int nb_ori; 
	type_evt=get_type_evt(evenement);
	nbdest=get_nbr_file_evt(evenement);
	int m;
	int batch_size;
   
	par=malloc(nbdest*sizeof(int));
   
	for(m=0;m<nbdest;m++){
		*(par+m)= get_param(evenement,m);
	}
   
   
	//printf(" Evt: %d \n", evenement);
	switch(type_evt){   
	case 1: /* sortie externe */
#ifdef DEBUG
		if (etatSup!=NULL) printf("Service (%ld): (%ld, %ld) -> ", *par, etatInf[*par], etatSup[*par]);
		else printf("Service (%ld): %ld -> ", *par, etatInf[*par]);
#endif

		etatInf[*par] = sortie_ext(etatInf,*par);
		if (etatSup!=NULL) etatSup[*par] = sortie_ext(etatSup,*par);

#ifdef DEBUG
		if (etatSup!=NULL) printf("(%ld, %ld)\n", etatInf[*par], etatSup[*par]); 
		else printf("%ld\n", etatInf[*par]); 
#endif

		break;

	case 2: /* arrivee externe */
		/* caracteristique 1er parametre numero de file = -1 dernier parametre numero de file = -1 */
		arrivee_ext_deb_rejet(etatInf,nbdest,par); 
		if (etatSup!=NULL) arrivee_ext_deb_rejet(etatSup,nbdest,par);

		break;

	case 3: /* When a client leaves a Multi-server network */
		Depart_multi_serveur(etatInf, nbdest, par);
		if (etatSup!=NULL) Depart_multi_serveur(etatSup, nbdest, par);
		break;

	case 4: /* Join the shortest queue arrivee avec rejet */
		JSQ_rejet(etatInf,nbdest,par);
		if (etatSup!=NULL) JSQ_rejet(etatSup,nbdest,par);
		break;
  
	case 5: /* Index avec rejet */
		numf=malloc(nbdest*sizeof(int));
		for(m=0;m<nbdest;m++){
			*(numf+m)= get_numf(evenement,m);
		}
		Arrivee_rejet_index(etatInf, nbdest, par, numf);
		if (etatSup!=NULL) Arrivee_rejet_index(etatSup, nbdest, par, numf);
		free(numf);
		break;
  
	case 6: /* routage vers n files avec debordement avec rejet */
		/* caracteristique 1er parametre numero de file != avant dernier param numero de file  et dernier parametre numero de file == -1 */
		//printf("ROUTAGE nbdest : %d *par %d \n",nbdest,*par);
		routage_nfile_rejet(etatInf,nbdest,par);
		if (etatSup!=NULL) routage_nfile_rejet(etatSup,nbdest,par);

		break;
  
	case 7: /* routage vers n files avec debordement avec blocage */
		/* caracteristique 1er parametre numero de file == avant dernier param numero de file et dernier parametre numero de file == -1 */
		routage_nfile_bloc(etatInf,nbdest,par);
		if (etatSup!=NULL) routage_nfile_bloc(etatSup,nbdest,par);
		break;
  
	case 8: /* Arrive dans un multi serveur avec index */
		numf=malloc(nbdest*sizeof(int));
		for(m=0;m<nbdest;m++){
			*(numf+m)= get_numf(evenement,m);
		}
		   
		tab = get_multi_server(evenement);	
		Arrivee_rejet_multi_index(etatInf, nbdest, par, numf, tab);
		if (etatSup!=NULL) Arrivee_rejet_multi_index(etatSup, nbdest, par, numf, tab);
		free(numf);
		break;
  
	case 9: /* depart call center */
		nb_ori= get_nb_ori(evenement);
		Depart_call_center(etatInf, nbdest, par, nb_ori);
		if (etatSup!=NULL) Depart_call_center(etatSup, nbdest, par, nb_ori);
		break;

	case 120: /*negative customer*/
		if (nbdest!=2) printf("Error: negative customer event associated with %d queues.\n", nbdest);
		negative_customer(etatInf, etatSup, par);
		break;

	case 121: /*batch*/
		batch_size = get_batch_size(evenement);  

#ifdef DEBUG
		if (etatSup!=NULL) printf("Batch arrival (%ld, +%ld): (%ld, %ld) -> ", *(par+1), batch_size, etatInf[*(par+1)], etatSup[*(par+1)]);
		else printf("Batch arrival (%ld, -%ld): %ld -> ", *(par+1), batch_size, etatInf[*par]);
#endif

		batch(etatInf, etatSup, par, batch_size);
		   
#ifdef DEBUG
		if (etatSup!=NULL) printf("(%ld, %ld)\n", etatInf[*(par+1)], etatSup[*(par+1)]);
		else printf("%ld\n", etatInf[*(par+1)]);
#endif

		break;

	case 22: /*decomposable batch*/
		batch_size = get_batch_size(evenement);
		dec_batch(etatInf, par, batch_size);
		if (etatSup!=NULL) dec_batch(etatSup, par, batch_size);
		break;

	case 123: /*batch index routing*/
		batch_size = get_batch_size(evenement);
		numf=malloc(nbdest*sizeof(int));
		for(m=1;m<nbdest;m++){
			*(numf+m)= get_numf(evenement,m);
		}
		
#ifdef DEBUG
		printf("Before - Batch index routing (size = %ld): ", batch_size);
		if (etatSup==NULL) imprime_etat(etatInf);
		else {
			printf("Inf:"); 
			imprime_etat(etatInf); 
			printf(", Sup:"); 
			imprime_etat(etatSup);
		}
		printf("\n"); 
#endif

		batch_index_routing(etatInf, etatSup, nbdest, par, numf, batch_size);

#ifdef DEBUG
		printf("After - Batch index routing (size = %ld): ", batch_size);
		if (etatSup==NULL) imprime_etat(etatInf);
		else {
			printf("Inf:"); 
			imprime_etat(etatInf); 
			printf(", Sup:"); 
			imprime_etat(etatSup);
		}
		printf("\n\n"); 
#endif
		break;

	case 50: 
		Independent_Pull(etatInf, *par, parametre);
		if (etatSup!=NULL) Independent_Pull(etatSup, *par, parametre);
		break;

	case 51:
		Independent_Push(etatInf, *par, parametre);
		if (etatSup!=NULL) Independent_Push(etatSup, *par, parametre);
		break;

	case 52:
		Arrival_Push(etatInf, *par, parametre);
		if (etatSup!=NULL) Arrival_Push(etatSup, *par, parametre);
		break;

	case 54:
		LTM_arrival(etatInf, *par, parametre);
		if (etatSup!=NULL) LTM_arrival(etatSup, *par, parametre);
		break;

	case 55:
		LTM_departure(etatInf, *par, parametre);
		if (etatSup!=NULL) LTM_departure(etatSup, *par, parametre);
		break;

		/******************************************/
		/**** NON MONOTONE LOAD SHARING SYSTEM ****/

	case 153:
		 
		if(etatSup == NULL){
			TF_Departure_Pull(etatInf, *par, parametre);
		}
		else{
			ENV_Departure_Pull(etatInf, etatSup, *par, parametre);
		}

		break;

	case 56:
		EmptyQueue_Pull(etatInf, *par, parametre);
		if (etatSup!=NULL) EmptyQueue_Pull(etatSup, *par, parametre);
		break;

		/********************************************************************************/
		/**** NON MONOTONE TANDEM QUEUING NETWORK (COXIAN SERVICE WITH TWO PHASE) *******/

	 case 31:
		 if(nbdest != 3)
			 printf("\nError : event type 31 require three queues (queue1,phase,queue2)\n");
		 
		 TQN_phase1_service(etatInf, par);
		 if (etatSup!=NULL) TQN_phase1_service(etatSup, par);
		 break;
		 
	 case 132:
		 if(nbdest != 3)
			 printf("\nError : event type 31 require three queues (queue1,phase,queue2)\n");

		 if(etatSup == NULL){
			 TF_TQN_phase1_service_skip_phase2(etatInf, par);
		 }
		 else{
			 ENV_TQN_phase1_service_skip_phase2(etatInf, etatSup, par);
		 }
		 
		 break;
		 
	 case 133:
		 if(nbdest != 3)
			 printf("\nError : event type 31 require three queues (queue1,phase,queue2)\n");

		 /* TQN_phase2_service(etatInf); */
/* 		 if (etatSup!=NULL) TQN_phase2_service(etatSup); */
		 if(etatSup == NULL){
			 TF_TQN_phase2_service(etatInf, par);
		 }
		 else{
			 ENV_TQN_phase2_service(etatInf, etatSup, par);
		 }

		 break;
		
	default : /*On a reconnu aucun type */
		printf("\nerror in data description file\ndetail : unknown event type %d\n", type_evt);

		exit(0);
	}
	free(par);
  
	//printf("Nouvel etat =: %d \n",etat[0]); 
}

 
