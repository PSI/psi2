 /* Copyright(C) (2004) (ID - IMAG) <Bernard.Tanzi@imag.fr> */

/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */



/*! \file lect_model.c
 * \brief Functions of lect_model.c . 
 * \author - Bernard.Tanzi@imag.fr
 * \author - Jean-Marc.Vincent@imag.fr
 * \author - Jerome.Vienne@imag.fr
 *
 * \date Juillet 21 , 2004
 */



/*
*------------------------------------------------------------------
* Fichier      : lect_model.c
* Langage      : C ANSI
* Auteur       : Bernard Tanzi,Jean-Marc Vincent
* Creation     : 20 Juillet 2004
*--------------------------------------------------------------------
* Description  :
*          Programme permettant la lecture des donn�s d utilsateurs 
* ainsi que la creation dynamique a partir des donnees lues des 
* structures de donnees qui seront utiliser par l'application 
*  
*-------------------------------------------------------------------------------
*/

/********************************************************/
/********** INCLUSION DES INTERFACES SYSTEMES   *********/
/********************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dlfcn.h>
#include <getopt.h>

#include <errno.h>

/********************************************************/
/******** INCLUSION DES INTERFACES APPLICATIVES *********/
/********************************************************/

#include "lect_model.h"
#include "data.h"

/*********************************************************/
/*****  CONSTANTES, MACROS et TYPES LOCAUX  *************/
/********************************************************/
//#define DEBUG 1 

#ifdef __linux__
	#define _LINUX_
#endif

#ifdef __APPLE__
  #define _APPLE_
#endif
/**********************************************************/
/********* DEFINITIONS DES VARIABLES LOCALES **************/
/**********************************************************/


/********************************************************/
/** DECLARATION VARIABLES GLOBALES A L'APPLICATION  *****/
/********************************************************/

/* donn�s lues du fichier utilisateur */
 
/**  pointer on the maximal length on a trajectory */
double *lg_traj_max; 

/** pointer on the table of trajectories */ 
int *trajectoire;   

/** pointer on the table of trajectories parameter */ 
int **trajectoire_par;
 
/** pointer on the number of sample */   
int *nbr_echantillon; 

/** pointer on the number of antithetic variable */ 
int *nbr_VA;

/** pointer on the number of index function*/ 
int *nbr_fct;


/** pointer on seed  */   
int *germe_gener; 
 

/** pointer on file number */
int *nbr_file;   

/**** tableaux dynamiques de longueur le nombre de files ****/

/** pointer on queue capacity */
int *capfile;  

/** pointer on etat_init_sup */
int *etat_init_supU;

/** pointeur on etat_init_inf */ 
int *etat_init_infU;


/** pointer etat inf */ 
int *etat_inf;
 
/** pointer etat sup */     
int *etat_sup;  

/** split threshold */
int split_threshold = 0;
/** is splitted */
int is_splitted;
/** number of splitted states */
int nb_etats_split;
/** splitted states */
int **etats_split;

/** pointer on token number of a queue */    
int *nb_jeton; 
/** pointer coupling of a queue  */     
int **couplage;
      
/** pointer on event number */
int *nb_evt;   

/** Use to know if we use dynamic lib or not, use = 1, no use = 0 */ 
int dym_on;

/** Use to know if we use dynamic lib or not, use = 1, no use = 0 */ 
int dym_ws_on;

/** Use to know if we use index or not, use = 1, no use = 0 */ 
int idx_use;

/*** tableaux dynamiques de longueur le nombre d'evnements ***/

/** pointer on events rate*/
double *P;
/** pointer on thresholds */     
double *R;
/** pointer on alternative events */      
int *A;       

/** pointer on structure event */
struct st_evt *evt;  


/** Pointer on the list of servers for multi servers*/
struct st_multi *multis;

/** Pointer on the table which contain index function values .*/
double **table_index;

int is_indexed;   

void *lib_handle;
void *lib_handle_ws;
int (*ptrfunc)(int * etat_inf, int * etat_sup);
int (*ptraff)(int * etat);


/*****************************************************************************************/
/*******************   LOAD SHARING DATA *************************************************/
/*****************************************************************************************/


/** [LOAD SHARING] pointer on the hierarchy tree root*/
struct st_hier_node * hier_root;

/** [LOAD SHARING] pointer on the queue_number<-->tree_node correspondance array*/
struct st_hier_node ** node_queue;

/** [LOAD SHARING] matrix of distance between two queues : node_dist[i][j] = distance between i and j*/
int ** node_dist;

/** [LOAD SHARING] array containing for each node, the neighbors of the node*/
struct st_neighbors * node_neighbors;

/** [LOAD SHARING] mode which tell if the user give a hierarchy Tree or a Matrix to give the node_dist matrix, Tree = 0, Matrix = 1*/
int ws_mode; 

/** [LOAD SHARING] pointer on the prob_limit value which give the number of concurent nodes in the index calculation */ 
int * prob_limit;

/** [LOAD SHARING] link to the dynamic library which contains the index function for the index calculation*/
double (*ptr_ws_index)(int num_ori, int num_cible, int dist, int ch_cible);
 

/**
*----------------------------------------------------------------
*
* Function     : lire_data_FU
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* nomf  char *    pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -i option, create data structures (with malloc) 
*		and read index file if there is one.
*		return 1 if there is a problem or 0 if all is ok
*
*  
*----------------------------------------------------------------------
*/


int lire_data_FU(char *nomf){

  
  int i, j, k, error;
  int cpt;
  int  nb_file_multi;
  char * var = malloc(50*sizeof(char));   // utilise qd on lit la liste des files
  FILE *fd,*fd2;      /* pointeur sur descripteur du fichier */
  FILE *fd3 = NULL;
  int *value_number=NULL;
  int value; 
	int id_ev;
 
  char file_index[50];
	char file_ws[50];
  char * commande = (char *)malloc(BUFSIZ*sizeof(char));
  

  //On etire toute les lignes qui commence par un #
  sprintf(commande, "grep -v \"#\" %s",nomf );
  	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture de fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
    }
  else{ 
      printf("Reading of the data file : %s\n", nomf);
      /* lecture variable nombre de files ****/  
      nbr_file=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_file);  
          
#ifdef DEBUG  /* pour affichage nombre de files *****/
      printf("nombre de file : %d\n",*nbr_file);
#endif
      
      capfile=(int *)malloc((*nbr_file) * sizeof(int)); 
	  /* alloue �chaque pointeur sur entier une adresse memoire ******************/
      for(j=0;j<*nbr_file;j++){ 
		fscanf(fd,"%d",capfile+j);  /* stocke a cette @ la valeur lue ****/

 #ifdef DEBUG /* pour affichage capacite file ****/
	printf("capacite  file %d : %d\n",j,*(capfile+j));
#endif
      
      }
      /* lecture etat_inf initial et stockage dans tableau allou�dynamiquement ***/      
      etat_init_infU=(int *)malloc((*nbr_file)*sizeof(int));
	  
	for(j=0;j<*nbr_file;j++){ 
		fscanf(fd,"%d",etat_init_infU+j);
#ifdef DEBUG
      //printf("mini file %d  : %d\n",j,etat_init_infU[j]);
#endif
      }
     /* lecture etat_sup initial et stockage dans tableau allou�dynamiquement ***/   
      etat_init_supU=(int *)malloc((*nbr_file)*sizeof(int));
	  
      for(j=0;j<*nbr_file;j++){
		fscanf(fd,"%d",etat_init_supU+j);
#ifdef DEBUG
//      printf("maxi file %d  : %d\n",j,*etat_init_supU[j]);
#endif   
      }
      /* creation dynamique d'un tableau pour stocker les etats inf  **/
	etat_inf=(int *)malloc((*nbr_file)*sizeof(int));
	  
	  
      /* initialisation de ce tableau avec les valeurs initiales ***/
	for(j=0;j<*nbr_file;j++){
		etat_inf[j]=etat_init_infU[j];
      	} 

      /* creation dynamique d'un tableau pour stocker les etats sup  **/
	etat_sup=(int *)malloc((*nbr_file)*sizeof(int));
	  
	  /* initilaisation de ce tableau avec les valeurs initiales ***/
	for(j=0;j<*nbr_file;j++){
			etat_sup[j]=etat_init_supU[j];
	}
      

      /* creation dynamique d'un tableau pour stocker les etats apres le split  **/
      if (split_threshold > 0) {
	etats_split = (int **)malloc(split_threshold*sizeof(int *));
        for (j=0; j<split_threshold; j++) etats_split[j] = (int *)calloc((*nbr_file), sizeof(int));
      }

      /* creation dynamique d'un tableau pour stocker l'etat des files **/
      nb_jeton=(int *)malloc((*nbr_file)*sizeof(int));
     
      /* creation dynamique d'un tableau pour stocker l'etat du couplage des files **/
      couplage=(int**)calloc(*nbr_VA, sizeof(int));
	  for (i=0; i< *nbr_VA; i++){
		couplage[i]=(int *)calloc((*nbr_file),sizeof(int));
      }
	  
      /* lecture du nombre d'evenement **/
      nb_evt=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nb_evt);  
      
	  /////////////////////////////////////////////////////////////////////////////
	  // Lecture du nom du fichier 
	  fscanf(fd,"%s",var);
	  if(strcmp("File:", var)){
		printf("Error: \"File:\" waited, we have %s\n", var);
	  } else {
	  
			fscanf(fd,"%s", file_index);
			
		if(!strcmp("N", file_index)){
			printf("There is no index file\n");
			is_indexed=0;
		} else {
			printf("Reading of the index file : %s\n", file_index);
	        is_indexed=1;
			
			sprintf(commande, "grep -v \"#\" %s",file_index );
	
				if ((fd2 = popen(commande,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",nomf);
					perror("Le Pb est : ");
				return(1);
			} else {
				//On recupere le nombre de fonction
			 	nbr_fct=(int *)malloc(sizeof(int));	
				
				fscanf(fd2,"%d", nbr_fct);
				
				
				value_number=(int *)malloc((*nbr_fct)*sizeof(int));
				// On recupère le nombre de valeur pour chaque file
				for(i=0;i<*nbr_fct; i++){
					fscanf(fd2,"%d\n", (value_number+i));
					//printf("Nombre de valeur: %d\n", *(value_number+i));
				} 

				// Creation of the table
				printf("Creation of the index function table......\n");
				table_index = (double **) calloc(*nbr_fct, sizeof(double));
				if (table_index != NULL){
					error=0;
					cpt =0;
			// On rempli la table d'index par l'index des files dest
			for(i=0; i<*nbr_fct; i++){
					table_index[i] = (double *) calloc(*(value_number+i), sizeof(double));
					if (table_index[i] == NULL){
						error = 1;
					}
					cpt++;
				}
				
				if (error){
					while (cpt){
						cpt--;
						free(table_index[cpt]);
					}
					perror("Error not enough memory ");
					return(1);
				}
			} else {
				printf("Not enough memory\n");
			}
			
			// Remplissage du tableau
			
			for(i=0; i<*nbr_fct; i++){
				
				for (j=0; j<*(value_number+i); j++){
					
					fscanf(fd2, "%d", &value);
					
					if(value != j){
						printf("Error: there is a problem on the line where there is the number of the index function, it should be %d\n", j);
					}
					fscanf(fd2, "%lf", &table_index[i][j]);
					//printf("On a mis %lf en (%d, %d)\n",table_index[i][j], i, j);
				}
			}
				
		}
		pclose(fd2);
	  	printf("Reading of the index file %s over\n", file_index);
	}
	}
	 
		//////////////////////////////////////////////////////////////////////////
		/** reading load sharing configuration file if exist                  */
		
		fscanf(fd,"%s",var);
	  if(strcmp("File:", var)){
			printf("Error: \"File:\" waited, we have %s\n", var);
	  } else {
			
			fscanf(fd,"%s", file_ws);
	  
			if(!strcmp("N", file_ws)){
				printf("There is no load sharing file\n");
			} else {
				printf("Reading of the load sharing file : %s\n", file_ws);
				
				sprintf(commande, "grep -v \"#\" %s",file_ws );
				
				if ((fd3 = popen(commande,"r")) == NULL ){
					printf("Pb d'ouverture de fichier '%s' \n",file_ws);
					perror("Le Pb est : ");
					return(1);
				} else {
				
					lire_index_dl(fd3);

					/* reading of the prob limit*/
					fscanf(fd3, "%s", var);
					if(strcmp("Prob-Limit:", var)){
						printf("Error: \"Prob-Limit:\" waited, we have %s\n", var);
					} else {
						
						
						prob_limit = (int *)malloc(sizeof(int));
						fscanf(fd3,"%d",prob_limit);

						if(*prob_limit > *nbr_file)
							printf("Warning : the prob limit is greater than the number of queue\n", var);
					}

					/* reading of the topology */

					fscanf(fd3,"%s",var);
					if(strcmp("Mode:", var)){
						printf("Error: \"Mode:\" waited, we have %s\n", var);
					} else {	
					
						fscanf(fd3,"%s", var);
						if(!strcmp("Tree", var)){

							ws_mode = 0; // ws_mode is Tree
						
							node_dist = (int**)malloc(*nbr_file * sizeof(int*));
							for(i=0;i<*nbr_file;i++)
								node_dist[i] = (int*)malloc((i+1) * sizeof(int));
						
							node_queue = (struct st_hier_node **)malloc(*nbr_file * sizeof(struct st_hier_node *));
						
							lire_hier(fd3);
						}
						else if(!strcmp("Matrix", var)){
						
							ws_mode = 1; // ws_mode is Matrix

							node_dist = (int**)malloc(*nbr_file * sizeof(int*));
							for(i=0;i<*nbr_file;i++)
								node_dist[i] = (int*)malloc(*nbr_file * sizeof(int));
						
						
							for(i=0;i<*nbr_file;i++){
								for(j=0;j<*nbr_file;j++){
									fscanf(fd3,"%d", &node_dist[i][j]);
								}
							}

						}
						else{
							printf("Error: \"Tree\" or \"Matrix\" waited, we have \"%s\"", var);
						}
			
						/* data transformations */
						//node_dist_to_node_neighbors();
					}

				}
			
			pclose(fd3);
			printf("Reading of the load sharing file %s over\n", file_ws);
			}
		}
		
		
		/////////////////////////////////////////////////////////////////////////////     
 
      /* creation dynamique d'un tableau pour les probabilites des evenements ***/
      
		P=(double *)malloc((*nb_evt)*sizeof(double));

		//		if(P==NULL)printf("P = NULL"); else printf("P != NULL youpi");
		
      /* creation dynamique d'un tableau pour les seuils ***/
      R=(double *)malloc((*nb_evt)*sizeof(double));

			if(R==NULL)printf("R = NULL");

      /* creation dynamique d'un tableau pour les evenements alternatifs ***/
      A=(int *)malloc((*nb_evt)*sizeof(int));

			if(A==NULL)printf("A = NULL");

      /* pour importer le tableau  des evenements dans une structure ******/
      evt=(struct st_evt *)malloc((*nb_evt)*sizeof(struct st_evt));
      multis=(struct st_multi *)calloc((*nb_evt), sizeof(struct st_multi));
	   
      for(i=0;i<(*nb_evt);i++){
		
	  
		fscanf(fd,"%d %d %lf ",&(evt+i)->id_evt,&(evt+i)->typ_evt,&(evt+i)->lambda);

		if ((evt+i)->typ_evt == 121 || (evt+i)->typ_evt == 22 || (evt+i)->typ_evt == 123){
			// Reading of batch size (events 121, 22, 123)
			fscanf(fd,"%d ",&(evt+i)->batch_size);	
		}	
		else (evt+i)->batch_size=1; 

		fscanf(fd,"%d ", &(evt+i)->nbr_file_evt);

		
		//printf("\n id: %2d       type: %2d    lambda: %f   nbr_file evt:%2d \n",(evt+i)->id_evt,(evt+i)->typ_evt,(evt+i)->lambda,(evt+i)->nbr_file_evt); 
		
		((evt+i)->param_evt)=malloc(((evt+i)->nbr_file_evt)*sizeof(int));
		((evt+i)->num_fct)=malloc(((evt+i)->nbr_file_evt)*sizeof(int));
	    
		
		if ((evt+i)->typ_evt == 5){
			// On est ds le type 5
			fscanf(fd,"%s",var);
			*((evt+i)->param_evt)=atoi(var);
			
			fscanf(fd,"%s",var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			for (j=1;j<((evt+i)->nbr_file_evt);j++){
				
				fscanf(fd," (%d,%d)", ((evt+i)->param_evt+j), ((evt+i)->num_fct+j));
				//printf("J'ai lu: %d et %d\n", *((evt+i)->param_evt+j), *((evt+i)->num_fct+j));
				// on vérifie d'on a assez de valeur de fonction d'index pour chaque fonction
				// Capacité de la file doit être < ou = à Nombre de valeur de la fonction d'index associé
				//printf("Capacité de la file: %d\n", *(capfile+*((evt+i)->param_evt+j)));
				//printf("Nombre de valeur de la fonction d'index: %d\n", *(value_number+*((evt+i)->num_fct+j)));

				if( (*((evt+i)->param_evt+j)) != -1 ){
					//printf("On est diff de -1\n");
					if ( *(capfile+*((evt+i)->param_evt+j)) > (*(value_number+*((evt+i)->num_fct+j)) - 1) ){
					
						printf(" Function index %d is not defined for all the value of the queue %d \n", *((evt+i)->num_fct+j), *((evt+i)->param_evt+j));
						printf("There is %d index values for a queue with a capacity of %d\n", *(value_number+*((evt+i)->num_fct+j)) , *(capfile+*((evt+i)->param_evt+j)) );
					}
				} else {
					if( (*(value_number+*((evt+i)->num_fct+j))) < 1 )
						printf("You need at least 1 value for the exit (-1) on index function");
				}	

			}
			
		}
		
		else if ((evt+i)->typ_evt == 8){
			// On est ds le type 8
			
			fscanf(fd,"%s",var);
			*((evt+i)->param_evt)=atoi(var);
			
			fscanf(fd,"%s",var);
			if(strcmp(":", var)){
				printf("Erreur: On attendait un :\n");
			}
			
			multis[i].queue_list = (int **) calloc((evt+i)->nbr_file_evt, sizeof(int*));
			
			for (j=1;j<((evt+i)->nbr_file_evt);j++){
				
				fscanf(fd," (%d,", &nb_file_multi);
				//printf("Il y a  %d file \n", nb_file_multi);
				
				if( nb_file_multi != -1){
					multis[i].queue_list[j] = (int *) calloc((nb_file_multi+1), sizeof(int));
					multis[i].queue_list[j][0] = nb_file_multi;
				
					for(k=0; k<nb_file_multi; k++){
						
						fscanf(fd,"%d,", &(multis[i].queue_list[j][k+1]));
						//printf("On a la file %d en %d, %d\n", multis[i].queue_list[j][k+1], j, (k+1) );
				
					}
				
					fscanf(fd,"%d)", ((evt+i)->num_fct+j) );
					//printf("La file d'index est: %d \n",*((evt+i)->num_fct+j) );
				}else{
					multis[i].queue_list[j] = (int *) calloc(1, sizeof(int));
					multis[i].queue_list[j][0] = -1;
					
					//printf("On a la file %d en %d, O\n", multis[i].queue_list[j][0], j );
					
					fscanf(fd,"%d)", ((evt+i)->num_fct+j) );
					//printf("La file d'index est %d \n",*((evt+i)->num_fct+j) );
				}
			
			}
		} 
		else if ((evt+i)->typ_evt == 9){	
			// On est ds le type 9 -> call center ^^
			
			
			for (j=0;j<((evt+i)->nbr_file_evt);j++){
				fscanf(fd,"%s",var);
				
				if(strcmp(":", var) ){ 
					*((evt+i)->param_evt+j)=atoi(var);
				}else{
					(evt+i)->num_ori=j;
					//printf("Evt: %d Nombre de file origine: %d \n",i, (evt+i)->num_ori );
					// Ici on a rencontre le caractere :
					j--;
				}
			}

		}else{
			// On n' est pas dans le type 5 ou 8
			for (j=0;j<((evt+i)->nbr_file_evt);j++){
				fscanf(fd,"%s",var);
				
				if(strcmp(":", var) ){ 
					*((evt+i)->param_evt+j)=atoi(var);
				}else{
					// Ici on a rencontre le caractere :
					j--;
				}
			}
		}
    
		
	
	}
  }

	/* creation en memoire du tableau de trajectoires ***/
      trajectoire=(int*)calloc(*lg_traj_max,sizeof(int));
	  
				trajectoire_par=(int**)malloc(*lg_traj_max * sizeof(int*));

				if(prob_limit != NULL){
					
					for(i=0;i<*lg_traj_max;i++){
						//trajectoire_par[i] = (int*)malloc(*capfile * 2 * sizeof(int)); // TO DO bounding
						trajectoire_par[i] = (int*)malloc(*prob_limit * sizeof(int));
						
						for(j=0;j<*prob_limit;j++){
							trajectoire_par[i][j] = -1; 
						}
					}
				}
				
				
				free(commande);
				free(var);
				free(value_number); 
				pclose(fd);
				printf("Reading of the data file : %s over\n", nomf);
				
				return(0);	
}

/**
*----------------------------------------------------------------
*
* Function     : lire_couplage
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* 
*
* Description  :
*
*	Use to open dynamic library
*
*  
*----------------------------------------------------------------------
*/

int lire_couplage(FILE *fd){

	char * var=malloc(50*sizeof(char));   // utilis pour lire le nom du fichier  
char * file_coupl=malloc(100*sizeof(char));
char *chemin3;
char *nfin;  
char *nfout;
char *chficso;
	  
fscanf(fd,"%s",var);
	  
if(strcmp("File:", var)){
	printf("Error: \"File:\" waited, we have %s\n", var);
} else {
	  
	fscanf(fd,"%s", file_coupl);
	 
	if(!strcmp("No", file_coupl)){
		printf("There is no coupling file\n");
		dym_on=0;
	} else {
		dym_on=1;
		printf("Reading of the coupling file : %s\n", file_coupl);
		 /* pour stocker le nom de fichier sans son extension */

		nfin = (char *) malloc(40*sizeof(char));
		nfout = (char *) malloc(40*sizeof(char)); 
		strcpy(nfin,"");
		strcat(nfin,file_coupl);
		strcat(nfin,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfin);
		#endif

		int lgfile = strlen(nfin);

		#ifdef DEBUG
		printf("longueur du fichier %d\n",lgfile);
		#endif

		int i;
		for(i=0;i<(lgfile-1);i++){
			*(nfout+i)=nfin[i];
		}
		strcat(nfout,"\0");
		  
		#ifdef DEBUG
		printf("nom de fichier : %s\n",nfout);
		#endif
	     
		
		chemin3=(char *)malloc(100*sizeof(char));
		strcpy(chemin3,"");
		#ifdef _LINUX_
			/* pour creer la commande systeme ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
			strcat(chemin3,"gcc -c ");
			strcat(chemin3,file_coupl);
			strcat(chemin3,";ld -shared -o ");
			strcat(chemin3,nfout);
			strcat(chemin3,"so ");
			strcat (chemin3,nfout);
			strcat(chemin3,"o ");;
		#endif
		
		#ifdef _APPLE_
			/* pour creer la commande system ("gcc -dynamiclib chemin/test_couplage.c libtest_couplage.dylib ") ; */ 
			strcat(chemin3,"gcc -dynamiclib -flat_namespace -undefined suppress ");
			strcat(chemin3,file_coupl);
			strcat(chemin3," -o lib");
			strcat(chemin3,nfout);
			strcat(chemin3,"dylib ");
		#endif

		#ifdef DEBUG
		printf("commande system %s\n",chemin3);

		printf ("C'est le programme main qui s'execute\n") ;

		printf ("Compilation/creation de la bibliotheque \n") ;
		#endif 
	      
		/* pour executer la commande */
		system(chemin3);
  
		#ifdef DEBUG
		printf ("Ouverture de la bibliotheque partagee\n") ;
		#endif

		chficso = (char *)malloc(30*sizeof(char));

		#ifdef _LINUX_
			strcat(chficso,"./");
			strcat(chficso,nfout);
			strcat(chficso,"so");
		#endif
		
		#ifdef _APPLE_
			strcat(chficso,"lib");
			strcat(chficso,nfout);
			strcat(chficso,"dylib");
		#endif
		
		#ifdef DEBUG
		printf("chemin fichier so %s\n",chficso);
		#endif
	      
		lib_handle = dlopen (chficso, RTLD_LAZY) ;
		if (lib_handle){
			printf("Dynamic library opening: Successful\n");	
		} else {
			printf ("[%s] Unable to open library: %s \n", chficso, dlerror ()) ;
			exit (-1) ;
		}
		  
		#ifdef DEBUG
		printf ("Dynamic library \n") ;
		#endif
		ptrfunc = (void *) dlsym (lib_handle, "egalite_reward") ;
		if (ptrfunc == NULL){
			printf("Loading egalite_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading egalite_reward(): Successful\n");
		}

		ptraff = (void *) dlsym (lib_handle, "affiche_reward") ;
		if (ptraff == NULL){
			printf("Loading affiche_reward(): UnSuccessful, default function use\n");
		} else{
			printf("Loading affiche_reward(): Successful\n");
		}
		
		#ifdef DEBUG
		printf ("Pointeur sur la fonction r�up��\n") ;
		printf ("Appel de la fonction\n") ;
		#endif
		  
		printf ("Coupling function initialisation : OK\n") ; 
		free(chemin3);
		free(chficso);
		free(nfin);
		free(nfout);
	}
	
}		
	free(file_coupl);
	free(var);
	return 0;
}




/**
*----------------------------------------------------------------
*
* Function     : lire_param_FU
*
* Result     : int
*
* Parameters   :
*
* Nom       Type      Role
*
* nomf  char *     pointer on the character strings including the name of the file with his relative access path
*
* Description  :
*
*		read the file of the -p option, create data structures (with malloc)
*
*----------------------------------------------------------------
*/

int lire_param_FU(char *nomf){

  FILE *fd;      /* pointeur sur descripteur de fichier */
  char commande[BUFSIZ];
 
  
  
  sprintf(commande, "grep -v \"#\" %s",nomf );
	
  if ((fd = popen(commande,"r")) == NULL ){
      printf("Pb d'ouverture du fichier '%s' \n",nomf);
      perror("Le Pb est : ");
      return(1);
   } else { 

      /* lecture variable nombre echantillon ***/
      nbr_echantillon=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_echantillon);

#ifdef DEBUG  /* pour affichage nombre d echantillons *****/
      printf("nombre d'echatillon : %d\n",*nbr_echantillon);
#endif

      /* lecture variable nombre de variable antithétique***/
      nbr_VA=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",nbr_VA);

#ifdef DEBUG  /* pour affichage nombre de variable antithétique *****/
      printf("nombre de variable antithétique : %d\n",*nbr_VA);
#endif

      /* lecture variable longueur trajectoire maxi ****/
      lg_traj_max=(double *)malloc(sizeof(double));
      fscanf(fd,"%lf",lg_traj_max);	

#ifdef DEBUG  /* pour affichage longueur trajectoire maxi *****/
      printf("longueur trajectoire maxi : %lf\n",*lg_traj_max);
#endif

      /* lecture germe ***/
      germe_gener=(int *)malloc(sizeof(int));
      fscanf(fd,"%d",germe_gener);

#ifdef DEBUG  /* pour affichage germe *****/
      printf("germe: %d\n",*germe_gener);
#endif	  
	  lire_couplage(fd);
  }
  pclose(fd);
  return(0);
}

/**
*----------------------------------------------------------------
*
* Function     : lire_index_dl
*
* Result       : int
*
* Parameters   :
*
* Name       Type      Role
*
* fd         FILE      file descriptor to read the C file name
*
* Description  :
*
*	Use to link ws_index fonction for load sharing event with a dynamic library
*
*  
*----------------------------------------------------------------------
*/

int lire_index_dl(FILE *fd){
	
	char * var=(char *)malloc(100*sizeof(char));   // utilis pour lire le nom du fichier  
	char * file=(char *)malloc(100*sizeof(char));
	char *chemin3;
	char *nfin;  
	char *nfout;
	char *chficso;
	
	fscanf(fd,"%s",var);
	
	if(strcmp("File:", var)){
		printf("Error: \"File:\" waited, we have %s\n", var);
	} else {
	  
		fscanf(fd,"%s", file);
		
		if(!strcmp("N", file)){
			printf("There is no index load sharing file\n");
		} else {
			
			/* pour stocker le nom de fichier sans son extension */
			nfin = (char *) calloc(100,sizeof(char));
		nfout = (char *) calloc(100,sizeof(char)); 
		strcpy(nfin,"");
		strcat(nfin,file);
		strcat(nfin,"\0");
		
#ifdef DEBUG
		printf("nom de fichier : %s\n",nfin);
#endif
		
		int lgfile = strlen(nfin);
		
#ifdef DEBUG
		printf("longueur du fichier %d\n",lgfile);
#endif
		
		int i;
		for(i=0;i<(lgfile-1);i++){
			*(nfout+i)=nfin[i];
		}
		strcat(nfout,"\0");
		
#ifdef DEBUG
		printf("nom de fichier : %s\n",nfout);
#endif
		
		
		chemin3=(char *)malloc(100*sizeof(char));
		strcpy(chemin3,"");
#ifdef _LINUX_
		/* pour creer la commande systeme ("gcc -c chemin/test_couplage.c;ld -shared -o test_couplage.so test_couplage.o ") ; */ 
		strcat(chemin3,"gcc -c ");
		strcat(chemin3,file);
		strcat(chemin3,";ld -shared -o ");
		strcat(chemin3,nfout);
		strcat(chemin3,"so ");
		strcat (chemin3,nfout);
		strcat(chemin3,"o ");;
#endif
		
#ifdef _APPLE_
		/* pour creer la commande system ("gcc -dynamiclib chemin/test_couplage.c libtest_couplage.dylib ") ; */ 
		strcat(chemin3,"gcc -dynamiclib -flat_namespace -undefined suppress ");
		strcat(chemin3,file);
		strcat(chemin3," -o lib");
		strcat(chemin3,nfout);
		strcat(chemin3,"dylib ");
#endif
		
#ifdef DEBUG
		printf("commande system %s\n",chemin3);
		
		printf ("C'est le programme main qui s'execute\n") ;
		
		printf ("Compilation/creation de la bibliotheque \n") ;
#endif 
		
		/* pour executer la commande */
		system(chemin3);
		
#ifdef DEBUG
		printf ("Ouverture de la bibliotheque partagee\n") ;
#endif
		
		chficso = (char *)malloc(100*sizeof(char));
		
#ifdef _LINUX_
		strcat(chficso,"./");
		strcat(chficso,nfout);
		strcat(chficso,"so");
#endif
		
#ifdef _APPLE_
		strcat(chficso,"lib");
		strcat(chficso,nfout);
		strcat(chficso,"dylib");
#endif
		
#ifdef DEBUG
		printf("chemin fichier so %s\n",chficso);
#endif
		
		lib_handle_ws = dlopen (chficso, RTLD_LAZY) ;
		if (lib_handle_ws){
			printf("Dynamic library opening: Successful\n");
			dym_ws_on = 1;
		} else {
			printf ("[%s] Unable to open library: %s \n", chficso, dlerror ()) ;
			exit (-1) ;
		}
		
#ifdef DEBUG
		printf ("Dynamic library \n") ;
#endif
		ptr_ws_index = (void *) dlsym (lib_handle_ws, "ws_index") ;
		if (ptr_ws_index == NULL){
			printf("Loading ws_index(): UnSuccessful, default function use\n");
		} else{
			printf("Loading ws_index(): Successful\n");
		}
		
#ifdef DEBUG
		printf ("Pointeur sur la fonction r�up��\n") ;
		printf ("Appel de la fonction\n") ;
#endif
		
		printf ("Index function initialisation : OK\n") ; 
		free(chemin3);
		free(chficso);
		free(nfin);
		free(nfout);
		
		}		
		free(file);
 free(var);
 return 0;
	}
}


/**
*----------------------------------------------------------------
*
* Function     : lire_hier
*
* Result     : int
*
* Parameters   :
*
* Nom       Type      Role
*
* fd   FILE *     pointer on the file descriptor where the hierarchy should be read   
*
* Description  :
*
*		read the hierarchy in the load sharing file and translate it in tree structured data 
*
*----------------------------------------------------------------
*/
int lire_hier(FILE * f){
 	
	int i;
	int num = 0;
	int level = 0;
	char * c = malloc(100*sizeof(char));
	int n;
	int fin_lect = 0;
	int readnext = 1;
	struct st_hier_node * cur = NULL;
	struct st_hier_node * son = NULL;


	while(!fin_lect){
		if(readnext)
			*c = (char)fgetc(f);
		else
			readnext = 1;
		
		if(*c == ','){
			;
		}
		else if(*c == '('){
			if(level == 0 && hier_root == NULL){ // cas particulier de la racine (début de la lecture)
				hier_root = (struct st_hier_node *)malloc(sizeof(struct st_hier_node));
				hier_root->num = num;
				hier_root->level = level;
				hier_root->father = NULL;
				hier_root->nbsons = 0;
				hier_root->sons = NULL;
				
				cur = hier_root;
			}
			else{
				
				son = add_son(cur);
				
				num++;
				level++;
				son->num = num;
				son->level = level;
				son->father = cur;
				
				cur = son;
			}
		}
		else if(*c == ')'){
			cur = cur->father;
			level--;
			
			if(level == -1){
				fin_lect = 1;
			}
		}
		else if(*c >= '0' && *c <= '9'){
			i = 0;
			while(*(c+i) >= '0' && *(c+i) <= '9'){
				i++;
				*(c+i) = (char)fgetc(f);
				}
			n = atoi(c);

			*c = *(c+i);
			readnext = 0;
			
			son = add_son(cur);
			num++;
			son->num = num;
			son->level = level + 1;
			son->father = cur;

			node_queue[n] = son;

		}
		
	}

	make_table_node_dist();

	free(c);

	return 0;
}


/**
*----------------------------------------------------------------
*
* Function     : make_table_node_dist
*
* Result     : int
*
* Parameters   :
*
* Nom       Type      Role
*
* Description  :
*
*		construct the node_dist Matrix from the hierarchy tree (load sharing) 
*
*----------------------------------------------------------------
*/
int make_table_node_dist(){
	
	int i,j,k;
	struct st_hier_node * P;
	struct st_hier_node * node;

	struct st_hier_node *** queue_path = (struct st_hier_node ***)malloc(*nbr_file * sizeof(struct st_hier_node **));

	for(i=0;i<*nbr_file;i++){
		node = node_of_queue(i);
		queue_path[i] = (struct st_hier_node **)malloc((node->level+1)* sizeof(struct st_hier_node *));
		P = node;
		j = node->level;
		
		while(P != NULL){
			queue_path[i][j] = P;
			P = P->father;
			j--;
		}
	}


	for(i=0;i<*nbr_file;i++){
		for(j=0;j<=i;j++){
			k=0;
			while(queue_path[i][k] == queue_path[j][k] && k < ((node_of_queue(i))->level+1)){
				k++;
			}
			node_dist[i][j] = (node_of_queue(i))->level + (node_of_queue(j))->level  - 2*(queue_path[i][k-1])->level;
		}
		}
	
	
	for(i=0;i<*nbr_file;i++){
		free(queue_path[i]);
	}
	free(queue_path);
	
	return 0;

}

void node_dist_to_node_neighbors() {
	
	int i,j,k;
	int nb_neigh;
	int * tmp_neigh = (int *)calloc(*nbr_file,sizeof(int));

	node_neighbors = (struct st_neighbors *)calloc(*nbr_file,sizeof(struct st_neighbors));
	
	for(i=0;i<*nbr_file;i++){
		nb_neigh = 0;

		for(j=0;j<*nbr_file;j++){
			if(node_dist[i][j] == 1){
				tmp_neigh[nb_neigh] = j;
				nb_neigh++;
			}
		}

		node_neighbors[i].nb_neighbors = nb_neigh;
		node_neighbors[i].neighbors = (int *)calloc(nb_neigh, sizeof(int));
		for(k=0;k<nb_neigh;k++){
			node_neighbors[i].neighbors[k] = tmp_neigh[k];
		}
	}

	free(tmp_neigh);

	
	for(i=0;i<*nbr_file;i++){
		
		printf("\nles voisins de %d sont : \n", i);
		
		for(j=0;j<node_neighbors[i].nb_neighbors;j++){
			printf("%d ", node_neighbors[i].neighbors[j]);
		}
	}



}
